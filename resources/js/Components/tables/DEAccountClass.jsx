import { moveCursorToEnd } from "@/utlis/common";
import React, { useRef, useCallback, useEffect } from "react";

// DataEditor Level
const DEAccountClass = ({ onChange, cell }) => {
    const inputRef = useRef(null);

    const handleChange = useCallback(
        (event) => {
            onChange({ ...cell, value: event.target.value });
        },
        [onChange, cell]
    );

    useEffect(() => {
        if (inputRef.current) {
            moveCursorToEnd(inputRef.current);
        }
    }, [inputRef]);

    const value = cell?.value ?? "";

    return (
        <div className="Spreadsheet__data-editor whitespace-normal">
            <select ref={inputRef} onChange={handleChange}
                value={value}
                autoFocus>
                <option value="">Please Account Class...</option>
                <option value="1.1 Current Assets">1.1 Current Assets</option>
                <option value="1.2 Non-current Assets">1.2 Non-current Assets</option>
                <option value="2.1 Short Term Liability">2.1 Short Term Liability</option>
                <option value="2.2 Long Term Liability">2.2 Long Term Liability</option>
                <option value="3.1 Capital Stock">3.1 Capital Stock</option>
                <option value="3.2 Other Equity Component">3.2 Other Equity Component</option>
                <option value="3.3 Comprehensive Income - B/S">3.3 Comprehensive Income - B/S</option>
                <option value="3.4 Retained Earning">3.4 Retained Earning</option>
                <option value="3.5 Non-controlling Interest">3.5 Non-controlling Interest</option>
                <option value="4.1 Revenue">4.1 Revenue</option>
                <option value="5.1 Cost of Revenue">5.1 Cost of Revenue</option>
                <option value="6.1 Operating Expenses">6.1 Operating Expenses</option>
                <option value="7.1 Other (Income)/Expenses">7.1 Other (Income)/Expenses </option>
                <option value="8.1 Income Tax">8.1 Income Tax</option>
                <option value="9.1 Other Comprehensive Income (OCI) - P/L">9.1 Other Comprehensive Income (OCI) - P/L</option>
            </select>
        </div>
    );
};
export default DEAccountClass