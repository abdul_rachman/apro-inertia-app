import * as React from "react";
import { getComputedValue } from "@/utlis/common";

export const TRUE_TEXT = "TRUE";
export const FALSE_TEXT = "FALSE";

/** The default Spreadsheet DVFormula component */
const DVFormula = ({ cell, formulaParser }) => {
    console.log('cell',cell)
    const value = getComputedValue({ cell, formulaParser });
    //   const temp = getComputedValue({ cell, "=LEFT" });
    return <span className="Spreadsheet__data-viewer">testing</span>;
};

export default DVFormula;

export function convertBooleanToText(value) {
    return value ? TRUE_TEXT : FALSE_TEXT;
}
