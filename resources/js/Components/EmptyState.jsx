export default function EmptyState({image, description}){
    return(
        <div className="flex flex-col justify-center items-center py-8">
            <img className="w-28" src={image} alt="" />
            <p className="mt-8 text-[#464646] font-semibold text-sm">{description}</p>
        </div>
    )
}