import React from 'react';

export default function Button({ type = 'submit', className = '', processing, children, ...props }) {
    return (
        <button
            {...props}
            type={type}
            className={
                `block w-full p-[18px] text-center bg-[#1088E4] border border-transparent rounded-md font-semibold text-white uppercase tracking-widest active:bg-gray-900 transition ease-in-out duration-150 ${processing && 'opacity-25'
                } ` + className
            }
            disabled={processing}
        >
            {children}
        </button>
    );
}
