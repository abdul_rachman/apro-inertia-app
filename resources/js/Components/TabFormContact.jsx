import React, { useState } from 'react'
import InputForm from './clients/InputForm'
import InputSelect from './InputSelect'

const options = [
    { label: "Status PIC", value: "" },
    { label: "Permanent", value: "Permanent" },
    { label: "Contract", value: "Contract" },
]

const dataField = {
    managing_partner_name: "",
    managing_partner_email: "",
    managing_partner_phone: "",
    // person_in_charge_name: "",
    // person_in_charge_email: "",
    person_in_charge_phone: "",
    status: "",
}

const labelField = {
    managing_partner_name: "Managing Partner Name",
    managing_partner_email: "Email",
    managing_partner_phone: "Phone No.",
    person_in_charge_name: "Person-in-Charge (PIC) Name",
    person_in_charge_email: "Email",
    person_in_charge_phone: "Phone No.",
    status: "Status",
}

export default function TabFormContact({ onCancel, onSubmit }) {
    const [data, setData] = useState(dataField)
    const [errors, setErrors] = useState({})

    const [status, setStatus] = useState(options[0])

    const handelSetData = (key, value) => {
        const temp = { ...data }
        temp[key] = value
        setData(temp)
    }

    const handleSubmit = () => {
        const temp = { ...errors }
        for (const [key, value] of Object.entries({...data, status: status?.value})) {
            console.log(key,data[key])
            console.log('status?.value,',typeof status?.value)
            console.log('data[key],',typeof data[key])
            if (value === "") {
                temp[key] = `${labelField[key]} is required`
            } else {
                delete temp[key]
            }
        }
        setErrors(temp)
        if (Object.keys(temp).length === 0 && temp.constructor === Object) {
            onSubmit({...data, status: status?.value})
        }
    }

    return (
        <div className="flex flex-col space-y-6">
            <InputForm
                value={data?.managing_partner_name} onChange={(e) => handelSetData('managing_partner_name', e.target.value)}
                type="text"
                className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                placeholder={labelField?.managing_partner_name}
                error={errors?.managing_partner_name}
            />
            <div className="flex space-x-5 mb-10">
                <div className="flex-1">
                    <InputForm
                        value={data?.managing_partner_email} onChange={(e) => handelSetData('managing_partner_email', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField?.managing_partner_email}
                        error={errors?.managing_partner_email}
                    />
                </div>
                <div className="flex-1">
                    <InputForm
                        value={data?.managing_partner_phone} onChange={(e) => handelSetData('managing_partner_phone', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField?.managing_partner_phone}
                        error={errors?.managing_partner_phone}
                    />
                </div>
            </div>
            <InputForm
                value={data?.person_in_charge_phone} onChange={(e) => handelSetData('person_in_charge_phone', e.target.value)}
                type="text"
                className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5 "
                placeholder={labelField?.person_in_charge_phone}
                error={errors?.person_in_charge_phone}
            />
            {/*<InputForm
                value={data?.person_in_charge_name} onChange={(e) => handelSetData('person_in_charge_name', e.target.value)}
                type="text"
                className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5 "
                placeholder={labelField?.person_in_charge_name}
                error={errors?.person_in_charge_name}
            />
            <div className="flex space-x-5">
                <div className="flex-1">
                    <InputForm
                        value={data?.person_in_charge_email} onChange={(e) => handelSetData('person_in_charge_email', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField?.person_in_charge_email}
                        error={errors?.person_in_charge_email}
                    />
                </div>
                <div className="flex-1">
                    <InputForm
                        value={data?.person_in_charge_phone} onChange={(e) => handelSetData('person_in_charge_phone', e.target.value)}
                        type="text"
                        className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                        placeholder={labelField?.person_in_charge_phone}
                        error={errors?.person_in_charge_phone}
                    />
                </div>
            </div>*/}
            <InputSelect options={options} value={status} onChange={(item) => setStatus(item)} />
            {
                errors?.status && (
                    <span className="text-xs text-red-500 mr-1 mt-2">{errors?.status}</span>
                )
            }
            <div className="flex justify-end">
                <div className="flex">
                    <button onClick={onCancel} className="py-4 px-7 text-[#1088E4] rounded-lg font-medium">Back</button>
                    <button onClick={handleSubmit} className="py-4 px-7 bg-[#1088E4] text-white rounded-lg font-semibold">Submit</button>
                </div>
            </div>
        </div>
    )
}