export default function Card({ label, sublabel }) {
    return (
        <div className="border border-[#E6E6E8] rounded-2xl px-3 py-4 w-full bg-white shadow-md">
            <div className="flex justify-between items-center ">
                <a
                    href="#"
                    className="w-6 h-6 bg-[#ECEFF7] rounded-full flex justify-center items-center"
                >
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="10"
                        height="6"
                        fill="none"
                        viewBox="0 0 10 6"
                    >
                        <path
                            fill="#464646"
                            d="M9.395.555a.82.82 0 010 1.176L5.697 5.323a1 1 0 01-1.394 0L.605 1.731A.82.82 0 011.748.555l2.904 2.82a.5.5 0 00.696 0L8.252.556a.82.82 0 011.143 0z"
                        ></path>
                    </svg>
                </a>
                <a href="" className="text-xs text-[#1989E9]">
                    View
                </a>
            </div>
            <div className="mt-4">
                <strong className="block text-[#2A2A2A]">{label}</strong>
                <span className="block text-[#9FB3C5] text-xs mt-1">
                    {sublabel}
                </span>
            </div>
        </div>
    );
}
