import {
    BarChart,
    Bar,
    XAxis,
    YAxis,
    CartesianGrid,
    Tooltip,
    Legend,
    ResponsiveContainer,
} from "recharts";
import TitleWithDropdown from "./TitleWIthDropdown";

const data = [
    {
        name: "Jan",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Feb",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Mar",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Apr",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "May",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Jun",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Jul",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Aug",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Sep",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Nov",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
    {
        name: "Des",
        all: 0,
        new: 0,
        on_review: 0,
        rejected: 0,
    },
];

export default function Insight() {
    return (
        <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 mt-5 shadow-md">
            <TitleWithDropdown title="APRO Insight" label="This Month" />
            <div className="w-full h-[450px]">
                <ResponsiveContainer>
                    <BarChart
                        width={500}
                        height={300}
                        data={data}
                        margin={{
                            top: 20,
                            right: 30,
                            left: 20,
                            bottom: 5,
                        }}
                    >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis
                            axisLine={false}
                            tick={{ fill: "#9FB3C5" }}
                            dataKey="name"
                            padding={{ left: 42, right: 20 }}
                        />
                        <YAxis axisLine={false} tick={{ fill: "#9FB3C5" }} />
                        <Tooltip />
                        <Legend />
                        <Bar
                            barSize={60}
                            dataKey="all"
                            stackId="a"
                            fill="#9FDBFF"
                        />
                        <Bar dataKey="new" stackId="a" fill="#3F8DFD" />
                        <Bar dataKey="on_review" stackId="a" fill="#FF9D3F" />
                        <Bar dataKey="rejected" stackId="a" fill="#FF9D3F" />
                    </BarChart>
                </ResponsiveContainer>
            </div>
        </div>
    );
}
