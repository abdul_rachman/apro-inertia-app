import {Fragment, useEffect, useState} from 'react'
import {Listbox, Transition} from '@headlessui/react'
import {useTranslation} from 'react-i18next'

const people = [{name: 'Indonesia'}, {name: 'English'}]

export default function TranslateButton() {
  const [t, i18n] = useTranslation('common')
  const [selected, setSelected] = useState(people[0])

  return (
    <div className="">
      <Listbox
        value={selected}
        onChange={e => {
          setSelected(e)
          if (e.name === 'English') {
            i18n.changeLanguage('en')
          } else {
            i18n.changeLanguage('id')
          }
        }}
      >
        <div className="relative">
          <Listbox.Button className="hover:cursor-pointer relative w-[200px] cursor-default rounded-lg bg-white py-[15px] pl-3 pr-10 text-left border-[#2EA6FF] border">
            <div className="flex items-center gap-3">
              <svg width="24" height="25" viewBox="0 0 24 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                <path
                  d="M2 12.5762C2 18.0992 6.477 22.5762 12 22.5762C17.523 22.5762 22 18.0992 22 12.5762C22 7.05317 17.523 2.57617 12 2.57617C6.477 2.57617 2 7.05317 2 12.5762Z"
                  stroke="#2EA6FF"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M13 2.62598C13 2.62598 16 6.57598 16 12.576C16 18.576 13 22.526 13 22.526"
                  stroke="#2EA6FF"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M11 22.526C11 22.526 8 18.576 8 12.576C8 6.57598 11 2.62598 11 2.62598"
                  stroke="#2EA6FF"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M2.63086 16.0762H21.3709"
                  stroke="#2EA6FF"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
                <path
                  d="M2.63086 9.07617H21.3709"
                  stroke="#2EA6FF"
                  stroke-width="1.5"
                  stroke-linecap="round"
                  stroke-linejoin="round"
                />
              </svg>

              <span className="block truncate text-[#2EA6FF]">{selected.name}</span>
            </div>
            <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                fill="none"
                viewBox="0 0 24 24"
                strokeWidth={1.5}
                stroke="currentColor"
                className="w-5 h-5 stroke-[#2EA6FF]"
              >
                <path strokeLinecap="round" strokeLinejoin="round" d="M19.5 8.25l-7.5 7.5-7.5-7.5" />
              </svg>
            </span>
          </Listbox.Button>
          <Transition as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
            <Listbox.Options className="absolute mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
              {people.map((person, personIdx) => (
                <Listbox.Option
                  key={personIdx}
                  className={({active}) =>
                    `relative cursor-default select-none py-2 pl-10 pr-4 ${
                      active ? 'bg-amber-100 text-amber-900' : 'text-gray-900'
                    }`
                  }
                  value={person}
                >
                  {({selected}) => (
                    <>
                      <span className={`block truncate ${selected ? 'font-medium' : 'font-normal'}`}>
                        {person.name}
                      </span>
                      {selected ? (
                        <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-amber-600">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            fill="none"
                            viewBox="0 0 24 24"
                            strokeWidth={1.5}
                            stroke="currentColor"
                            className="w-6 h-6"
                          >
                            <path
                              strokeLinecap="round"
                              strokeLinejoin="round"
                              d="M9 12.75L11.25 15 15 9.75M21 12a9 9 0 11-18 0 9 9 0 0118 0z"
                            />
                          </svg>
                        </span>
                      ) : null}
                    </>
                  )}
                </Listbox.Option>
              ))}
            </Listbox.Options>
          </Transition>
        </div>
      </Listbox>
    </div>
  )
}
