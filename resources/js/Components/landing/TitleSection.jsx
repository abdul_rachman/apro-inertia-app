export default function TitleSection({title, descriptions,learMore = "Lear More", className = ""}) {
  return (
    <div className={`text-center ${className}`}>
      <h3 className="text-[38px] font-semibold mb-4 text-[#222222]">{title}</h3>
      <p className="text-[#22222280] mb-4">{descriptions}</p>
      <a className="text-[#2EA6FF] inline-block" href="#">
        <div className="flex items-center">
          <span>{learMore}</span>
          <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
            <path
              d="M13.9395 12.3536L10.0001 16.293L10.707 16.9999L15.7069 12L10.707 7.00014L10.0001 7.70703L13.9395 11.6465L14.2931 12L13.9395 12.3536Z"
              fill="black"
              stroke="#2EA6FF"
            />
          </svg>
        </div>
      </a>
    </div>
  )
}
