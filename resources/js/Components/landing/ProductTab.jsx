import {useState} from 'react'
import {Tab} from '@headlessui/react'
import {useTranslation} from 'react-i18next'

function classNames(...classes) {
  return classes.filter(Boolean).join(' ')
}

export default function ProductTab() {
  const [t, i18n] = useTranslation('common')
  const categories = {
    Feature: [
      {
        label: t('welcome.products.feature.label'),
        title: t('welcome.products.feature.title'),
        image: 'images/feature.png',
        descriptions: t('welcome.products.feature.content'),
        list: [
          t('welcome.products.feature.list.item1'),
          t('welcome.products.feature.list.item2'),
          t('welcome.products.feature.list.item3'),
          t('welcome.products.feature.list.item4'),
        ],
        link: '#',
        linkLabel: t('welcome.trialLabel'),
      },
    ],
    Company: [
      {
        label: t('welcome.products.company.label'),
        title: t('welcome.products.company.title'),
        image: 'images/product_company.png',
        descriptions: t('welcome.products.company.content'),
        list: [
          t('welcome.products.company.list.item1'),
          t('welcome.products.company.list.item2'),
          t('welcome.products.company.list.item3'),
          t('welcome.products.company.list.item4'),
        ],
        link: '#',
        linkLabel: t('welcome.trialLabel'),
      },
    ],
    Produk: [
      {
        label: t('welcome.products.product.label'),
        title: t('welcome.products.product.title'),
        image: 'images/product_product.png',
        descriptions: t('welcome.products.product.content'),
        list: [
          t('welcome.products.product.list.item1'),
          t('welcome.products.product.list.item2'),
          t('welcome.products.product.list.item3'),
          t('welcome.products.product.list.item4'),
        ],
        link: '#',
        linkLabel: t('welcome.trialLabel'),
      },
    ],
    Chat: [
      {
        label: t('welcome.products.custom.label'),
        title: t('welcome.products.custom.title'),
        image: 'images/product_custom.png',
        descriptions: t('welcome.products.custom.content'),
        link: '#',
        linkLabel: t('welcome.trialLabel'),
      },
    ],
    Monitoring: [
      {
        label: t('welcome.products.meetonline.label'),
        title: t('welcome.products.meetonline.title'),
        image: 'images/product_meet.png',
        descriptions: t('welcome.products.meetonline.content'),
        link: '#',
        linkLabel: t('welcome.trialLabel'),
      },
    ],
  }

  return (
    <div className="w-full mt-[48px]">
      <Tab.Group>
        <Tab.List className="flex max-w-[768px] mx-auto space-x-1 rounded-full bg-[#F3F4F8] p-2">
          {Object.keys(categories).map(category => (
            <Tab
              key={category}
              className={({selected}) =>
                classNames(
                  'w-full rounded-full py-2.5 text-sm font-medium leading-5 text-white',
                  'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2',
                  selected ? 'bg-gradient-to-b from-[#2EA6FF] to-[#0C7CD1]' : 'text-[#22222280]'
                )
              }
            >
              {categories[category][0].label}
            </Tab>
          ))}
        </Tab.List>
        <Tab.Panels className="mt-2">
          {Object.values(categories).map((posts, idx) => (
            <Tab.Panel
              key={idx}
              className={classNames(
                'rounded-xl bg-white p-3 py-[100px]',
                'ring-white ring-opacity-60 ring-offset-2 ring-offset-blue-400 focus:outline-none focus:ring-2'
              )}
            >
              {posts.map(post => (
                <div className="flex">
                  <div className="flex-1 flex flex-col justify-center h-full">
                    <h5 className="text-[25px] text-[#222222] font-semibold">{post.title}</h5>
                    <p className="my-10 text-[#22222280]">{post.descriptions}</p>
                    {post.list ? (
                      <ul>
                        {post.list.map(item => (
                          <li className="flex gap-6 mb-2 text-[#22222280]">
                            <span>
                              <svg
                                width="24"
                                height="24"
                                viewBox="0 0 24 24"
                                className="w-6 h-6"
                                fill="none"
                                xmlns="http://www.w3.org/2000/svg"
                              >
                                <path
                                  d="M12 22C6.48 21.994 2.006 17.52 2 12V11.8C2.11 6.30502 6.635 1.92802 12.13 2.00002C17.627 2.07402 22.034 6.56902 21.998 12.065C21.962 17.562 17.497 22 12 22ZM7.41 11.59L6 13L10 17L18 9.00002L16.59 7.58002L10 14.17L7.41 11.59Z"
                                  fill="url(#paint0_linear_201_2738)"
                                />
                                <path
                                  d="M12 22C6.48 21.994 2.006 17.52 2 12V11.8C2.11 6.30502 6.635 1.92802 12.13 2.00002C17.627 2.07402 22.034 6.56902 21.998 12.065C21.962 17.562 17.497 22 12 22ZM7.41 11.59L6 13L10 17L18 9.00002L16.59 7.58002L10 14.17L7.41 11.59Z"
                                  fill="url(#paint1_linear_201_2738)"
                                />
                                <defs>
                                  <linearGradient
                                    id="paint0_linear_201_2738"
                                    x1="6.99956"
                                    y1="19.7063"
                                    x2="17.0005"
                                    y2="4.29413"
                                    gradientUnits="userSpaceOnUse"
                                  >
                                    <stop stop-color="#4A26C4" />
                                    <stop offset="1" stop-color="#6D48E5" />
                                  </linearGradient>
                                  <linearGradient
                                    id="paint1_linear_201_2738"
                                    x1="12.0628"
                                    y1="1.99915"
                                    x2="12.0628"
                                    y2="22"
                                    gradientUnits="userSpaceOnUse"
                                  >
                                    <stop stop-color="#2EA6FF" />
                                    <stop offset="1" stop-color="#0C7CD1" />
                                  </linearGradient>
                                </defs>
                              </svg>
                            </span>
                            <span>{item}</span>
                          </li>
                        ))}
                      </ul>
                    ) : null}
                    <a href={post.link} className="mt-10 inline-block text-[#2EA6FF]">
                      <div className="flex gap-2 items-center">
                        <span> {post.linkLabel}</span>
                        <svg width="16" height="16" viewBox="0 0 16 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                          <path
                            d="M4.47206 12.4713C4.21171 12.7317 3.7896 12.7317 3.52925 12.4713C3.2689 12.211 3.2689 11.7889 3.52925 11.5285L11.5292 3.52851C11.7896 3.26816 12.2117 3.26816 12.4721 3.52851C12.7324 3.78886 12.7324 4.21097 12.4721 4.47132L4.47206 12.4713Z"
                            fill="#2EA6FF"
                          />
                          <path
                            d="M5.33464 3.33325C4.96645 3.33325 4.66797 3.63173 4.66797 3.99992C4.66797 4.36811 4.96645 4.66659 5.33464 4.66659H11.3346V10.6666C11.3346 11.0348 11.6331 11.3333 12.0013 11.3333C12.3695 11.3333 12.668 11.0348 12.668 10.6666V3.99992C12.668 3.63173 12.3695 3.33325 12.0013 3.33325H5.33464Z"
                            fill="#2EA6FF"
                          />
                        </svg>
                      </div>
                    </a>
                  </div>
                  <div className="flex-1">
                    <div className="flex justify-center items-center h-full">
                      <img src={post.image} />
                    </div>
                  </div>
                </div>
              ))}
            </Tab.Panel>
          ))}
        </Tab.Panels>
      </Tab.Group>
    </div>
  )
}
