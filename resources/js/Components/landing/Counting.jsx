export default function Counting({icon, title, descriptions}) {
  return (
    <div className="flex-1 flex flex-col items-center text-center gap-10">
      {icon}
      <div className="flex flex-col">
        <h3 className="font-semibold text-[50px] text-[#222222]">{title}</h3>
        <p className="text-[#22222280]">{descriptions}</p>
      </div>
    </div>
  )
}
