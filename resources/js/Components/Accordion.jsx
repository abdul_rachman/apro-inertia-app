import {Disclosure, Transition} from '@headlessui/react'
import React from 'react'

export default function Accordion({buttonContent, children}) {
  return (
    <Disclosure>
      {({open}) => (
        <>
          <Disclosure.Button as="div" className="w-full cursor-pointer relative">
            {buttonContent}
            <img
              className={`absolute top-1/2 -translate-y-1/2 right-8 transition-all ${open ? 'rotate-0' : '-rotate-90'}`}
              src="/images/icons/chevron.svg"
              alt=""
            />
          </Disclosure.Button>
          <Transition
            enter="transition duration-100 ease-out"
            enterFrom="transform scale-y-95 opacity-0"
            enterTo="transform scale-y-100 opacity-100"
            leave="transition duration-75 ease-out"
            leaveFrom="transform scale-y-100 opacity-100"
            leaveTo="transform scale-y-95 opacity-0"
          >
            <Disclosure.Panel>{children}</Disclosure.Panel>
          </Transition>
        </>
      )}
    </Disclosure>
  )
}
