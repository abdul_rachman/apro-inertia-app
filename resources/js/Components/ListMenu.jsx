import { Link } from "@inertiajs/inertia-react";

function ListMenu({ children, href = undefined, onClick = () => { }, active = false }) {
    return (
        <Link href={href} onClick={(e) => {
            if (!href) {
                e.preventDefault();
                onClick();
            }
        }} className={`flex w-full justify-between px-8 py-5 group hover:bg-[#1988e91f] hover:cursor-pointer ${active ? "bg-[#F9F9FB]" : "bg-white"} `}>
            {children}
        </Link>
    );
}

function ListGroup({ type = "row", center = false, right = false, children }) {
    return (
        <div className={`flex-1 flex flex-${type} ${type === "row" && "space-x-5"} ${center && "items-center"} ${right && "justify-end"}`}>
            {children}
        </div>
    );
}

function ListItemTitle({ text }) {
    return (
        <span className="font-medium text-sm">
            {text}
        </span>
    );
}

function ListItemSubtitle({ text }) {
    return (
        <span className="font-medium text-xs text-[#464646CC]">
            {text}
        </span>
    );
}

function ListItemIcon() {
    return (
        <svg
            xmlns="http://www.w3.org/2000/svg"
            width="6"
            height="10"
            fill="none"
            viewBox="0 0 6 10"
        >
            <path
                fill="#323232"
                d="M.555.605a.82.82 0 011.176 0l3.592 3.698a1 1 0 010 1.394L1.731 9.395A.82.82 0 01.555 8.252l2.82-2.904a.5.5 0 000-.696L.556 1.748a.82.82 0 010-1.143z"
            ></path>
        </svg>
    )
}

function ListItemBadge({ count }) {
    return (
        <span className="px-2 text-white rounded-full text-[10px] bg-[#FF5050]">{count}</span>
    )
}


ListMenu.Group = ListGroup;
ListMenu.Title = ListItemTitle;
ListMenu.Subtitle = ListItemSubtitle;
ListMenu.Icon = ListItemIcon;
ListMenu.Badge = ListItemBadge;

export default ListMenu;