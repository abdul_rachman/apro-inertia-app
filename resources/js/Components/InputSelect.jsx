import { Listbox, Transition } from "@headlessui/react";
import { Fragment, useState } from "react";

export default function InputSelect({ options, value, onChange}) {
    return (
        <Listbox value={value} onChange={onChange}>
            <div className="relative mt-1">
                <Listbox.Button className="relative w-full cursor-default border hover:cursor-pointer border-[#E6E6E8] focus:border-indigo-300 rounded-lg bg-white py-4 pl-4 pr-10 text-left text-[rgb(109,109,109)]">
                    <span className="block truncate">{value.label}</span>
                    <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <rect width="24" height="24" rx="12" fill="#ECEFF7" />
                            <path d="M16.3947 9.55487C16.7261 9.87681 16.7261 10.4089 16.3947 10.7308L12.6968 14.3231C12.3087 14.7001 11.6913 14.7001 11.3032 14.3231L7.60528 10.7308C7.27387 10.4089 7.27387 9.87681 7.60528 9.55487C7.92338 9.24586 8.42956 9.24586 8.74766 9.55487L11.6516 12.3758C11.8456 12.5643 12.1544 12.5643 12.3484 12.3758L15.2523 9.55487C15.5704 9.24586 16.0766 9.24586 16.3947 9.55487Z" fill="#2A2A2A" />
                        </svg>
                    </span>
                </Listbox.Button>
                <Transition
                    as={Fragment}
                    leave="transition ease-in duration-100"
                    leaveFrom="opacity-100"
                    leaveTo="opacity-0"
                >
                    <Listbox.Options className="absolute mt-1 z-10 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {options.map((person, personIdx) => (
                            <Listbox.Option
                                key={personIdx}
                                className={({ active }) =>
                                    `relative cursor-default hover:cursor-pointer select-none py-2 pl-10 pr-4 ${active ? 'bg-[#55a8ef1f]' : 'text-[#464646]'
                                    }`
                                }
                                value={person}
                            >
                                {({ selected }) => (
                                    <>
                                        <span
                                            className={`block truncate ${selected ? 'font-medium' : 'font-normal'
                                                }`}
                                        >
                                            {person.label}
                                        </span>
                                        {selected ? (
                                            <span className="absolute inset-y-0 left-0 flex items-center pl-3 text-[#1088E4]">
                                                <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6" fill="none" viewBox="0 0 24 24" stroke="currentColor" strokeWidth={2}>
                                                    <path strokeLinecap="round" strokeLinejoin="round" d="M5 13l4 4L19 7" />
                                                </svg>
                                            </span>
                                        ) : null}
                                    </>
                                )}
                            </Listbox.Option>
                        ))}
                    </Listbox.Options>
                </Transition>
            </div>
        </Listbox>
    )
}