import { useState } from "react";

export default function InputForm(props) {
    return (
        <div className="flex flex-col w-full">
            <input
                {...props}
                className={`w-full border  ${ props?.error ? "border-red-500" : "border-[#E6E6E8]"} rounded-lg text-[#464646] px-4 py-5`}
                onkeypress={() => setHideError(false)}
            />
            {
                props?.error && (
                    <span className="text-xs text-red-500 mr-1 mt-2">{props?.error}</span>
                )
            }
        </div>
    );
}
