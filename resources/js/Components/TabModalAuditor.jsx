import { Tab } from "@headlessui/react";
import DatePicker from "react-datepicker";
import { useState } from "react";

import "react-datepicker/dist/react-datepicker.css";
import { useForm } from "@inertiajs/inertia-react";
import InputForm from "./clients/InputForm";
import InputSelect from "./InputSelect";
import Switch from "react-switch";

const options = [
    {
        label: "Jenjang Pendidikan",
        value: ""
    },
    {
        label: "Strata 2 (S2) ",
        value: "s2"
    },
    {
        label: "Strata 1 (S1) ",
        value: "s1"
    },
    {
        label: "Diploma 3 (D3)",
        value: "d3"
    },
    {
        label: "Sekolah Menengah Atas (SMA)",
        value: "sma"
    },
]

export default function TabModalAuditor({ roles, closeModal }) {
    const [selectRole, setSelectRole] = useState(roles[0]);
    const [selectJenjang, setSelectJenjang] = useState(options[0]);
    const [status, setStatus] = useState(false);

    const { data, setData, post, progress, reset, errors } = useForm({
        auditor_role_id: '',
        staff_id: '',
        name: '',
        id_number: '',
        address: '',
        education: '',
        email: '',
    });

    const handleSubmit = () => {
        data["auditor_role_id"] = selectRole.value
        data["education"] = selectJenjang.value;
        post(route('company.auditors.store'), {
            onSuccess: () => {
                reset()
                closeModal()
            },
            onError: (errors) => {
                console.log('error gaess', errors)
            }
        });
    }

    return (
        <div className="mt-6">
            <div className="space-y-4">
                <InputForm
                    error={errors?.staff_id}
                    value={data?.staff_id} onChange={(e) => setData('staff_id', e.target.value)}
                    type="text"
                    className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                    placeholder="ID Karyawan"
                />
                <InputForm
                    error={errors?.name}
                    value={data?.name} onChange={(e) => setData('name', e.target.value)}
                    type="text"
                    className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                    placeholder="Nama Lengkap"
                />

                <InputForm error={errors?.id_number} value={data?.id_number} onChange={(e) => setData('id_number', e.target.value)} type="text" placeholder="No. KTP" />

                <InputSelect options={roles} value={selectRole} onChange={(item) => setSelectRole(item)} />

                <textarea
                    value={data?.address} onChange={(e) => setData('address', e.target.value)}
                    className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                    placeholder="Alamat"
                ></textarea>

                <InputSelect options={options} value={selectJenjang} onChange={(item) => setSelectJenjang(item)} />

                <InputForm
                    error={errors?.email}
                    value={data?.email} onChange={(e) => setData('email', e.target.value)}
                    type="text"
                    className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5"
                    placeholder="Email"
                />

                {/* <div className="flex justify-between py-5 px-5">
                    <span className="text-[#464646] ">Status</span>
                    <Switch uncheckedIcon={false} checkedIcon={false} onChange={setStatus} checked={status} />
                </div> */}
            </div>
            <div className="mt-10 space-x-4 flex justify-end">
                <button
                    type="button"
                    onClick={closeModal}
                    className="font-semibold text-sm p-5 px-8 text-[#1088E4] rounded-lg"
                >
                    Batal
                </button>
                <button
                    type="button"
                    onClick={() => {
                        handleSubmit()
                    }}
                    className="font-semibold text-sm p-5 px-8 text-white bg-[#1088E4] rounded-lg"
                >
                    Simpan
                </button>
            </div>
        </div>
    );
}
