import React, { useEffect, useRef } from 'react';

export default function Input({
    type = 'text',
    name,
    value,
    className,
    maxlength,
    autoComplete,
    required,
    isFocused,
    placeholder,
    handleChange,
}) {
    const input = useRef();

    useEffect(() => {
        if (isFocused) {
            input.current.focus();
        }
    }, []);

    return (
        <div className="flex flex-col items-start">
            <input
                type={type}
                name={name}
                value={value}
                maxLength={maxlength}
                className={
                    `border-[#E6E6E8] focus:border-indigo-300 p-5 focus:ring focus:ring-indigo-200 text-sm focus:ring-opacity-50 rounded-lg shadow-sm ` +
                    className
                }
                ref={input}
                placeholder={placeholder}
                autoComplete={autoComplete}
                required={required}
                onChange={(e) => handleChange(e)}
            />
        </div>
    );
}
