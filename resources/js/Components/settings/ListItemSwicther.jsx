import Switch from "react-switch";

const ListItemSwicther = ({ label, onChange, checked, borderTop = false, borderBottom = true, roundedTop = false, roundedBottom = false }) => {
    return (
        <li className={`bg-white  ${roundedTop && "rounded-t-lg"} ${roundedBottom && "rounded-b-lg"} overflow-hidden`}>
            <div className={`text-xs p-4 pl-0 ml-5 flex justify-between items-center ${borderTop && "border-t"} ${borderBottom && "border-b"} text-[#464646]`}>
                <span>{label}</span>
                <Switch onChange={onChange} uncheckedIcon={false} checkedIcon={false} onColor="#40E406" offColor="#4646461e" checked={checked} />
            </div>
        </li>
    )
}

export default ListItemSwicther