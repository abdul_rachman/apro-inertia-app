const ListItemInput = ({ title, value, onChange, borderTop = false, borderBottom = true, roundedTop = false, roundedBottom = false }) => {
    return (
        <li className={`bg-white  ${roundedTop && "rounded-t-lg"} ${roundedBottom && "rounded-b-lg"} overflow-hidden`}>
            <div className={`text-xs p-4 pl-0 ml-5 flex justify-between items-center ${borderTop && "border-t"} ${borderBottom && "border-b"} text-[#464646]`}>
                <span>{title}</span>
                <input value={value} onChange={onChange} placeholder={title} className="border-none text-right focus:outline-none" />
            </div>
        </li>
    )
}

export default ListItemInput

