import { Link } from "@inertiajs/inertia-react";

export default function SidebarLink({
    href,
    active = false,
    image,
    label,
    badge = null,
    sidebarSmall = false,
    badgeColor = "#FF5050"
}) {
    return (
        <Link
            href={href}
            className={`px-4 py-3 flex items-center ${badge && 'justify-between'} rounded-md text-gray-600 group ${active && "bg-[#55a8ef1f]"
                }`}
        >
            <div className="flex items-center  space-x-4 ">
                <img src={image} className="w-4" />
                {
                    !sidebarSmall && (
                        <span
                            className={`text-[#464646] font-medium text-xs ${!active && "opacity-60"
                                }`}
                        >
                            {label}
                        </span>
                    )
                }
            </div>

            {badge && !sidebarSmall && (
                <span style={{backgroundColor : badgeColor}} className={`text-white text-[8px] rounded-[32px] px-2 py-1`}>
                    {badge}
                </span>
            )}
        </Link>
    );
}
