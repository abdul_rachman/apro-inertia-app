import React from 'react'

export default function RadioInput({label, value, ...props}) {
  return (
    <div class="flex items-center">
      <input
        type="radio"
        value={value}
        name="default-radio"
        className="w-4 h-4 text-blue-600 bg-gray-100 border-gray-300 focus:ring-blue-500 opacity-80"
        {...props}
      />
      <label for="default-radio-1" class="ml-2 text-sm font-medium text-[#2A2A2A]">
        {label}
      </label>
    </div>
  )
}
