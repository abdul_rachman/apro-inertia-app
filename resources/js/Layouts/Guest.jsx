import React from 'react';
import ApplicationLogo from '@/Components/ApplicationLogo';
import { Link } from '@inertiajs/inertia-react';

export default function Guest({ children }) {
    return (
        <div className="min-h-screen flex flex-col sm:justify-center items-center pt-6 sm:pt-0 bg-gray-100">
            <div className="fixed bottom-0 left-0">
                <img src="/images/background1.png" />
            </div>
            <div className="fixed bottom-0 right-0">
                <img src="/images/background2.png" />
            </div>
            <div className="text-center mb-5">
                <Link href="/">
                  <img src="/images/logo.png" alt="APPRO" width="200" />
                   {/* <h1 className="font-montserrat text-[#2A2A2A] text-5xl">APRO</h1>
                   <span className="text-[#89A3BB]">An Integrated Audit Process Tools</span> */}
                </Link>
            </div>

            <div className="w-full sm:max-w-[400px] mt-6 p-[30px] bg-white overflow-hidden rounded-[20px] border border-[#E6E6E8] z-10">
                {children}
            </div>
        </div>
    );
}
