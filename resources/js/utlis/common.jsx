/** Move the cursor of given input element to the input's end */
import * as Formula from "@/Components/tables/formula"

export function moveCursorToEnd(el) {
    el.selectionStart = el.selectionEnd = el.value.length;
}
/** Returns whether given cell contains a formula value */
export function isFormulaCell(
    cell
) {
    return Formula.isFormulaValue(cell.value);
}

export function getFormulaComputedValue({
    cell,
    formulaParser,
}) {
    const formula = Formula.extractFormula(cell.value);
    const { result, error } = formulaParser.parse(formula);
    return error || result;
}

/** Get the computed value of a cell. */
export function getComputedValue({
    cell,
    formulaParser,
}) {
    if (cell === undefined) {
        return null;
    }
    if (isFormulaCell(cell)) {
        return getFormulaComputedValue({ cell, formulaParser });
    }
    return cell.value;
}

export function formatRupiah(number) {
    return new Intl.NumberFormat("id-ID", {
        style: "currency",
        currency: "IDR"
    }).format(number);
}