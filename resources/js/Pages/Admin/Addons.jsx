import React from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import DataTable from "react-data-table-component";
import HeaderTable from "@/Components/clients/HeaderTable";

const columns = [
    {
        name: "NO",
        selector: (row) => row.id,
    },
    {
        name: "Name",
        selector: (row) => row.name,
    },
    {
        name: "Price",
        selector: (row) => row.price,
    },
    {
        name: "Descriptions",
        selector: (row) => row.description,
    },
    {
        name: "Unit",
        selector: (row) => row.unit,
    },
];

const customStyle = {
    headCells: {
        style: {
            background: "linear-gradient(180deg, #00CB75 0%, #02A962 100%)",
            color: "#FFFFFF",
            fontSize: 10,
            fontWeight: 700,
        },
    },
};


export default function DatabaseBackup({ addons, ...props }) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current="Addons" />
            }
        >
            <Head title="Addons" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="Addons"
                            textButton="Add Addons"
                            onClick={() => { }}
                        />
                        <div className="">
                            <DataTable
                                columns={columns}
                                customStyles={customStyle}
                                data={addons.data}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
