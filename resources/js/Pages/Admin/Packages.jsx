import React, { useState, useEffect, useLayoutEffect } from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import DataTable from "react-data-table-component";
import HeaderTable from "@/Components/clients/HeaderTable";
import Modal from "@/Components/clients/Modal";
import TabModal from "@/Components/clients/TabModal";

const columns = [
    {
        name: "NO",
        selector: (row) => row.id,
    },
    {
        name: "Name",
        selector: (row) => row.name,
    },
    {
        name: "Description",
        selector: (row) => row.description,
    },
    {
        name: "Max User",
        selector: (row) => row.max_user,
    },
    {
        name: "Storage",
        selector: (row) => row.storage,
    },
    {
        name: "Price",
        selector: (row) => row.price,
    },
];

const customStyle = {
    headCells: {
        style: {
            background: "linear-gradient(180deg, #00CB75 0%, #02A962 100%)",
            color: "#FFFFFF",
            fontSize: 10,
            fontWeight: 700,
        },
    },
};

export default function Packages({ packages, ...props }) {
    const [isOpen, setIsOpen] = useState(false)
    const [selectedPackage, setSelectedPackage] = useState({})

    function closeModal() {
        setIsOpen(false)
    }

    function openModal() {
        setIsOpen(true)
    }

    useLayoutEffect(() => {
        console.log("test")
        const tableItem = document.querySelectorAll('.rdt_TableRow')

        tableItem.forEach(el => el.addEventListener('contextmenu', event => {
            event.preventDefault()
            const id = event.target.parentNode.getAttribute("id")
            let idPackage = null
            if(id.includes("row")){
                idPackage = id.split("-")[1]
            }else{
                idPackage = id.split("-")[2]
            }
            if(idPackage){
                setSelectedPackage(packages.data.find(item => item.id === Number(idPackage))) 
                openModal()
            }
        }))
    }, [])

    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current="Packages" />
            }
        >
            <Head title="Packages" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="Packages"
                            textButton="Add Packages"
                            onClick={() => openModal()}
                        />
                        <div className="">
                            <DataTable
                                columns={columns}
                                customStyles={customStyle}
                                data={packages.data}
                            />
                        </div>
                    </div>
                </div>
                <Modal open={isOpen} onClose={closeModal}>
                    <div className="">
                        <div className="flex justify-between">
                            <h2 className="text-lg font-semibold">Package</h2>
                            <button
                                className="w-6 h-6"
                                onClick={closeModal}
                                as="button"
                            >
                                <svg
                                    width="14"
                                    height="14"
                                    viewBox="0 0 14 14"
                                    fill="none"
                                    xmlns="http://www.w3.org/2000/svg"
                                >
                                    <path
                                        d="M13.295 0.705C12.9056 0.315639 12.2744 0.315639 11.885 0.705L7 5.59L2.115 0.705C1.72564 0.315639 1.09436 0.315639 0.705 0.705C0.315639 1.09436 0.315639 1.72564 0.705 2.115L5.59 7L0.705 11.885C0.315639 12.2744 0.315639 12.9056 0.705 13.295C1.09436 13.6844 1.72564 13.6844 2.115 13.295L7 8.41L11.885 13.295C12.2744 13.6844 12.9056 13.6844 13.295 13.295C13.6844 12.9056 13.6844 12.2744 13.295 11.885L8.41 7L13.295 2.115C13.6844 1.72564 13.6844 1.09436 13.295 0.705Z"
                                        fill="#2A2A2A"
                                    />
                                </svg>
                            </button>
                        </div>
                        <div className="">
                            {selectedPackage.name}
                        </div>
                    </div>
                </Modal>
            </div>
        </Authenticated>
    );
}
