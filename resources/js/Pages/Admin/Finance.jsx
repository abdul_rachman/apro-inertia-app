import React from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import DataTable from "react-data-table-component";
import HeaderTable from "@/Components/clients/HeaderTable";

const columns = [
    {
        name: "NO",
        selector: (row) => row.id,
    },
    {
        name: "Date",
        selector: (row) => row.created_at,
    },
    {
        name: "Invoice Number",
        selector: (row) => row.invoice_number,
    },
    {
        name: "Name",
        selector: (row) => row.company_name,
    },
    {
        name: "NPWP",
        selector: (row) => row.tax_id_number,
    },
    {
        name: "Periode",
        selector: (row) => row.period,
    },
    {
        name: "Package Name",
        selector: (row) => row.package_name,
    },
    {
        name: "Package Quantity",
        selector: (row) => row.package_qty,
    },
    {
        name: "Package Amount",
        selector: (row) => row.package_amount,
    },
    {
        name: "Add User (Quantity)",
        selector: (row) => row.add_on_1_qty,
    },
    {
        name: "Add User (Amount)",
        selector: (row) => row.add_on_1_amt,
    },
    {
        name: "Add Storage (Quantity)",
        selector: (row) => row.add_on_2_qty,
    },
    {
        name: "Add Storage (Amount)",
        selector: (row) => row.add_on_2_amt,
    },
    {
        name: "Add Timesheet (Quantity)",
        selector: (row) => row.add_on_3_qty,
    },
    {
        name: "Add Timesheet (Amount)",
        selector: (row) => row.add_on_3_amt,
    },
    {
        name: "Add PCC (Quantity)",
        selector: (row) => row.add_on_4_qty,
    },
    {
        name: "Add PCC (Amount)",
        selector: (row) => row.add_on_4_amt,
    },
    {
        name: "STATUS",
        selector: (row) => row.status,
    },
];

const data = [
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
    {
        id: 1,
        id_db: "23369874",
        nama_kap: "KAP Indonesia",
        npwp: "101058348365",
        tgl_update: "July 14, 2015",
        jam: "9:45",
        size: "341.4 Mb",
        total_client: "540 client",
        status: "complete",
    },
    
];

const customStyle = {
    headCells: {
        style: {
            background: "linear-gradient(180deg, #00CB75 0%, #02A962 100%)",
            color: "#FFFFFF",
            fontSize: 10,
            fontWeight: 700,
        },
    },
};


export default function DatabaseBackup({ transactions, ...props}) {
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current="Database Backup" />
            }
        >
            <Head title="Finance" />

            <div className="w-full">
                <div className="flex space-x-8">
                <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="Finance"
                            textButton="Export"
                            onClick={() => {}}
                        />
                        <div className="">
                            <DataTable
                                columns={columns}
                                customStyles={customStyle}
                                data={transactions}
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
