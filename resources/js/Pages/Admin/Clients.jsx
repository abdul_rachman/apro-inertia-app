import React from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head, Link } from "@inertiajs/inertia-react";
import DataTable from "react-data-table-component";
import HeaderTable from "@/Components/clients/HeaderTable";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import { Inertia } from "@inertiajs/inertia";

const columnsNew = [
    {
        name: "NO",
        selector: (row) => row.id,
    },
    {
        name: "NAMA KAP",
        selector: (row) => row.user.name,
    },
    {
        name: "NPWP",
        selector: (row) => row.tax_id_number,
    },
    {
        name: "NOMOR NIB",
        selector: (row) => row.business_id_number,
    },
    {
        name: "ACTION",
        selector: (row) => (
            <Link href={ route("admin.clients.show",{company : row.id}) } className="bg-[#ECEFF7] p-2 rounded-lg text-[#464646] flex gap-2 text-xs">
                <img src="/images/small_folder.png" alt="View Document" />
                <span>View Document</span>
            </Link>
        ),
    },
]

const columns = [
    {
        name: "NO",
        selector: (row) => row.id,
    },
    {
        name: "TANGGAL REGISTRASI",
        selector: (row) => row.created_at,
    },
    {
        name: "NAMA KAP",
        selector: (row) => row.user.name,
    },
    {
        name: "NPWP",
        selector: (row) => row.tax_id_number,
    },
    {
        name: "NOMOR NIB",
        selector: (row) => row.business_id_number,
    },
    {
        name: "TANGGAL NIB",
        selector: (row) => row.business_id_date,
    },
    {
        name: "NO. IJIN USAHA KAP",
        selector: (row) => row.business_license_number,
    },
    {
        name: "TANGGAL IJIN USAHA KAP",
        selector: (row) => row.business_license_date,
    },
    {
        name: "ALAMAT KAP",
        selector: (row) => row.address,
    },
    {
        name: "NO. TELEPON",
        selector: (row) => row.phone,
    },
    {
        name: "WEBSITE KAP",
        selector: (row) => row.website,
    },
    {
        name: "CONTACT PERSON",
        selector: (row) => row.pic_name,
    },
    {
        name: "STATUS",
        selector: (row) => row.user.status,
    },
];

const customStyle = {
    headCells: {
        style: {
            background: "linear-gradient(180deg, #00CB75 0%, #02A962 100%)",
            color: "#FFFFFF",
            fontSize: 10,
            fontWeight: 700,
        },
    },
};

export default function Clients({ new_companies, companies, ...props }) {
    const handlePageChange = page => {
        Inertia.get(route('admin.clients.index'), { page })
    };
    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb current="User List" />
            }
        >
            <Head title="User list" />

            <div className="w-full">
                <div className="flex flex-col gap-5">

                    <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="New Request KAP"
                            badge="3"
                        />
                        <div className="">
                            <DataTable
                                columns={columnsNew}
                                customStyles={customStyle}
                                data={new_companies}
                                noTableHead={true}
                            />
                            <div className="text-center py-8">
                                <p className="text-[#1989E9]">Before you continue, please make sure all of the document is completely correct.</p>
                            </div>
                        </div>
                    </div>

                    <div className="border border-[#E6E6E8] rounded-2xl w-full bg-white shadow-md">
                        <HeaderTable
                            title="Users"
                        />
                        <div className="">
                            <DataTable
                                columns={columns}
                                customStyles={customStyle}
                                data={companies.data}
                                pagination
                                paginationServer
                                paginationTotalRows={companies.total}
                                paginationPerPage={companies.per_page}
                                onChangePage={handlePageChange}
                                paginationDefaultPage={companies.current_page}
                                paginationComponentOptions={{
                                    noRowsPerPage: true
                                }}
                            />

                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
