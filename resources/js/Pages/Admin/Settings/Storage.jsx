import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Storage(props) {
    return (
        <WrapperSetting {...props} title="Storage" back={route('admin.settings.index')} >
            <div className="flex-1 bg-white p-5">
                <div className="flex justify-between text-[#464646] text-xs items-center">
                    <span>Cloud Storage</span>
                    <span>40 GB of 120 GB Used</span>
                </div>
                <div className="bg-[#F5F5F5] rounded-lg overflow-hidden flex h-10 w-full mt-4">
                    <div className="w-[100px] bg-[#FF002E]" />
                    <div className="w-[60px] bg-[#1370FCCC]" />
                    <div className="w-[27px] bg-[#B0B0B0]" />
                    <div className="w-[18px] bg-[#3CD905]" />
                </div>
                <div className="flex gap-4 text-xs items-center mt-3">
                    <div className="flex gap-2 items-center">
                        <div className="w-2 h-2 bg-[#FF002E]" />
                        <span>Project</span>
                    </div>
                    <div className="flex gap-2 items-center">
                        <div className="w-2 h-2 bg-[#1370FCCC]" />
                        <span>Messages</span>
                    </div>
                    <div className="flex gap-2 items-center">
                        <div className="w-2 h-2 bg-[#B0B0B0]" />
                        <span>Media</span>
                    </div>
                    <div className="flex gap-2 items-center">
                        <div className="w-2 h-2 bg-[#3CD905]" />
                        <span>Photo</span>
                    </div>
                    <div className="flex">
                        <span>Calculating...</span>
                    </div>
                </div>
            </div>
        </WrapperSetting>
    );
}
