import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";

export default function ControlRisk({ settings, ...props }) {
    const [editable, setEditable] = useState(false)
    const [editable1, setEditable1] = useState(false)
    const [editable2, setEditable2] = useState(false)

    const [currentData, setCurrentData] = useState(settings)

    const handleChange = (key1, value) => {
        const temp = {...currentData}
        temp[key1] = value
        setCurrentData( temp )
    }

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            let params = {
                type: 'level',
                control_risk_low: currentData.control_risk_low,
                control_risk_moderate: currentData.control_risk_moderate,
                control_risk_high: currentData.control_risk_high,
            }
            Inertia.post(route('admin.settings.control-risk.update'), params)
            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    const handleSaveEdit1 = () => {
        if (editable1) {
            // Update Data
            let params = {
                type: 'design',
                control_design_yes: currentData.control_design_yes,
                control_design_partial: currentData.control_design_partial,
                control_design_no: currentData.control_design_no,
            }
            Inertia.post(route('admin.settings.control-risk.update'), params)
            setEditable1(false)
        } else {
            // local update
            setEditable1(true)
        }
    }

    const handleSaveEdit2 = () => {
        if (editable2) {
            // Update Data
            let params = {
                type: 'implementation',
                control_implementation_yes: currentData.control_implementation_yes,
                control_implementation_partial: currentData.control_implementation_partial,
                control_implementation_no: currentData.control_implementation_no,
            }
            Inertia.post(route('admin.settings.control-risk.update'), params)
            setEditable2(false)
        } else {
            // local update
            setEditable2(true)
        }
    }

    return (
        <WrapperSetting {...props} title="Control Risk" back={route('admin.settings.index')} >
            <div className="flex gap-4">
                <div className="flex-1">
                    <div className="rounded-lg overflow-hidden w-full">
                        <ItemTableHeader header={true} text1="CONTROL RISK LEVEL" text2="FACTOR" />
                        <ItemTableHeader text1="Low" text2={currentData.control_risk_low} editable={editable} onChange={ (e) => handleChange('control_risk_low',e.target.value) } type="input" />
                        <ItemTableHeader text1="Moderate" text2={currentData.control_risk_moderate} onChange={ (e) => handleChange('control_risk_moderate',e.target.value) } editable={editable} type="input" />
                        <ItemTableHeader text1="High" text2={currentData.control_risk_high} onChange={ (e) => handleChange('control_risk_high',e.target.value) } editable={editable} type="input" />
                    </div>
                    <div className="mt-5 flex gap-3 text-xs">
                        <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                        <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
                    </div>
                </div>

                <div className="flex-1">
                    <div className="rounded-lg overflow-hidden w-full">
                        <ItemTableHeader header={true} text1="TYPE OF CONTROL" text2="CODE" />
                        <ItemTableHeader text1="Prevent" text2="P" />
                        <ItemTableHeader text1="Detect" text2="D" />
                    </div>
                </div>
            </div>
            <div className="flex gap-4 mt-10">
                <div className="w-[500px]">
                        <div className="rounded-lg overflow-hidden w-full">
                            <ItemTableHeader header={true} text1="CONTROL DESIGN" text2="FACTOR" />
                            <ItemTableHeader text1="Yes" text2={currentData.control_design_yes} onChange={ (e) => handleChange('control_design_yes',e.target.value) }  editable={editable1} type="input" />
                            <ItemTableHeader text1="Partial" text2={currentData.control_design_partial} onChange={ (e) => handleChange('control_design_partial',e.target.value) } editable={editable1} type="input" />
                            <ItemTableHeader text1="No" text2={currentData.control_design_no} onChange={ (e) => handleChange('control_design_no',e.target.value) } editable={editable1} type="input" />
                        </div>
                        <div className="mt-5 flex gap-3 text-xs">
                            <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                            <button onClick={handleSaveEdit1} className={`${editable1 ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable1 ? "Save" : "Edit"}</button>
                        </div>
                    </div>
                    <div className="w-[500px]">
                        <div className="rounded-lg overflow-hidden w-full">
                            <ItemTableHeader header={true} text1="CONTROL IMPLEMENTATION" text2="FACTOR" />
                            <ItemTableHeader text1="Yes" text2={currentData.control_implementation_yes} editable={editable2} onChange={ (e) => handleChange('control_implementation_yes',e.target.value) } type="input" />
                            <ItemTableHeader text1="Partial" text2={currentData.control_implementation_partial} editable={editable2} onChange={ (e) => handleChange('control_implementation_partial',e.target.value) } type="input" />
                            <ItemTableHeader text1="No" text2={currentData.control_implementation_no} editable={editable2} onChange={ (e) => handleChange('control_implementation_no',e.target.value) } type="input" />
                        </div>
                        <div className="mt-5 flex gap-3 text-xs">
                            <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                            <button onClick={handleSaveEdit2} className={`${editable2 ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable2 ? "Save" : "Edit"}</button>
                        </div>
                    </div>
            </div>


        </WrapperSetting>
    );
}

const ItemTableHeader = ({ text1, text2, type = "text", onChange = undefined, editable = false, header = false, bg = false }) => {
    return (
        <div className="flex gap-2 mb-1 items-center">
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} flex-1 text-xs p-3 ${header ? "text-white" : "text-[#333]"}  ${header && "text-center"}`}>{text1}</div>
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} w-[200px] text-xs p-3 ${header ? "text-white" : "text-[#333]"} text-center`}>
                {
                    type === "input" && editable ?
                        (<input type="text" value={text2} readOnly={!editable} className={`w-full focus:outline-none text-xs`} onChange={onChange} />)
                        :
                        text2
                }
            </div>
        </div>
    )
}
