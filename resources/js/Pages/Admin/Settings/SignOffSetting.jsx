import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";
import { ListItemLink } from "@/Components/settings";

export default function SignOffSetting({ settings, ...props }) {

    return (
        <WrapperSetting {...props} title="Sign Off Setting" back={route('admin.settings.index')} >
            <div className="mb-10">
                <h2 className="text-[#46464663] text-sm font-medium my-4 ml-5">MAIN WORKSPACE MENU</h2>
                <ul>
                    <ListItemLink title={"A  Pre-engagement"} href={route('admin.settings.sign-off-setting.pre-engagement')} />
                    <ListItemLink title={"B  Identify and Asses Risk"} href={route('admin.settings.sign-off-setting.identify-and-assess-risk')} />
                    <ListItemLink title={"C  Design Audit Response"} href={route('admin.settings.sign-off-setting.design-audit-response')} />
                    <ListItemLink title={"D  Obtain Audit Evidance"} href={route('admin.settings.sign-off-setting.obtain-audit-evidance')} />
                    <ListItemLink borderBottom={false} title={"E  Completion and Reporting"} href={route('admin.settings.sign-off-setting.completion-and-reporting')} />
                </ul>
            </div>
            <div className="">
                <h2 className="text-[#46464663] text-sm font-medium my-4 ml-5">SIGN OFF TYPE</h2>
                <ul className="w-[200px] rounded-md overflow-hidden text-xs font-medium">
                    <li className="bg-white pl-3"><span className="block border-b border-[#E6E6E8] p-3 text-[#464646]">Ap   : Approver</span></li>
                    <li className="bg-white pl-3"><span className="block border-b border-[#E6E6E8] p-3 text-[#464646]">Rv   : Reviewer</span></li>
                    <li className="bg-white pl-3"><span className="block border-b border-[#E6E6E8] p-3 text-[#464646]">Ck   : Checker</span></li>
                    <li className="bg-white pl-3"><span className="block border-none border-[#E6E6E8] p-3 text-[#464646]">Pr   : Preparer</span></li>
                </ul>
            </div>

        </WrapperSetting>
    );
}
