import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";
import Swal from 'sweetalert2'

const dummyData = [
    {
        id: 1,
        parent: 'Profit based entities',
        label: 'Profit before tax',
        min: 3,
        max: 10
    },
    {
        id: 2,
        parent: '',
        label: 'Revenue',
        min: 0.5,
        max: 2
    },
    {
        id: 3,
        parent: 'Asset based or investment entities',
        label: 'Total assets',
        min: 1,
        max: 2
    },
    {
        id: 4,
        parent: '',
        label: 'Net assets ',
        min: 2,
        max: 5
    },
    {
        id: 5,
        parent: 'Not-for-profit entities',
        label: 'Gross income',
        min: 0.5,
        max: 1
    },
    {
        id: 6,
        parent: '',
        label: 'Expenditure',
        min: 0.5,
        max: 1
    },
]

export default function Materiality({ settings, ...props }) {
    const [editable, setEditable] = useState(false)
    const [currentData, setCurrentData] = useState(settings)

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            setEditable(false)

            let params = {}
            currentData.forEach((item) => {
                params[item.id] = {
                    min: item.min,
                    max: item.max
                }
            })
            Inertia.post(route('admin.settings.materiality.update'), params);
        } else {
            // local update
            setEditable(true)
        }
    }

    const handleChange = (key, value, id) => {
        const newArr = currentData.map(object => {
            if (object.id === id) {
                return { ...object, [key]: value };
            }
            return object;
        });
        setCurrentData(newArr)
    }

    const handleReset = () => {handleSaveEdit
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, reset it!'
        }).then((result) => {
            if (result.isConfirmed) {
                setCurrentData(settings)
            }
        })
        
    }

    return (
        <WrapperSetting {...props} title="Materiality" back={route('admin.settings.index')} >
            <div className="flex">
                <div className="flex-1">
                    <table class="table-auto border-separate  w-full text-xs text-[#464646]">
                        <thead className="bg-[#1088e459] text-white text-center">
                            <tr>
                                <th className="p-2">NATURE OF THE ENTITY</th>
                                <th>BENCHMARKS</th>
                                <th colspan="2" className="border-spacing-x-10">% OF MATERIALITY</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                currentData.map((item, index) => (
                                    <tr key={`item-${index}`}>
                                        {
                                            item?.parent !== "" && (
                                                <td rowspan="2" className="p-2 border-l border-b border-[#ECEFF7] bg-white">{item?.parent}</td>
                                            )
                                        }
                                        <td className="p-2 border-l border-b border-[#ECEFF7] bg-white">{item?.label}</td>
                                        <td className="p-2 border-l border-b border-[#ECEFF7] bg-white">
                                            {
                                                editable ? (
                                                    <>
                                                        Min : <input type="text" onChange={(e) => handleChange("min", e.target.value, item?.id)} className="w-[70px]" value={item?.min} />
                                                    </>
                                                ) : (`Min:${item?.min}%`)
                                            }
                                        </td>
                                        <td className="p-2 border-l border-b border-r border-[#ECEFF7] bg-white ">
                                            {
                                                editable ? (
                                                    <>
                                                        Max : <input type="text" onChange={(e) => handleChange("max", e.target.value, item?.id)} className="w-[70px]" value={item?.max} />
                                                    </>
                                                ) : (`Max:${item?.max}%`)
                                            }
                                        </td>
                                    </tr>
                                ))
                            }
                        </tbody>
                    </table>
                    <div className="mt-5 flex gap-3 text-xs">
                        <button onClick={handleReset} className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                        <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
                    </div>
                </div>
            </div>
        </WrapperSetting>
    );
}