import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";

export default function ConfidenceLevel({ settings, ...props }) {
    const [editable, setEditable] = useState(false)
    const [currentData, setCurrentData] = useState(settings)

    const handleChange = (key1, key2, value) => {
        const temp = {...currentData}
        temp[key1][key2] = value
        setCurrentData( temp )
    }

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            Inertia.post(route('admin.settings.confidence-level.update'), currentData)
            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    return (
        <WrapperSetting {...props} title="Confidence Level" back={route('admin.settings.index')} >
            <div className="w-[1000px] m-auto">
                <div className="flex gap-3">
                    <div className="bg-[#1088e459] w-[500px] text-white p-3 uppercase">RISK REDUCTION REQUIRED</div>
                    <div className="bg-[#1088e459] flex-1 text-white p-3 uppercase">CONFIDENCE LEVEL  (%)</div>
                    <div className="bg-[#1088e459] flex-1 text-white p-3 uppercase">CONFIDENCE FACTOR (C.F)</div>
                </div>

                <ItemInput 
                    className="bg-[#F5FFF5]" 
                    text1="Very Low" 
                    text2={currentData.very_low_level_1.level} 
                    text3={currentData.very_low_level_1.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('very_low_level_1','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('very_low_level_1','factor', e.target.value)}
                    />
                <ItemInput 
                    className="bg-[#EBFFEB]" 
                    text1="Very Low" 
                    text2={currentData.very_low_level_2.level} 
                    text3={currentData.very_low_level_2.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('very_low_level_2','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('very_low_level_2','factor', e.target.value)}
                />
                <ItemInput 
                    className="bg-[#E1FEE1]" 
                    text1="Very Low" 
                    text2={currentData.very_low_level_3.level} 
                    text3={currentData.very_low_level_3.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('very_low_level_3','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('very_low_level_3','factor', e.target.value)}
                />

                <ItemInput 
                    className="bg-[#DAFED9]" 
                    text1="Low" 
                    text2={currentData.low_level_1.level} 
                    text3={currentData.low_level_1.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('low_level_1','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('low_level_1','factor', e.target.value)}
                />

                <ItemInput 
                    className="bg-[#CDFECC]" 
                    text1="Low" 
                    text2={currentData.low_level_2.level} 
                    text3={currentData.low_level_2.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('low_level_2','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('low_level_2','factor', e.target.value)}
                />

                <ItemInput 
                    className="bg-[#B5FEB3]" 
                    text1="Low" 
                    text2={currentData.low_level_3.level} 
                    text3={currentData.low_level_3.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('low_level_3','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('low_level_3','factor', e.target.value)}
                />

                <ItemInput 
                    className="bg-[#FFF3CA]" 
                    text1="Moderate" 
                    text2={currentData.moderate_level_1.level} 
                    text3={currentData.moderate_level_1.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('moderate_level_1','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('moderate_level_1','factor', e.target.value)}
                />
                <ItemInput 
                    className="bg-[#FFF0B9]" 
                    text1="Moderate" 
                    text2={currentData.moderate_level_2.level} 
                    text3={currentData.moderate_level_2.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('moderate_level_2','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('moderate_level_2','factor', e.target.value)}
                />
                <ItemInput 
                    className="bg-[#FFECA7]" 
                    text1="Moderate" 
                    text2={currentData.moderate_level_3.level} 
                    text3={currentData.moderate_level_3.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('moderate_level_3','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('moderate_level_3','factor', e.target.value)}
                />

                <ItemInput 
                    className="bg-[#FFB0B0]" 
                    text1="High" 
                    text2={currentData.high_level_1.level} 
                    text3={currentData.high_level_1.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('high_level_1','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('high_level_1','factor', e.target.value)}
                />
                <ItemInput 
                    className="bg-[#FF9696]" 
                    text1="High" 
                    text2={currentData.high_level_2.level} 
                    text3={currentData.high_level_2.factor} 
                    editable={editable}
                    onChangeText2={ (e) => handleChange('high_level_2','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('high_level_2','factor', e.target.value)} 
                />
                <ItemInput 
                    className="bg-[#FF7C7C]" 
                    text1="High" 
                    text2={currentData.high_level_3.level} 
                    text3={currentData.high_level_3.factor} 
                    editable={editable} 
                    onChangeText2={ (e) => handleChange('high_level_3','level', e.target.value)}
                    onChangeText3={ (e) => handleChange('high_level_3','factor', e.target.value)}
                />

                <div className="mt-5 flex gap-3 text-xs">
                    <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                    <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
                </div>

            </div>
        </WrapperSetting>
    );
}

const ItemInput = ({ className = "", text1, text2, text3, editable = false, onChangeText2, onChangeText3 }) => {
    return (
        <div className="flex gap-3">
            <div className={`${className} w-[500px] text-[#464646BF] p-3`}>
                {
                    text1
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] p-3`}>
                {
                    editable ? (
                        <input type="text" value={text2} onChange={onChangeText2} />
                    ) : (text2 + '%')
                }
            </div>
            <div className={`${className} flex-1 text-[#464646BF] p-3`}>
                {
                    editable ? (
                        <input type="text" value={text3} onChange={onChangeText3} />
                    ) : text3
                }
            </div>
        </div>
    )
}