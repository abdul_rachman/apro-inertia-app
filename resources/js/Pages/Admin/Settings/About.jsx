import React from "react";
import { ListItemInput, ListItemSpace } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";
import Button from "@/Components/Button";
import { useForm } from "@inertiajs/inertia-react";

export default function About({ settings, ...props }) {
    const { data, setData, post, processing, errors } = useForm({
        app_name: settings.app_name,
        software_version: settings.software_version,
        model_name: settings.model_name,
        security_version: settings.security_version,
        security_expirate_date: settings.security_expirate_date
    })

    const submit = () => {
        if (
            data.app_name &&
            data.software_version &&
            data.model_name &&
            data.security_version &&
            data.security_expirate_date
        ) {
            post(route('admin.settings.about.update'), data);
        }
    }

    return (
        <WrapperSetting {...props} title="About" back={route('admin.settings.index')} >
            <ul>
                <ListItemInput title="Name" value={data.app_name} onChange={e => setData('app_name', e.target.value)} roundedTop={true} />
                <ListItemInput title="Software Version" value={data.software_version} onChange={e => setData('software_version', e.target.value)} />
                <ListItemInput title="Model Name" value={data.model_name} onChange={e => setData('model_name', e.target.value)} borderBottom={false} roundedBottom={true} />
                <ListItemSpace />
                <ListItemInput title="Security Version" value={data.security_version} onChange={e => setData('security_version', e.target.value)} roundedTop={true} />
                <ListItemInput title="Expirate Date" value={data.security_expirate_date} onChange={e => setData('security_expirate_date', e.target.value)} borderBottom={false} roundedBottom={true} />
            </ul>
            <div className="flex justify-end mt-7">
                <Button className="w-[150px] p-0 text-xs h-[30px]" onClick={submit}>Save</Button>
            </div>
        </WrapperSetting >
    );
}
