import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function PreEngagement({ settings, ...props }) {
    const [editable, setEditable] = useState(false)

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    const setValue = (value) => {
        let result = 'N/A'
        if (value == 1) {
            result = 'Yes'
        } else if (value == 0) {
            result = 'No'
        }

        return result
    }

    return (
        <WrapperSetting {...props} title="A  Pre-engagement" back={route('admin.settings.sign-off-setting')} >
            <div className="w-full m-auto bg-white">
                <ItemInput column1="User Type" column2={"A.1"} column3="A.2" column4={"A.3"} column5="A.4" column6="A.5" column7="A.6" column8="CWS" editable={editable} header={true} />
                <ItemInput column1="Auditor" column2={setValue(settings[1][0].has_permission)} column3={setValue(settings[1][1].has_permission)} column4={setValue(settings[1][2].has_permission)} column5={setValue(settings[1][3].has_permission)} column6={setValue(settings[1][4].has_permission)} column7={setValue(settings[1][5].has_permission)} column8={setValue(settings[1][6].has_permission)} editable={editable} />
                <ItemInput column1="Manager" column2={setValue(settings[2][0].has_permission)} column3={setValue(settings[2][1].has_permission)} column4={setValue(settings[2][2].has_permission)} column5={setValue(settings[2][3].has_permission)} column6={setValue(settings[2][4].has_permission)} column7={setValue(settings[2][5].has_permission)} column8={setValue(settings[2][6].has_permission)} editable={editable} />
                <ItemInput column1="Partner" column2={setValue(settings[3][0].has_permission)} column3={setValue(settings[3][1].has_permission)} column4={setValue(settings[3][2].has_permission)} column5={setValue(settings[3][3].has_permission)} column6={setValue(settings[3][4].has_permission)} column7={setValue(settings[3][5].has_permission)} column8={setValue(settings[3][6].has_permission)} editable={editable} />
                <ItemInput column1="EQCR Partner" column2={setValue(settings[4][0].has_permission)} column3={setValue(settings[4][1].has_permission)} column4={setValue(settings[4][2].has_permission)} column5={setValue(settings[4][3].has_permission)} column6={setValue(settings[4][4].has_permission)} column7={setValue(settings[4][5].has_permission)} column8={setValue(settings[4][6].has_permission)} editable={editable} />
                <ItemInput column1="Managing Partner" column2={setValue(settings[5][0].has_permission)} column3={setValue(settings[5][1].has_permission)} column4={setValue(settings[5][2].has_permission)} column5={setValue(settings[5][3].has_permission)} column6={setValue(settings[5][4].has_permission)} column7={setValue(settings[5][5].has_permission)} column8={setValue(settings[5][6].has_permission)} editable={editable} />
                <ItemInput column1="Risk Management" column2={setValue(settings[6][0].has_permission)} column3={setValue(settings[6][1].has_permission)} column4={setValue(settings[6][2].has_permission)} column5={setValue(settings[6][3].has_permission)} column6={setValue(settings[6][4].has_permission)} column7={setValue(settings[6][5].has_permission)} column8={setValue(settings[6][6].has_permission)} editable={editable} />
                <ItemInput column1="Auditor Expert Partner" column2={setValue(settings[7][0].has_permission)} column3={setValue(settings[7][1].has_permission)} column4={setValue(settings[7][2].has_permission)} column5={setValue(settings[7][3].has_permission)} column6={setValue(settings[7][4].has_permission)} column7={setValue(settings[7][5].has_permission)} column8={setValue(settings[7][6].has_permission)} editable={editable} />
                <ItemInput column1="Guess" column2={setValue(settings[8][0].has_permission)} column3={setValue(settings[8][1].has_permission)} column4={setValue(settings[8][2].has_permission)} column5={setValue(settings[8][3].has_permission)} column6={setValue(settings[8][4].has_permission)} column7={setValue(settings[8][5].has_permission)} column8={setValue(settings[8][6].has_permission)} editable={editable} />
            </div>
        </WrapperSetting>
    );
}

const ItemInput = ({ className = "", column1, column2, column3, column4, column5, column6, column7, column8, editable = false, header = false }) => {
    return (
        <div className="flex gap-1">
            <div className={`${className} w-[500px] ${header ? "text-white bg-[#1088e459] uppercase" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"}  p-3`}>
                {
                    column1
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column2}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column2}</span>
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column3}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column3}</span>
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column4}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column4}</span>
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column5}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column5}</span>
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column6}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column6}</span>
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column7}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column7}</span>
                }
            </div>
            <div className={`${className} flex-1 ${header ? "text-white bg-[#1088e459]" : "text-[#464646BF] border-b-4 border-[#f3f4f694]"} p-3`}>
                {
                    editable ? (
                        <select className="w-full" defaultValue={column8}>
                            <option value="yes">Yes</option>
                            <option value="no">No</option>
                        </select>
                    ) : <span className="text-center block">{column8}</span>
                }
            </div>
        </div>
    )
}