import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";
import Modal from "@/Components/clients/Modal";
import Swal from 'sweetalert2'

export default function Risk({ risks, inherent_risks, ...props }) {
    const [editable, setEditable] = useState(false)
    const [editable1, setEditable1] = useState(false)
    const [open, setOpen] = useState(false)
    const [currentData, setCurrentData] = useState(inherent_risks)
    const [currentDataRisk, setCurrentDataRisk] = useState(risks)
    const [newInherentRisk, setNewInherentRisk] = useState('')
    const [newInherentSignificant, setNewInherentSignificant] = useState('1')

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            Inertia.post(route('admin.settings.risk.update'), currentDataRisk)
            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    const handleSaveEdit1 = () => {
        if (editable1) {
            // Update Data
            let params = {
                data: currentData
            }
            Inertia.post(route('admin.settings.risk.inherent.update'), params)
            setEditable1(false)
        } else {
            // local update
            setEditable1(true)
        }
    }

    const handleChange = (key, value, id) => {
        const newArr = currentData.map(object => {
            if (object.id === id) {
                return { ...object, ["is_significant"]: value === "Y" ? 1 : 0 };
            }
            return object;
        });
        setCurrentData(newArr)
    }

    const handleChangeRisk = (key1, value) => {
        const temp = {...currentDataRisk}
        temp[key1] = value
        setCurrentDataRisk( temp )
    }

    const handleDelete = (item) => {
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.isConfirmed) {
                const removeObject = currentData.findIndex(object => {
                    return object.id === item.id;
                });
                currentData.splice(removeObject, 1)
                const newData = [...currentData]

                let params = {
                    id: item.id
                }
                Inertia.post(route('admin.settings.risk.inherent.destroy'), params)

                setCurrentData(newData)
            }
        })
    }

    const handleAddData = () => {
        let params = {
            name: newInherentRisk,
            is_significant: newInherentSignificant
        }
        Inertia.post(route('admin.settings.risk.inherent.store'), params)

        setOpen(false)
    }

    return (
        <WrapperSetting {...props} title="Risk of Material Misstatement" back={route('admin.settings.index')} >
            <div className="rounded-lg overflow-hidden w-[600px]">
                <ItemTableHeader header={true} text1="RISK OF MATERIAL MISSTATEMENT LEVEL" text2="FACTOR" />
                <ItemTableHeader 
                    text1="Low" 
                    text2={currentDataRisk.risk_material_misstatement_level_low} 
                    onChange={ (e) => handleChangeRisk("risk_material_misstatement_level_low",e.target.value) } 
                    editable={editable} type="input" 
                />
                <ItemTableHeader 
                    text1="Moderate" 
                    text2={currentDataRisk.risk_material_misstatement_level_moderate} 
                    editable={editable} 
                    onChange={ (e) => handleChangeRisk("risk_material_misstatement_level_moderate",e.target.value) } 
                    type="input" 
                />
                <ItemTableHeader 
                    text1="High" 
                    text2={currentDataRisk.risk_material_misstatement_level_high} 
                    editable={editable} 
                    onChange={ (e) => handleChangeRisk("risk_material_misstatement_level_high",e.target.value) } 
                    type="input" 
                />
            </div>
            <div className="mt-5 flex gap-3 text-xs">
                <button className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
            </div>
            <div className="rounded-lg overflow-hidden w-full mt-10">
                <ItemTableHeaderWidthEdit header={true} text1="INHERENT RISK CONSIDERATION FACTOR (ISA 315. Par 28)" text2="SIGNIFICANT RISK (Y/N)" />
                {
                    currentData?.map((item, index) => (
                        <ItemTableHeaderWidthEdit
                            key={`item-${index}`}
                            number={index}
                            bg={item?.is_editable ? false : true}
                            text1={item?.name}
                            text2={item?.is_significant === 1 ? 'Y' : 'N'}
                            editable={item?.is_editable === 1 ? true : false}
                            item={item}
                            onDelete={handleDelete}
                            onChange={handleChange}
                            type={editable1 ? "input" : "text"}
                        />
                    ))
                }
            </div>
            <div className="mt-5 flex gap-3 text-xs">
                <button onClick={() => setOpen(true)} className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Add</button>
                <button onClick={handleSaveEdit1} className={`${editable1 ? "bg-[#00ff00e5]" : "bg-[#1088E4]"} py-2 px-4 w-[100px] rounded-md text-white`}>{editable1 ? "Save" : "Edit"}</button>
            </div>
            <Modal open={open} onClose={() => setOpen(false)}>
                <div className="">
                    <label>
                        INHERENT RISK CONSIDERATION FACTOR (ISA 315. Par 28)
                        <input type="text" className="w-full mb-4" onChange={(e) => setNewInherentRisk(e.target.value)} />
                    </label>
                    <label>
                        SIGNIFICANT RISK (Y/N)
                        <select className="w-full" onChange={(e) => setNewInherentSignificant(e.target.value)}>
                            <option value="1">Y</option>
                            <option value="0">N</option>
                        </select>
                    </label>
                    <div className="mt-5 flex gap-3 justify-end">
                        <button onClick={handleAddData} className="bg-[#1088E4] text-white py-2 px-4 w-[100px] rounded-md">Save</button>
                    </div>
                </div>
            </Modal>
        </WrapperSetting>
    );
}

const ItemTableHeader = ({ text1, text2, type = "text", onChange = undefined, editable = false, header = false, bg = false }) => {
    return (
        <div className="flex gap-2 mb-1 items-center">
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} flex-1 text-xs p-3 ${header ? "text-white" : "text-[#333]"}  ${header && "text-center"}`}>{text1}</div>
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} w-[200px] text-xs p-3 ${header ? "text-white" : "text-[#333]"} text-center`}>
                {
                    type === "input" && editable ?
                        (<input type="text" value={text2} readOnly={!editable} className={`w-full focus:outline-none text-xs`} onChange={onChange} />)
                        :
                        text2
                }
            </div>
        </div>
    )
}

const ItemTableHeaderWidthEdit = ({ number = 0, text1, item = undefined, text2, type = "text", onChange = undefined, editable = false, header = false, bg = false, onDelete = undefined }) => {
    return (
        <div className="flex gap-2 mb-1 items-center">
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} flex-1 text-xs p-3 ${header ? "text-white" : "text-[#333]"}  ${header && "text-center"}`}>
                {
                    type === "input" && editable ?
                        (<input type="text" value={text1} readOnly={!editable} className={`w-full focus:outline-none text-xs`} onChange={(e) => onChange('text1', e.target.value, item?.id)} />)
                        :
                        `${!header ? number + 1 : ""} ${!header ? "." : ""} ${text1}`
                }
            </div>
            <div className={`${header && "bg-[#1088e459]"} ${bg ?"bg-[rgba(70,70,70,0.1)]" : "bg-white"} w-[200px] text-xs p-3 ${header ? "text-white" : "text-[#333]"} text-center`}>
                {
                    type === "input" && editable ?
                        (
                            <select className="w-full" value={text2} onChange={(e) => onChange('text2', e.target.value, item?.id)}>
                                <option value="Y">Y</option>
                                <option value="N">N</option>
                            </select>
                        )
                        :
                        text2
                }
            </div>

            <div className="flex w-[100px] gap-3 ml-3">
                {
                    editable && type === "input" && (
                        <>

                            <button onClick={() => onDelete(item)} className="">
                                <svg width="20" height="26" viewBox="0 0 20 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M7.32432 4.2L7.81081 3.66667L8.2973 3.13333L9.7682 1.41329C9.95841 1.19086 10.2981 1.17863 10.5038 1.3868L11.369 2.26233C11.556 2.45159 11.5621 2.75415 11.3828 2.95073L5.37838 9.53333M7.32432 4.2L5.86486 2.6L2.45946 6.33333L3.91892 7.93333M7.32432 4.2L3.91892 7.93333M3.91892 7.93333L1.30736 10.7964C1.13323 10.9873 1.13323 11.2794 1.30736 11.4703L2.09005 12.3284C2.28835 12.5457 2.63057 12.5457 2.82886 12.3284L4.89189 10.0667M4.89189 10.0667L6.83784 25H16.5676L18.5135 9.53333H5.37838M4.89189 10.0667L5.37838 9.53333M15.3094 2.6H13.85M8.08389 22.3333H8.78378H9.42438V12.2H8.78378H8.08389V22.3333ZM11.1921 12.2H11.7027H12.5228V22.3333H11.7027H11.1921V12.2ZM14.012 12.2V22.3333H14.6216H15.3094V12.2H14.6216H14.012ZM10.7297 6.86667L12.1892 7.93333H14.1351L13.6486 5.8L12.1892 5.26667L10.7297 6.86667ZM15.5946 4.73333V5.8L16.0811 7.4L19 6.86667L18.027 5.26667V4.2L15.5946 4.73333Z" stroke="#464646" strokeOpacity="0.55" />
                                </svg>
                            </button>
                        </>
                    )
                }
            </div>
        </div>
    )
}
