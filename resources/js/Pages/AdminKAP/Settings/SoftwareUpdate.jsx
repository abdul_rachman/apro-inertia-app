import React, { useState } from "react";
import { ListItemSwicther } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function SoftwareUpdate(props) {
    const [active, setActive] = useState(false)
    return (
        <WrapperSetting {...props} title="Software Update" back={route('company.settings.index')} >
            <ul>
                <ListItemSwicther borderBottom={false} label="Automatic Updates" onChange={() => setActive(!active)} checked={active} />
            </ul>
        </WrapperSetting>
    );
}
