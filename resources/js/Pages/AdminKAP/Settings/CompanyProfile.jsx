import React from "react";
import { ListItemLink } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function CompanyProfile(props) {
    return (
        <WrapperSetting {...props} title="Company Profile" back={route('company.settings.index')} >
            <ul>
                <ListItemLink href={route('company.settings.company.general')} title="General Information" value="" />
                <ListItemLink href={route('company.settings.company.billing-rate')} title="Billing Rate Setting" value="" />
                <ListItemLink href={route('company.settings.company.privacy-policy')} title="Privacy Policy" value="" />
                <ListItemLink borderBottom={false} href={route('company.settings.company.term-and-conditions')} title="Term and Conditions" value="" />
            </ul>
        </WrapperSetting>
    );
}
