import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import WrapperSetting from "@/Components/WrapperSetting";
import Swal from 'sweetalert2'

const dummyData = [
    {
        id: 1,
        parent: 'Profit based entities',
        label: 'Profit before tax',
        min: 3,
        max: 10
    },
    {
        id: 2,
        parent: '',
        label: 'Revenue',
        min: 0.5,
        max: 2
    },
    {
        id: 3,
        parent: 'Asset based or investment entities',
        label: 'Total assets',
        min: 1,
        max: 2
    },
    {
        id: 4,
        parent: '',
        label: 'Net assets ',
        min: 2,
        max: 5
    },
    {
        id: 5,
        parent: 'Not-for-profit entities',
        label: 'Gross income',
        min: 0.5,
        max: 1
    },
    {
        id: 6,
        parent: '',
        label: 'Expenditure',
        min: 0.5,
        max: 1
    },
]

export default function BillingRateSettings({ settings, ...props }) {
    const [editable, setEditable] = useState(false)
    const [currentData, setCurrentData] = useState(dummyData)
    const [active, setActive] = useState(settings)

    const handleSaveEdit = () => {
        if (editable) {
            // Update Data
            let params = {
                billing_rate: active
            }
            Inertia.post(route('company.settings.company.billing-rate.update'), params);

            setEditable(false)
        } else {
            // local update
            setEditable(true)
        }
    }

    const handleChange = (key, value, id) => {
        const newArr = currentData.map(object => {
            if (object.id === id) {
                return { ...object, [key]: value };
            }
            return object;
        });
        setCurrentData(newArr)
    }

    const handleReset = () => {
        handleSaveEdit
        Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, reset it!'
        }).then((result) => {
            if (result.isConfirmed) {
                setCurrentData(settings)
            }
        })

    }

    return (
        <WrapperSetting {...props} title="Billing Rate Setting" back={route('company.settings.company')} >
            <div className="rounded-lg overflow-hidden w-full">
                <ItemTableHeader header={true} text1="POSITION" text2="JABODETABEK (Rp/Hour)" text3="OUTSIDE JABODETABEK (Rp/Hour)" />
                <ItemTableHeader text1="Junior Auditor" text2="100.000" text3="100.000" />
                <ItemTableHeader text1="Senior Auditor" text2="100.000" text3="100.000" />
                <ItemTableHeader text1="Supervisor " text2="100.000" text3="100.000" />
                <ItemTableHeader text1="Manager " text2="100.000" text3="100.000" />
                <ItemTableHeader text1="Partner " text2="100.000" text3="100.000" />
            </div>
            <div className="text-[#464646] flex gap-5 font-medium text-xs mt-6 max-w-full">
                <div className="flex-1 pl-3">
                    <p>Please select one category are meet with your criteria, and  adjust it to your minimum billing rate *). Please note that your minimum billing rate can not less than billing rate above.</p>
                </div>
                <div className="flex justify-center items-center w-[300px]">
                    <ButtonCheck active={active === "in"} onChange={ () => setActive("in")} />
                </div>
                <div className="flex justify-center items-center w-[300px]">
                    <ButtonCheck active={active === "out"} onChange={ () => setActive("out")} />
                </div>
            </div>
            <div className="mt-5 flex gap-3 text-xs">
                <button onClick={handleReset} className="bg-[#4646460c] py-2 px-4 w-[100px] rounded-md text-[#464646]">Reset</button>
                <button onClick={handleSaveEdit} className={`${editable ? "bg-[#00ff00e5]" : "bg-[#1088E4]"}  py-2 px-4 w-[100px] rounded-md text-white`}>{editable ? "Save" : "Edit"}</button>
            </div>
        </WrapperSetting>
    );
}

const ItemTableHeader = ({ text1, text2, text3, type = "text", onChange = undefined, onChange2 = undefined, editable = false, header = false, bg = false }) => {
    return (
        <div className="flex gap-2 mb-1 items-center">
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} flex-1 text-xs p-3 ${header ? "text-white" : "text-[#333]"}  ${header && "text-center"}`}>{text1}</div>
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} w-[300px] text-xs p-3 ${header ? "text-white" : "text-[#333]"} text-center`}>
                {
                    type === "input" && editable ?
                        (<input type="text" value={text2} readOnly={!editable} className={`w-full focus:outline-none text-xs`} onChange={onChange} />)
                        :
                        text2
                }
            </div>
            <div className={`${header && "bg-[#1088e459]"} ${bg ? "bg-[rgba(70,70,70,0.1)]" : "bg-white"} w-[300px] text-xs p-3 ${header ? "text-white" : "text-[#333]"} text-center`}>
                {
                    type === "input" && editable ?
                        (<input type="text" value={text3} readOnly={!editable} className={`w-full focus:outline-none text-xs`} onChange={onChange2} />)
                        :
                        text3
                }
            </div>
        </div>
    )
}

const ButtonCheck = ({ active = false, onChange= undefined }) => {
    return (
        <button onClick={onChange}>
            {
                active ? (
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        width="45"
                        height="30"
                        fill="none"
                        viewBox="0 0 45 30"
                    >
                        <path
                            fill="#0F0"
                            fillOpacity="0.85"
                            d="M0 15C0 6.716 6.716 0 15 0h15c8.284 0 15 6.716 15 15 0 8.284-6.716 15-15 15H15C6.716 30 0 23.284 0 15z"
                        ></path>
                        <g filter="url(#filter0_d_133_3207)">
                            <path
                                fill="#fff"
                                fillRule="evenodd"
                                d="M15.65 13.623a.92.92 0 000 1.301l5.854 5.854a.92.92 0 001.3 0l2.386-2.385.015-.016 7.573-7.572a.92.92 0 000-1.301l-2.385-2.385a.92.92 0 00-1.3 0l-6.939 6.938-2.818-2.818a.92.92 0 00-1.3 0l-2.386 2.384z"
                                clipRule="evenodd"
                            ></path>
                        </g>
                        <defs>
                            <filter
                                id="filter0_d_133_3207"
                                width="22.572"
                                height="19.104"
                                x="13.541"
                                y="6.85"
                                colorInterpolationFilters="sRGB"
                                filterUnits="userSpaceOnUse"
                            >
                                <feFlood floodOpacity="0" result="BackgroundImageFix"></feFlood>
                                <feColorMatrix
                                    in="SourceAlpha"
                                    result="hardAlpha"
                                    values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 127 0"
                                ></feColorMatrix>
                                <feOffset dx="0.613" dy="2.453"></feOffset>
                                <feGaussianBlur stdDeviation="1.226"></feGaussianBlur>
                                <feComposite in2="hardAlpha" operator="out"></feComposite>
                                <feColorMatrix values="0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0.16 0"></feColorMatrix>
                                <feBlend
                                    in2="BackgroundImageFix"
                                    result="effect1_dropShadow_133_3207"
                                ></feBlend>
                                <feBlend
                                    in="SourceGraphic"
                                    in2="effect1_dropShadow_133_3207"
                                    result="shape"
                                ></feBlend>
                            </filter>
                        </defs>
                    </svg>
                ) :
                    (
                        <svg
                            xmlns="http://www.w3.org/2000/svg"
                            width="45"
                            height="30"
                            fill="none"
                            viewBox="0 0 45 30"
                        >
                            <mask id="path-1-inside-1_313_1459" fill="#fff">
                                <path
                                    fillRule="evenodd"
                                    d="M14.65 13.623a.92.92 0 000 1.301l5.854 5.854a.92.92 0 001.3 0l2.386-2.385.015-.016 7.573-7.572a.92.92 0 000-1.301l-2.385-2.385a.92.92 0 00-1.3 0l-6.939 6.938-2.818-2.818a.92.92 0 00-1.3 0l-2.386 2.384z"
                                    clipRule="evenodd"
                                ></path>
                            </mask>
                            <path
                                fill="#464646"
                                fillOpacity="0.05"
                                fillRule="evenodd"
                                d="M14.65 13.623a.92.92 0 000 1.301l5.854 5.854a.92.92 0 001.3 0l2.386-2.385.015-.016 7.573-7.572a.92.92 0 000-1.301l-2.385-2.385a.92.92 0 00-1.3 0l-6.939 6.938-2.818-2.818a.92.92 0 00-1.3 0l-2.386 2.384z"
                                clipRule="evenodd"
                            ></path>
                            <path
                                fill="#464646"
                                fillOpacity="0.05"
                                d="M14.65 14.924l-.707.707.707-.707zm0-1.3l.707.707-.707-.707zm5.854 7.154l.707-.707-.707.707zm1.3 0l-.706-.707.707.707zm2.386-2.385l.707.707-.707-.707zm.015-.016l-.707-.707-.008.008-.008.01.724.69zm7.573-7.572l.707.707-.707-.707zm-3.686-3.686l-.707-.707.707.707zm-6.938 6.938l-.707.707.707.707.707-.707-.707-.707zm-2.818-2.818l.707-.707-.707.707zm-1.3 0l-.708-.707.707.707zm-1.679 2.978a.08.08 0 010 .114l-1.414-1.415a1.92 1.92 0 000 2.715l1.415-1.414zm5.854 5.854l-5.853-5.854-1.415 1.414 5.854 5.854 1.414-1.414zm-.113 0a.08.08 0 01.113 0l-1.414 1.414c.75.75 1.965.75 2.715 0l-1.414-1.414zm2.385-2.385l-2.385 2.385 1.414 1.414 2.385-2.385-1.414-1.414zm-.001.001l1.415 1.413.032-.033-1.447-1.38zm7.589-7.59l-7.573 7.573 1.415 1.414 7.572-7.572-1.414-1.414zm0 .114a.08.08 0 010-.113l1.414 1.414a1.92 1.92 0 000-2.715l-1.414 1.414zm-2.385-2.385l2.385 2.385 1.414-1.414L30.1 6.412l-1.414 1.414zm.113 0a.08.08 0 01-.113 0L30.1 6.412a1.92 1.92 0 00-2.715 0L28.8 7.826zm-6.938 6.938L28.8 7.826l-1.414-1.414-6.938 6.938 1.414 1.414zm-4.232-2.818l2.818 2.818 1.414-1.414-2.818-2.818-1.414 1.414zm.113 0a.08.08 0 01-.113 0l1.414-1.414a1.92 1.92 0 00-2.715 0l1.414 1.414zm-2.385 2.385l2.385-2.385-1.414-1.414-2.385 2.384 1.415 1.415z"
                                mask="url(#path-1-inside-1_313_1459)"
                            ></path>
                            <path
                                stroke="#464646"
                                strokeOpacity="0.05"
                                d="M.5 15C.5 6.992 6.992.5 15 .5h15C38.008.5 44.5 6.992 44.5 15S38.008 29.5 30 29.5H15C6.992 29.5.5 23.008.5 15z"
                            ></path>
                        </svg>
                    )
            }

        </button>
    )
}