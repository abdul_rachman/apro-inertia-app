import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import AdminKAPMenuSettings from "@/Components/AdminKAPMenuSettings";
import { ListRadiobox } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";
const plans = [
    {
        name: 'Arial',
    },
    {
        name: 'Inter',
    },
]

export default function FontType(props) {
    console.log('props',props)
    const [selected, setSelected] = useState(plans[1])

    const handleChange = (value) => {
        setSelected(value)
        let params = {
            font_type: value.name
        }

        Inertia.post(route('company.settings.fonts.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Font Type" titleBack="Fonts" back={route('company.settings.fonts')} >
            <ListRadiobox selected={selected} setSelected={handleChange} data={plans} />
        </WrapperSetting>
    );
}
