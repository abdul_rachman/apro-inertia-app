import React, { useState } from "react";
import { ListItem, ListItemSpace } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function Certification({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="Certification" titleBack="Legal & Regulatory" back={route('company.settings.legal')} >
           <ul>
                <ListItem title="Registration Number" value={settings.registration_number} />
                <ListItem title="Registration Date" value={settings.registration_date} />
                <ListItem title="Date of Protection Start" value={settings.date_of_protection_start} />
                <ListItemSpace />
                <ListItem title="Application Name" value={settings.app_name} />
                <ListItem title="Registered by" value={settings.registered_by} />
                <ListItem title="Domain Name" value={settings.domain_name} />
            </ul>
        </WrapperSetting>
    );
}
