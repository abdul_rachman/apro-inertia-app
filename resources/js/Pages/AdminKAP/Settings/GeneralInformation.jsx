import React from "react";
import { ListItem, ListItemSpace } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function GeneralInformation({ settings, ...props }) {
    return (
        <WrapperSetting {...props} title="General Information" titleBack="Company Profile" back={route('company.settings.company')} >
            <ul>
                <ListItem borderBottom={false} title="Client ID" value={settings.client_id} />
                <ListItemSpace />
                <ListItem title="Company Name" value={settings.company_name} />
                <ListItem title="Tax ID" value={settings.tax_id} />
                <ListItem borderBottom={false} title="Address" value={settings.address} />
                <ListItemSpace />
                <ListItem title="Practice License " value={settings.practice_license} />
                <ListItem borderBottom={false} title="Expired Date" value={settings.practice_expiry_date} />
                <ListItemSpace />
                <ListItem title="Managing Partner Name" value={settings.managing_partner_name} />
                <ListItem borderBottom={false} title="Email" value={settings.managing_partner_email} />
                <ListItemSpace />
                <ListItem title="Admin Name" value={settings.admin_name} />
                <ListItem borderBottom={false} title="Email" value={settings.admin_email} />
            </ul>
        </WrapperSetting>
    );
}
