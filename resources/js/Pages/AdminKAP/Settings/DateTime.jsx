import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListItem, ListItemSpace, ListItemSwicther } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";

export default function DateTime({ settings, ...props }) {
    const [active, setActive] = useState(settings.hour_time_24 == '1' ? true : false)
    const [active1, setActive1] = useState(settings.show_date_in_status_bar == '1' ? true : false)
    const [active2, setActive2] = useState(settings.set_automatically == '1' ? true : false)

    const handleChange = (type) => {
        let params = {}
        switch(type) {
            case 'hour_time_24' :
                params['key'] = 'hour_time_24'
                params['value'] = !active === true ? '1' : '0'
                setActive(!active)
                break
            case 'show_date_in_status_bar' :
                params['key'] = 'show_date_in_status_bar'
                params['value'] = !active1 === true ? '1' : '0'
                setActive1(!active1)
                break
            case 'set_automatically' :
                params['key'] = 'set_automatically'
                params['value'] = !active2 === true ? '1' : '0'
                setActive2(!active2)
                break
            default:
        }

        Inertia.post(route('company.settings.date.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Date Time" back={route('company.settings.index')} >
            <ul>
                <ListItemSwicther label="24-Hour Time" onChange={() => handleChange('hour_time_24')} checked={active} />
                <ListItemSwicther label="Show Date in Status Bar" onChange={() => handleChange('show_date_in_status_bar')} checked={active1} borderBottom={false} />
                <ListItemSpace />
                <ListItemSwicther label="Set Automatically" onChange={() => handleChange('set_automatically')} checked={active2} />
                <ListItem title="Time Zone" value={settings.time_zone} borderBottom={false} />
            </ul>
        </WrapperSetting>
    );
}
