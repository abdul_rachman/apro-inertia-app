import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function License({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="License" titleBack="Legal & Regulatory" back={route('company.settings.legal')} >
           <div className="bg-white p-5 rounded-md" dangerouslySetInnerHTML={{__html: settings}} />
        </WrapperSetting>
    );
}
