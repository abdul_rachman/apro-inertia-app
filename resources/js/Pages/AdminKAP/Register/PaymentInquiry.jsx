import TopBreadcrumb from "@/Components/TopBreadcrumb";
import RegisterLayout from "@/Layouts/RegisterLayout";
import { formatRupiah } from "@/utlis/common";
import { Inertia } from "@inertiajs/inertia";
import { Head } from "@inertiajs/inertia-react";

export default function PaymentInquiry({ company, transaction, token, ...props }) {

    const handleCancel = () => {
        let params = {
            transaction_code: transaction.code,
        }

        Inertia.post(route('register.package-billing.cancel'), params)
    }

    const handlePay = () => {
        if (token) {
            snap.pay(token, {
                onSuccess: function (result) {
                    // TODO snap
                    // After snap do payment()
                    // Var result from snap
                    let params = {
                        transaction_code: result.order_id,
                        midtrans_transaction_id: result.transaction_id,
                        transaction_status: result.transaction_status,
                        payment_type: result.payment_type,
                        payment_channel: result.va_numbers
                            ? result.va_numbers[0].bank
                            : (result.permata_va_number
                                ? "permata"
                                : result.bank),
                        va_number:
                            (result.va_numbers
                                ? result.va_numbers[0].va_number
                                : result.permata_va_number) || null,
                        bill_key: result.bill_key,
                        biller_code: result.biller_code,
                        payment_code: result.payment_code,
                    }
                    payment(params)
                },
                onPending: function (result) { console.log('pending'); console.log(result); },
                onError: function (result) { console.log('error'); console.log(result); },
                onClose: function () { console.log('customer closed the popup without finishing the payment'); }
            })
            console.log('token', token)

        } else {
            let params = {
                transaction_code: transaction.code,
            }
            payment(params)
        }
    }

    const payment = (params) => {
        Inertia.post(route('register.package-billing.payment'), params)
    }

    return (
        <RegisterLayout
            step={5}
            auth={props.auth}
            errors={props.errors}
            header={
                <TopBreadcrumb home="" />
            }
        >
            <Head title="Payment Inqury" />
            <div className="w-full flex justify-center items-center mb-5">
                <img src="/images/logo.png" className="w-24" />
            </div>
            <div className="flex flex-col m-auto w-[900px] bg-white p-5 border rounded-lg px-10">
                <Title text="Payment Inquiry" />
                <div className="text-xs">
                    <h2 className="font-bold mb-2">To : {company.name}</h2>
                    <p className="mb-4">{company.address}</p>
                    <p>Thank you for subscribing of APRO. Please make payment of your order below:</p>
                </div>
                <Title text="Billing Summary" />
                <div className="p-3 border rounded-xl text-xs">
                    <div className="flex justify-between px-3 mb-5">
                        <span>Order number</span>
                        <span className="text-[#787878]">{transaction.invoice_number}</span>
                    </div>
                    <OrderItem rows={["Order Description", "Unit", "Period (Month)", "Unit Price/Month", "Total Price"]} bold={true} />
                    <OrderItem rows={[transaction.package_detail.name, "1 Package", transaction.period, formatRupiah(Number(transaction.package_detail.price)), formatRupiah(Number(transaction.package_detail.price * transaction.period))]} />
                    {
                        transaction.package_add_ons?.filter(item => item.qty !== "0").map((item, index) => (
                            <OrderItem key={`item-${index}`} rows={[item.name, item.qty, transaction.period, formatRupiah(Number(item.price)), formatRupiah(Number(item.total_price))]} />
                        ))
                    }
                    <hr />
                    <OrderItemTotal rows={["Total", formatRupiah(Number(transaction.amount))]} bold={true} />
                    <OrderItemTotal rows={["VAT 11%", formatRupiah(Number(transaction.tax_amount))]} />
                    <OrderItemTotal rows={["Promo discount", formatRupiah(Number(transaction?.voucher_discount ?? 0))]} />
                    <OrderItemTotal rows={["Admin fee", formatRupiah(Number(transaction.admin_fee))]} />
                    <hr />
                    <OrderItemTotal rows={["Total Billing", formatRupiah(transaction.total_amount)]} bold={true} />
                </div>
                <div className="flex mt-7 mb-5">
                    <div className="flex justify-end items-end flex-1">
                        <button onClick={handleCancel} className="py-4 px-7 text-[#1088E4] rounded-lg font-medium">Cancel</button>
                        <button onClick={handlePay} className="py-4 px-7 disabled:bg-gray-300w bg-[#1088E4] text-white rounded-lg font-semibold">Pay</button>
                    </div>
                </div>
            </div>
        </RegisterLayout>
    );
}

const Title = ({ text }) => (
    <h1 className="text-sm font-bold my-4">{text}</h1>
)

const OrderItem = ({ rows = [], bold = false }) => {
    return (
        <div className="flex">
            {
                rows.map((item, index) => (
                    <div key={`item-order-${index}`} className={`flex-1 text-sm text-[#2A2A2A] p-3 ${index !== 0 && "text-right"} ${bold && "font-bold"}`}>
                        <span>{item}</span>
                    </div>
                ))
            }
        </div>
    )
}

const OrderItemTotal = ({ rows = [], bold = false }) => (
    <div className="flex justify-between">
        {
            rows.map((item, index) => (
                <div key={`item-order-total-${index}`} className={`flex-1 text-sm text-[#2A2A2A] p-3 ${bold && "font-bold"} ${index === rows.length - 1 && "text-right"}`}>
                    <span>{item}</span>
                </div>
            ))
        }
    </div>
)