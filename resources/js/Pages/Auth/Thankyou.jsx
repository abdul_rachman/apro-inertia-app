import { Inertia } from "@inertiajs/inertia";
import { CountdownCircleTimer } from "react-countdown-circle-timer";

export default function Thankyou() {
    return (
        <div className="flex items-center justify-center text-center flex-col">
            <div className="mb-12 mt-14">
                <h2 className="text-3xl text-[#2A2A2A] font-bold">APRO</h2>
                <p className="text-[#89A3BB] text-xs">Audit Process Online </p>
            </div>

            <div className="mb-12">
                <img src="/images/thankyou.png" alt="thank you" />
            </div>

            <div className="mb-8">
                <h3 className="text-2xl mb-5 text-[#2A2A2A]">
                    Pendaftaran Terkirim
                </h3>
                <p className="text-[#89A3BB]">
                    Permintaan pendaftaran Anda telah terkirim, selanjutnya{" "}
                    <br />
                    silakan cek link aktivasi di email Anda, terima kasih.
                </p>
            </div>

            <div className="">
                <CountdownCircleTimer
                    isPlaying
                    duration={5}
                    size={100}
                    colors={["#1088E4"]}
                    onComplete={() => Inertia.visit(route("login"))}
                >
                    {renderTime}
                </CountdownCircleTimer>
            </div>
        </div>
    );
}

const renderTime = ({ remainingTime }) => {
    return (
        <div className="timer">
            <div className="value">{remainingTime}</div>
        </div>
    );
};
