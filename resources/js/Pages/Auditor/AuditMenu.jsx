import React, { useEffect, useState } from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import Panel from "@/Components/Panel";
import ListMenu from "@/Components/ListMenu";
import axios from "axios";
import swal from "sweetalert";
import moment from "moment";
import { Inertia } from "@inertiajs/inertia";
import { Resizable } from "re-resizable";

export default function AuditMenu({ project, stages, ...props }) {

    const [selectedItem, setSelectedItem] = useState([]);
    const [selectedChild, setSelectedChild] = useState([]);

    const handleClick = (audit_stage_id) => {
        axios.get(route('auditor.audit-stages.detail', { audit_stage_id }))
            .then(function (response) {
                setSelectedItem(response.data.data)
            })
            .catch(function () {
                swal("Maaf terjadi kesalan system kami", {
                    buttons: false,
                    timer: 1000,
                });
            });
    }

    return (
        <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
            <Head title="Existing Project" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="flex-1">
                        <Panel>
                            <Panel.Header>
                                <Panel.Back href={route("auditor.projects.existing")} />
                                <Panel.Title
                                    title={`${project.workspace_type} - ${project.workspace}`}
                                    subtitle={`${moment(project.start_date).format('LL')} - ${moment(project.end_date).format('LL')}`}
                                />
                                <Panel.Search placeholder="Search..." />
                            </Panel.Header>
                            <Panel.Content>
                                <div className="w-full flex overflow-hidden">
                                    <Resizable
                                        className="flex items-start justify-center border-r-8 border-[#b5b5c318] bg-white h-[calc(100vh-137px)]"
                                        defaultSize={{
                                            width: '50%',
                                        }}
                                        maxWidth="100%"
                                        minWidth="1"
                                        enable={{ top:false, right:true, bottom:false, left:false, topRight:false, bottomRight:false, bottomLeft:false, topLeft:false }}
                                    >
                                        <div className="flex-1 bg-white py-6">
                                            {stages.map((item, index) => (
                                                <ListMenu
                                                    key={index}
                                                    active={selectedItem?.id === item.id}
                                                    onClick={() => {
                                                        handleClick(item.id)
                                                    }}
                                                >
                                                    <ListMenu.Group>
                                                        <ListMenu.Title
                                                            text={item.code}
                                                        />
                                                        <ListMenu.Title
                                                            text={item.name}
                                                        />
                                                    </ListMenu.Group>
                                                    <ListMenu.Group
                                                        center={true}
                                                        right={true}
                                                    >
                                                        <ListMenu.Badge
                                                            count={0}
                                                        />
                                                        <ListMenu.Icon />
                                                    </ListMenu.Group>
                                                </ListMenu>
                                            ))}
                                        </div>
                                    </Resizable>
                                    <div className="flex items-start justify-center bg-white h-[calc(100vh-137px)] overflow-y-auto" style={{ width: '100%', minWidth: '1px' }}>
                                        <div className="flex-1 py-6 bg-white flex flex-col justify-start items-start">
                                            {selectedItem?.length > 0 ? (
                                                selectedItem?.map((item, index) => (
                                                    <ListMenu
                                                        key={index}
                                                        onClick={() => {
                                                            if (item.route_name !== null) {
                                                                Inertia.get(route(item.route_name, { project_id: project.id }))

                                                            } else {
                                                                if (selectedChild?.id === item.id) {
                                                                    setSelectedChild([])

                                                                } else if (item.child) {
                                                                    setSelectedChild(item)
                                                                }
                                                            }

                                                        }
                                                        }
                                                    >
                                                        <ListMenu.Group>
                                                            <ListMenu.Title
                                                                text={item.code}
                                                            />
                                                            <ListMenu.Title
                                                                text={item.name}
                                                            />
                                                        </ListMenu.Group>
                                                        <ListMenu.Group
                                                            center={true}
                                                            right={true}
                                                        >
                                                            <ListMenu.Badge
                                                                count={item.progres}
                                                            />
                                                            {item.route_name === null && (
                                                                <ListMenu.Icon />
                                                            )}
                                                        </ListMenu.Group>
                                                    </ListMenu>
                                                ))
                                            ) : (
                                                <div className="flex flex-col justify-center items-center w-full">
                                                    <div className=" py-14">
                                                        <img src="/images/image_hello_project.png" />
                                                        <div className="mt-14 text-center">
                                                            <h2 className="text-[#2A2A2A] text-2xl font-bold">
                                                                Pilih Menu
                                                            </h2>
                                                            <p className="text-[#89A3BB] mt-4">
                                                                Anda perlu memilih menu
                                                                yang ada
                                                                <br />
                                                                untuk melanjukan proses
                                                                auditmu
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                            )}
                                        </div>
                                        {
                                            selectedChild?.child?.length > 0 && (
                                                <div className="flex-1 py-6 bg-white flex flex-col ">
                                                    {
                                                        selectedChild?.child?.map((item, index) => (
                                                            <ListMenu
                                                                key={index}
                                                                onClick={() =>
                                                                    console.log("testing")
                                                                }
                                                            >
                                                                <ListMenu.Group>
                                                                    <ListMenu.Title
                                                                        text={item.code}
                                                                    />
                                                                    <ListMenu.Title
                                                                        text={item.name}
                                                                    />
                                                                </ListMenu.Group>
                                                            </ListMenu>
                                                        ))
                                                    }
                                                </div>
                                            )
                                        }
                                    </div>
                                </div>
                            </Panel.Content>
                            <Panel.Breadcrumb>
                                <Panel.BreadcrumbItem
                                    text="Existing Project"
                                    href={route("auditor.projects.existing")}
                                />
                                <Panel.BreadcrumbItem
                                    href={route("auditor.projects.existing")}
                                    text="Biffco Enterprises Ltd."
                                />
                                <Panel.BreadcrumbItem
                                    text="AUD - Biffco Enterprises Ltd."
                                    current={selectedItem?.child?.length > 0 ? false : true}
                                />
                                {
                                    selectedItem?.child?.length > 0 && (
                                        <Panel.BreadcrumbItem
                                            text={selectedItem?.name}
                                            current={selectedChild?.child?.length > 0 ? false : true}
                                        />
                                    )
                                }
                                {
                                    selectedChild?.child?.length > 0 && (
                                        <Panel.BreadcrumbItem
                                            text={selectedChild?.name}
                                            current={true}
                                        />
                                    )
                                }
                            </Panel.Breadcrumb>
                        </Panel>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
