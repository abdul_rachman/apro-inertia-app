import EmptyState from "@/Components/EmptyState";
import Card from "@/Components/homepage/Card";
import ClientStatus from "@/Components/homepage/ClientStatus";
import ClientTalk from "@/Components/homepage/ClientTalk";
import Insight from "@/Components/homepage/Insight";
import TitleWithDropdown from "@/Components/homepage/TitleWIthDropdown";
import TopBreadcrumb from "@/Components/TopBreadcrumb";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import {
    Bar,
    BarChart,
    CartesianGrid,
    Cell,
    Legend,
    Pie,
    PieChart,
    ResponsiveContainer,
    Sector,
    Tooltip,
    XAxis,
    YAxis,
} from "recharts";

const data = [
    {
        name: "The Walt Disney Disney Disney",
        percen: 80,
        pv: 2400,
        amt: 2400,
    },
    {
        name: "McDonald's",
        percen: 96,
        pv: 1398,
        amt: 2210,
    },
    {
        name: "Apple",
        percen: 77,
        pv: 9800,
        amt: 2290,
    },
    {
        name: "Louis Vuitton",
        percen: 84,
        pv: 3908,
        amt: 2000,
    },
    {
        name: "eBay",
        percen: 49,
        pv: 4800,
        amt: 2181,
    },
    {
        name: "Facebook",
        percen: 62,
        pv: 3800,
        amt: 2500,
    },
];

const data1 = [
    { name: "Complete", value: 80 },
    { name: "On Progress", value: 4 },
    { name: "Pending", value: 16 },
];
const COLORS = ["#00CB75", "#FFA513", "#FF007A"];

export default function Dashboard(props) {
    const CustomLabel = (props) => {
        const { x, y, width, height, value } = props;
        const fireOffset = value.toString().length < 5;
        const offset = fireOffset ? -40 : 5;
        return (
            <text
                x={x + width - offset}
                y={y + height - 5}
                className="font-montserrat"
                dy={-4}
                fill="#464646"
                textAnchor="middle"
            >
                {value}%
            </text>
        );
    };

    const RADIAN = Math.PI / 180;
    const renderCustomizedLabel = ({
        cx,
        cy,
        midAngle,
        innerRadius,
        outerRadius,
        percent,
        index,
    }) => {
        const radius = innerRadius + (outerRadius - innerRadius) * 0.1;
        const x = cx + radius * Math.cos(-midAngle * RADIAN);
        const y = cy + radius * Math.sin(-midAngle * RADIAN);

        return (
            <text
                x={x}
                y={y}
                fill="white"
                textAnchor={x > cx ? "start" : "end"}
                dominantBaseline="central"
            >
                {`${(percent * 100).toFixed(0)}%`}
            </text>
        );
    };

    return (
        <Authenticated
            auth={props.auth}
            errors={props.errors}
            header={<TopBreadcrumb />}
        >
            <Head title="Dashboard" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="flex-1">
                        <div className="w-full border border-[#E6E6E8] rounded-2xl px-3 py-9 bg-white mb-5 mt-5 shadow-md">
                            <TitleWithDropdown
                                title="Project Status"
                                label="All"
                            />
                            <div className="w-full h-[450px]">
                                <ResponsiveContainer>
                                    <BarChart
                                        layout="vertical"
                                        margin={{
                                            top: 5,
                                            right: 30,
                                            left: 20,
                                            bottom: 5,
                                        }}
                                        width={150}
                                        height={40}
                                        data={data}
                                    >
                                        <XAxis
                                            type="number"
                                            tick={false}
                                            axisLine={false}
                                            padding={{ right: 127 }}
                                        />
                                        <YAxis
                                            type="category"
                                            dataKey="name"
                                            width={200}
                                        />
                                        <CartesianGrid strokeDasharray="3 3" />
                                        <defs>
                                            <linearGradient
                                                id="colorUv"
                                                x1="1"
                                                y1="0.5"
                                                x2="0"
                                                y2="0.5"
                                            >
                                                <stop
                                                    offset="0%"
                                                    stop-color="#ffffb5"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#f6d860"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#ffffff"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#ed894b"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#d53a42"
                                                />
                                                <stop
                                                    offset="0%"
                                                    stop-color="#3fc4fd"
                                                />
                                                <stop
                                                    offset="100%"
                                                    stop-color="#3f8dfd"
                                                />
                                            </linearGradient>
                                        </defs>
                                        <Bar
                                            dataKey="percen"
                                            barSize={25}
                                            fill={`url(#colorUv)`}
                                            for
                                            label={<CustomLabel />}
                                            radius={[0, 30, 30, 0]}
                                        />
                                    </BarChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                    </div>

                    <div className="w-96">
                        <div className="w-full border border-[#E6E6E8] rounded-2xl px-2 py-9 bg-white mb-5 mt-5 shadow-md">
                            <TitleWithDropdown
                                label="This Month"
                                title="Total Achivement"
                            />
                            <div className="w-full h-[340px]">
                                <ResponsiveContainer>
                                    <PieChart
                                        width={800}
                                        height={400}
                                    // onMouseEnter={this.onPieEnter}
                                    >
                                        <Pie
                                            data={data1}
                                            cx={180}
                                            cy={120}
                                            innerRadius={60}
                                            labelLine={false}
                                            label={renderCustomizedLabel}
                                            outerRadius={120}
                                            activeShape={renderActiveShape}
                                            fill="#8884d8"
                                            dataKey="value"
                                            activeIndex={0}
                                        >
                                            {data.map((entry, index) => (
                                                <Cell
                                                    key={`cell-${index}`}
                                                    fill={
                                                        COLORS[
                                                        index %
                                                        COLORS.length
                                                        ]
                                                    }
                                                    label={true}
                                                />
                                            ))}
                                        </Pie>
                                        <Legend />
                                    </PieChart>
                                </ResponsiveContainer>
                            </div>
                        </div>
                    </div>
                </div>

                <div className="w-full border border-[#E6E6E8] rounded-2xl px-8 py-9 bg-white mb-5 mt-5 shadow-md">
                    <div className="mb-9 flex items-center justify-between ">
                        <h2 className="text-lg font-semibold">
                            Team Duscussion
                        </h2>
                        <div className="flex">
                            <div className="flex gap-2 items-center">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="18"
                                    height="18"
                                    fill="none"
                                    viewBox="0 0 18 18"
                                >
                                    <path
                                        fill="#2A2A2A"
                                        d="M16.275 14.175L13.95 11.85a7.01 7.01 0 01-2.1 2.1l2.325 2.325c.3.3.75.3 1.05 0l1.05-1.05c.3-.3.3-.75 0-1.05z"
                                    ></path>
                                    <path
                                        fill="#5E6278"
                                        d="M8.25 15C4.5 15 1.5 12 1.5 8.25s3-6.75 6.75-6.75S15 4.5 15 8.25 12 15 8.25 15zm0-12A5.218 5.218 0 003 8.25a5.218 5.218 0 005.25 5.25 5.218 5.218 0 005.25-5.25A5.218 5.218 0 008.25 3z"
                                        opacity="0.3"
                                    ></path>
                                </svg>
                                <input className="border-none" type="text" placeholder="Search" />
                            </div>
                            <div className="flex justify-between items-center p-3 rounded-lg border border-[#E2E2E4]">
                                <span className="mr-2 text-xs text-[#464646]">
                                    New Message
                                </span>
                            </div>
                        </div>
                    </div>
                    <div className="">
                        <div className="flex justify-between border-b">
                            <div className="flex gap-4 items-center py-5">
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="10"
                                    height="6"
                                    fill="none"
                                    viewBox="0 0 10 6"
                                >
                                    <path
                                        fill="#323232"
                                        d="M.605 5.445a.82.82 0 010-1.176L4.303.677a1 1 0 011.394 0l3.698 3.592a.82.82 0 01-1.143 1.176l-2.904-2.82a.5.5 0 00-.696 0l-2.904 2.82a.82.82 0 01-1.143 0z"
                                    ></path>
                                </svg>
                                <h2 className="text-sm font-medium text-[#464646]">Unread Message</h2>
                            </div>
                            <div className="flex gap-4 items-center py-5">
                                <p className="text-xs text-[#464646] font-medium">1 - 100 Message</p>
                                <svg
                                    xmlns="http://www.w3.org/2000/svg"
                                    width="24"
                                    height="24"
                                    fill="none"
                                    viewBox="0 0 24 24"
                                >
                                    <path
                                        fill="#5E6278"
                                        d="M20 12a2 2 0 11-4 0 2 2 0 014 0zm-6 0a2 2 0 11-4 0 2 2 0 014 0zm-6 0a2 2 0 11-4 0 2 2 0 014 0z"
                                        opacity="0.8"
                                    ></path>
                                </svg>
                            </div>
                        </div>
                        <div className="">
                            <ItemMessage
                                image={"/images/dummy_user_profile.png"}
                                name="Bessie Cooper"
                                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut lab ..."
                                time="9:32 PM"
                            />
                            <ItemMessage
                                image={"/images/dummy_user_profile.png"}
                                name="Bessie Cooper"
                                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut lab ..."
                                time="9:32 PM"
                            />
                            <ItemMessage
                                image={"/images/dummy_user_profile.png"}
                                name="Bessie Cooper"
                                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut lab ..."
                                time="9:32 PM"
                            />
                            <ItemMessage
                                image={"/images/dummy_user_profile.png"}
                                name="Bessie Cooper"
                                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut lab ..."
                                time="9:32 PM"
                            />
                        </div>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}

const ItemMessage = ({ image, name, description, time }) => {
    return (
        <div className="flex items-center justify-between gap-5 py-5 border-b text-xs text-[#464646]">
            <div className="flex-1 flex items-center gap-5">
                <svg
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    fill="none"
                    viewBox="0 0 16 16"
                >
                    <rect
                        width="15"
                        height="15"
                        x="0.5"
                        y="0.5"
                        fill="#F9F9FB"
                        stroke="#E6E6E8"
                        rx="3.5"
                    ></rect>
                </svg>
                <img src={image} className="rounded-full w-[24px] h-[24px]" />
                <strong className="font-semibold">{name}</strong>
                <p className="font-medium">{description}</p>
            </div>
            <div className="font-medium">
                <p>{time}</p>
            </div>
        </div>
    )
}


const renderActiveShape = (props) => {
    const RADIAN = Math.PI / 180;
    const { cx, cy, midAngle, innerRadius, outerRadius, startAngle, endAngle, fill, payload, percent, value } = props;
    const sin = Math.sin(-RADIAN * midAngle);
    const cos = Math.cos(-RADIAN * midAngle);
    const sx = cx + (outerRadius + 10) * cos;
    const sy = cy + (outerRadius + 10) * sin;
    const mx = cx + (outerRadius + 30) * cos;
    const my = cy + (outerRadius + 30) * sin;
    const ex = mx + (cos >= 0 ? 1 : -1) * 22;
    const ey = my;
    const textAnchor = cos >= 0 ? 'start' : 'end';

    return (
        <g>
            <text x={cx} y={cy} fontSize={35} fontWeight={700} dy={12} textAnchor="middle" fill={"#9FB3C5"}>
                3/10
            </text>
            <defs>
                <linearGradient id="colorUv1" x1="0" y1="0" x2="0" y2="1">
                    <stop offset="5%" stopColor="#0AF08F" stopOpacity={1} />
                    <stop offset="95%" stopColor="#02A962" stopOpacity={1} />
                </linearGradient>
            </defs>
            <Sector
                cx={cx}
                cy={cy}
                innerRadius={innerRadius}
                outerRadius={outerRadius}
                startAngle={startAngle}
                endAngle={endAngle}
                fill="url(#colorUv1)"
                fillOpacity={1}
            />
        </g>
    );
};
