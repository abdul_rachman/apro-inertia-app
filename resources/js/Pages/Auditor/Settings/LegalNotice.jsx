import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function LegalNotice({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="Legal Notice" titleBack="Legal & Regulatory" back={route('auditor.settings.legal')} >
           <div>Legal Notice</div>
        </WrapperSetting>
    );
}
