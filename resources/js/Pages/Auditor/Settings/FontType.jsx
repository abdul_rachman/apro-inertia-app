import React, { useState } from "react";
import { Inertia } from "@inertiajs/inertia";
import { ListRadiobox } from "@/Components/settings";
import WrapperSetting from "@/Components/WrapperSetting";
const plans = [
    {
        name: 'Arial',
    },
    {
        name: 'Inter',
    },
]

export default function FontType(props) {
    const [selected, setSelected] = useState(plans[1])

    const handleChange = (value) => {
        setSelected(value)
        let params = {
            font_type: value.name
        }

        Inertia.post(route('auditor.settings.fonts.update'), params);
    }

    return (
        <WrapperSetting {...props} title="Font Type" titleBack="Fonts" back={route('auditor.settings.fonts')} >
            <ListRadiobox selected={selected} setSelected={handleChange} data={plans} />
        </WrapperSetting>
    );
}
