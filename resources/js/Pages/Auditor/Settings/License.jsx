import React, { useState } from "react";
import WrapperSetting from "@/Components/WrapperSetting";

export default function License({ settings, ...props}) {
    return (
        <WrapperSetting {...props} title="Legal Notice" titleBack="Legal & Regulatory" back={route('auditor.settings.legal')} >
           <div>License</div>
        </WrapperSetting>
    );
}
