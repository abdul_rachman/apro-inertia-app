import React, { useState } from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head, Link, useForm } from "@inertiajs/inertia-react";
import Panel from "@/Components/Panel";
import Modal from "@/Components/clients/Modal";
import TabModal from "@/Components/clients/TabModal";
import InputSelect from "@/Components/InputSelect";
import { useEffect } from "react/cjs/react.production.min";
import InputForm from "@/Components/clients/InputForm";
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import moment from "moment";

export default function Project({ companyClients, ...props }) {

    const options = [
        {
            label: "Select Company",
            value: ""
        },
        ...companyClients.map(item => (
            {
                label: item.name,
                value: item.id,
            }
        ))
    ]

    const [isOpen, setIsOpen] = useState(false);
    const [startDate, setStartDate] = useState(new Date());
    const [endDate, setEndDate] = useState(new Date());
    const [selectedClient, setSelectedClient] = useState(options[0]);

    const { data, setData, post, progress, reset, errors } = useForm({
        company_client_id: "",
        workspace_type: "AUD",
        workspace: "",
        start_date: "",
        end_date: "",
    })

    function closeModal() {
        setIsOpen(false);
    }

    function handleSubmit() {
        data["start_date"] = moment(startDate).format('YYYY-MM-DD');
        data["end_date"] = moment(endDate).format('YYYY-MM-DD');
        data["company_client_id"] = selectedClient.value
        closeModal()
        post(route('auditor.projects.store'));
        
    }

    const handleNewProject = () => {
        setIsOpen(true);
    }

    return (
        <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
            <Head title="Projects" />

            <div className="w-full">
                <div className="flex space-x-8">
                    <div className="flex-1">
                        <Panel>
                            <Panel.Header>
                                <Panel.Title
                                    title="Project"
                                />
                            </Panel.Header>
                            <Panel.Content>
                                <div className="w-96 self-center text-center">
                                    <h3 className="text-lg font-semibold mt-8 mb-5 text-[#2A2A2A]">
                                        Welcome to APRO
                                    </h3>
                                    <p className="text-[#89A3BB] text-sm">
                                        Please select to create a new project or
                                        continue an existing project.
                                    </p>
                                </div>
                                <div className="flex w-full">
                                    <div className="w-full max-w-[750px]">
                                        <img
                                            src="/images/image_project.png"
                                            className="w-full max-w-[750px]"
                                        />
                                    </div>
                                    <div className="flex-1 mt-28">
                                        <button type="button" onClick={handleNewProject} className="rounded-lg text-left block w-full max-w-[256px] border border-[#E6E6E8] p-6">
                                            <div className="flex">
                                                <div className="flex items-center ">
                                                    <img src="/images/icon_dashboard.png" />
                                                </div>
                                                <strong className="text-[#464646] block ml-4">
                                                    New Project
                                                </strong>
                                            </div>
                                            <div className="ml-8 mt-3">
                                                <p className="text-[#9FB3C5] text-xs">
                                                    You can start creating a new
                                                    project by clicking me.
                                                </p>
                                            </div>
                                        </button>

                                        <Link href={route('auditor.projects.existing')} className="rounded-lg inline-block w-full max-w-[256px] border border-[#E6E6E8] p-6 mt-6">
                                            <div className="flex">
                                                <div className="flex items-center ">
                                                    <img src="/images/icon_dashboard.png" />
                                                </div>
                                                <strong className="text-[#464646] block ml-4">
                                                    Existing Project
                                                </strong>
                                            </div>
                                            <div className="ml-8 mt-3">
                                                <p className="text-[#9FB3C5] text-xs">
                                                    Hey would you continue with
                                                    your last project before.
                                                </p>
                                            </div>
                                        </Link>
                                    </div>
                                </div>
                            </Panel.Content>
                        </Panel>
                    </div>
                </div>
            </div>

            <Modal open={isOpen} onClose={closeModal}>
                <div className="">
                    <div className="flex justify-center">
                        <h2 className="text-lg font-semibold text-center">Company Information</h2>
                    </div>
                    <div className="mt-5 flex flex-col gap-5">
                        <InputSelect options={options} value={selectedClient} onChange={(item) => setSelectedClient(item)} />

                        <InputForm error={errors?.workspace} value={data?.workspace} onChange={(e) => setData('workspace', e.target.value)} type="text" placeholder="Workspace" />

                        <div className="flex gap-3">
                            <DatePicker className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5" selected={startDate} onChange={(date) => setStartDate(date)} />
                            <DatePicker className="w-full border border-[#E6E6E8] rounded-lg text-[#464646] px-4 py-5" selected={endDate} onChange={(date) => setEndDate(date)} />
                        </div>
                    </div>
                    <div className="mt-10 space-x-4 flex justify-end">
                        <button
                            type="button"
                            onClick={closeModal}
                            className="font-semibold text-sm p-5 px-8 text-[#1088E4] rounded-lg"
                        >
                            Batal
                        </button>
                        <button
                            type="button"
                            onClick={() => {
                                handleSubmit()
                            }}
                            className="font-semibold text-sm p-5 px-8 text-white bg-[#1088E4] rounded-lg"
                        >
                            Tambahkan
                        </button>
                    </div>
                </div>
            </Modal>
        </Authenticated>
    );
}
