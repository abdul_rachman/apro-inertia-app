export default function Title({title, description, className = '', lists = []}) {
  return (
    <div className={`mb-5 ${className}`}>
      {title ? <h2 className="text-[#2A2A2A]">{title}</h2> : null}
      {description ? <p className="text-sm text-[#2a2a2acc]">{description}</p> : null}
      {lists.length > 0 ? (
        <ul className="text-sm text-[#2a2a2acc] list-disc pl-5">
          {lists.map(list => (
            <li>{list}</li>
          ))}
        </ul>
      ) : null}
    </div>
  )
}
