import Accordion from '../../../A1/Components/Accordion'
import Question from '../../../A1/Components/Question'
import RadioButtonInline from '../../../A1/Components/RadioButtonInline'
import Grouping from '../../components/Grouping'

import SelectInline from '../../components/SelectInline'
import TextArea from '../../components/TextArea'
import TextInputInline from '../../components/TextInputInline'
import Title from '../../components/Title'

export default function Calculation() {
  return (
    <Accordion number={'II.'} title="CALCULATION">
      <Question number="1" title="Overall Materiality Computation (Planning Stage)">
        <Title description="A mount of misstatements that could influence the decision of Financial Statements users." />
        <Grouping>
          <SelectInline
            description="Materiality Rate"
            options={[{label: 'Profit and revenue based entities', value: ''}]}
          />
          <TextInputInline description="Overall Materiality" />
          <TextInputInline description="The overall materiality to be applied (Planning Stage)" />
        </Grouping>
        <TextArea placeholder="Rationale for the determination of Materiality" />
        <Title className="mt-12" title={'Final Materiality'} />
        <Grouping>
          <TextInputInline description="Final materiality calculation (Ref. E.2)" />
          <TextInputInline description="The overall materiality to be applied (Final Stage) - (Ref. E.9)" />
        </Grouping>
        <RadioButtonInline
          description="We detect that the overall materiality at planning stage is less than overall materiality at final stage, do you wan to change the overall materiality?"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <TextArea placeholder="Rationale" />
      </Question>
      <Question number="2" title="Performance Materiality Computation  (Planning Stage)">
        <Title description="Safety buffer for unidentified misstatements" />
        <Grouping>
          <SelectInline description="Performance Materiality Rate From Overall Materiality Amount" options={[]} />
          <TextInputInline description="Performance Materiality" />
        </Grouping>
        <TextArea placeholder="Rationale" />
      </Question>
      <Question number="3" title="Clearly Trivial Computation  (Planning Stage)">
        <Title description="Safety buffer for unidentified misstatements" />
        <Grouping>
          <SelectInline description="Clearly Trivial Rate  From Overall Materiality Amount" options={[]} />
          <TextInputInline description="Performance Materiality" />
        </Grouping>
        <TextArea placeholder="Rationale for the determination of Clearly Trivia" />
      </Question>
    </Accordion>
  )
}
