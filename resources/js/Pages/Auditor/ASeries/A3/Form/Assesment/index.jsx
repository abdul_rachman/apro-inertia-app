import Accordion from '../../../A1/Components/Accordion'
import Grouping from '../../components/Grouping'
import Question from '../../components/Question'
import SelectInline from '../../components/SelectInline'
import TextArea from '../../components/TextArea'
import TextInputInline from '../../components/TextInputInline'
import Title from '../../components/Title'

export default function Assesment() {
  return (
    <Accordion number={'I.'} title="ASSESMENT">
      <Question title="Factor to be considered">
        <Title
          description="To evaluate whether misstatements in qualitative disclosures might be material, the auditor can take into account several pertinent factors, including"
          lists={[
            'Nature of Transactions During the Period',
            'Numerous Small Misstatements',
            'Circumstances of the Entity',
            'The Applicable Financial Reporting Framework',
            'Nature of the Entity.',
          ]}
        />
        <Grouping>
          <SelectInline
            description="Nature of client entity"
            options={[{label: 'Profit and revenue based entities', value: ''}]}
          />
          <SelectInline description="Benchmarks" options={[{label: 'Profit before tax', value: ''}]} />
          <TextInputInline description="Balance" />
        </Grouping>
        <TextArea placeholder="Comment" />
      </Question>
    </Accordion>
  )
}
