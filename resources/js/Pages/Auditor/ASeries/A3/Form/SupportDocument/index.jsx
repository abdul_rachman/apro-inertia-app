import Accordion from '../../../A1/Components/Accordion'
import RadioButtonInline from '../../../A1/Components/RadioButtonInline'
import Question from '../../components/Question'
import Title from '../../components/Title'

export default function SupportDocument() {
  return (
    <Accordion title="SUPPORTING DOCUMENTS">
      <Question>
        <RadioButtonInline
          description="Do you want to attach documents?"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <Title description="What type of documents?" />
        <RadioButtonInline
          description="Audit working paper"
          options={[
            {label: '', value: 'Yes'},
          ]}
        />
        <RadioButtonInline
          description="Supporting document of audit working paper"
          options={[
            {label: '', value: 'Yes'},
          ]}
        />
      </Question>
    </Accordion>
  )
}
