import React from 'react'
import Accordion from '@/Components/Accordion'

export default function EngagementTeam() {
  return (
    <Accordion
      buttonContent={
        <div className="flex items-center justify-between px-3 py-8 border-y uppercase text-[#464646] font-semibold">
          <p>I. ENGAGEMENT TEAM</p>
        </div>
      }
    >
      <p>Test</p>
    </Accordion>
  )
}
