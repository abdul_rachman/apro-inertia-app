import React from 'react'
import Accordion from '@/Components/Accordion'
import RadioInput from '@/Components/RadioInput'

export default function BackgroundInformationProspectiveClient() {
  return (
    <Accordion
      buttonContent={
        <div className=" justify-between px-3 py-8 border-y uppercase text-[#464646] font-semibold">
          <p>I. Background Information of Prospective Client</p>
        </div>
      }
    >
      <div className="p-8 max-w-[720px]">
        <p className=" text-[#2A2A2A] font-medium mb-8">
          <span className="inline-block mr-4">I.A</span>
          General Information
        </p>
        <div className="px-4 overflow-auto">
          <table className="w-full">
            <tbody>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">1.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="clientName" className="opacity-80">
                    Nama Client
                    <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="clientName"
                    id="clientName"
                    placeholder="Client Name"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">2.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="nib" className="opacity-80">
                    Business Identification Number (NIB)
                    <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="nib"
                    id="nib"
                    placeholder="Your NIB number"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">3.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="npwp" className="opacity-80">
                    Tax ID (NPWP)
                    <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="npwp"
                    id="npwp"
                    placeholder="Your NPWP number"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">4.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="type" className="opacity-80">
                    Type of Business Entity
                    <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="type"
                    id="type"
                    placeholder="Select your business"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select your business
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">5.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label className="opacity-80">
                    Client Address
                    <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80"></td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-4">
                  <label className="opacity-80">Details Address</label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80"></td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="country" className="opacity-80">
                    Country
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="country"
                    id="country"
                    placeholder="Select Country"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Country
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="province" className="opacity-80">
                    Province
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="province"
                    id="province"
                    placeholder="Select Province"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Province
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="regency" className="opacity-80">
                    Regency
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="regency"
                    id="regency"
                    placeholder="Select Regency"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Regency
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="regency" className="opacity-80">
                    Regency
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="regency"
                    id="regency"
                    placeholder="Select Regency"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Regency
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="districts" className="opacity-80">
                    Districts
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="districts"
                    id="districts"
                    placeholder="Select Districts"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Districts
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="address" className="opacity-80">
                    Street / Building Name
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <textarea
                    name="address"
                    id="address"
                    placeholder="Your Street Location"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="postalCode" className="opacity-80">
                    Post Code
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="postalCode"
                    id="postalCode"
                    placeholder="Your Post Code"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="telephone" className="opacity-80">
                    Telephone
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="telephone"
                    id="telephone"
                    placeholder="Your Phone Number"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="facsimile" className="opacity-80">
                    Facsimile
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="facsimile"
                    id="facsimile"
                    placeholder="Your Facsimile"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] px-2 py-2"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-8">
                  <label htmlFor="email" className="opacity-80">
                    Email Address
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    name="email"
                    id="email"
                    type="email"
                    placeholder="Your Email Address"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">6.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="parentOrSubs" className="opacity-80">
                    Is the Client a Parent or Subsidiary Company? <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="parentOrSubs"
                    id="parentOrSubs"
                    placeholder="Select Answer"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Answer
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">7.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="publicEntity" className="opacity-80">
                    Is the Client a Public Entity <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">8.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="reportingFramework" className="opacity-80">
                    Financial Reporting Framework <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="reportingFramework"
                    id="reportingFramework"
                    placeholder="Select Answer"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Answer
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">9.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="financialStatements" className="opacity-80">
                    Period of Financial Statements <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="financialStatements"
                    id="financialStatements"
                    placeholder="Select Answer"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Answer
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">10.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="industrySector" className="opacity-80">
                    Industry Sector (please tick one box only) <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="industrySector"
                    id="industrySector"
                    placeholder="Select Answer"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Answer
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">11.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="endFiscal" className="opacity-80">
                    End of Fiscal Year <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="endFiscal"
                    id="endFiscal"
                    placeholder="Select Date"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Date
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">12.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="reportingCurrency" className="opacity-80">
                    Reporting Currency <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="reportingCurrency"
                    id="reportingCurrency"
                    placeholder="Select Currency"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Currency
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">13.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="invesmentStatus" className="opacity-80">
                    Investment Status <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="invesmentStatus"
                    id="invesmentStatus"
                    placeholder="Select Investment Status"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Investment Status
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">14.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="ownershipStatus" className="opacity-80">
                    Ownership Status <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="ownershipStatus"
                    id="ownershipStatus"
                    placeholder="Select Ownership"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Ownership
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">15.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="" className="opacity-80">
                    Names of Shareholders and Share Ownership <span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <button type="button" className="text-[#1088E4] text-sm font-medium">
                    Show Data
                  </button>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">16.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="parentCompany" className="opacity-80">
                    Parent Company Identity<span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="parentCompany"
                    id="parentCompany"
                    placeholder="Your Parent Company Identity"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">17.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="pic" className="opacity-80">
                    Person in Charge (PIC) Name
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="pic"
                    id="pic"
                    placeholder="Your PIC"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="picNoTelp" className="opacity-80">
                    No. Telp/Mobile
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="relative">
                    <p className="absolute top-1/2 left-4 -translate-y-1/2">+62</p>
                    <input
                      type="text"
                      name="picNoTelp"
                      id="picNoTelp"
                      placeholder="123-456-789"
                      className="flex-1 py-2 px-4 pl-14 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">17.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="picEmail" className="opacity-80">
                    Email
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="email"
                    name="picEmail"
                    id="picEmail"
                    placeholder="Your PIC Email"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">18.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="" className="opacity-80">
                    Name of the Board of Directors
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <button type="button" className="text-[#1088E4] text-sm font-medium">
                    Show Data
                  </button>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">19.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="" className="opacity-80">
                    Name of the Board of Commissioner
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <button type="button" className="text-[#1088E4] text-sm font-medium">
                    Show Data
                  </button>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">20.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="supervisoryAgencyName" className="opacity-80">
                    Supervisory Agency Name
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <select
                    name="supervisoryAgencyName"
                    id="supervisoryAgencyName"
                    placeholder="Select Option"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  >
                    <option value="" disabled selected>
                      Select Option
                    </option>
                  </select>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">21.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="supervisoryAgencyName" className="opacity-80">
                    Does the Client Use a Financial Agent/Banker?
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="bankerName" className="opacity-80">
                    Name
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="bankerName"
                    id="bankerName"
                    placeholder="Your Banker Name"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="bankerPhoneNumber" className="opacity-80">
                    No. Telp/Mobile
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="relative">
                    <p className="absolute top-1/2 left-4 -translate-y-1/2">+62</p>
                    <input
                      type="text"
                      name="bankerPhoneNumber"
                      id="bankerPhoneNumber"
                      placeholder="123-456-789"
                      className="flex-1 py-2 px-4 pl-14 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="bankerEmail" className="opacity-80">
                    Email
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="email"
                    name="bankerEmail"
                    id="bankerEmail"
                    placeholder="Your Banker Email"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">22.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="lawyerName" className="opacity-80">
                    Does the Client Have a Lawyer?
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="lawyerName" className="opacity-80">
                    Name
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="lawyerName"
                    id="lawyerName"
                    placeholder="Your Lawyer Name"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="lawyerPhoneNumber" className="opacity-80">
                    No. Telp/Mobile
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="relative">
                    <p className="absolute top-1/2 left-4 -translate-y-1/2">+62</p>
                    <input
                      type="text"
                      name="lawyerPhoneNumber"
                      id="lawyerPhoneNumber"
                      placeholder="123-456-789"
                      className="flex-1 py-2 px-4 pl-14 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="lawyerEmail" className="opacity-80">
                    Email<span className="text-[#FE000F]">*</span>
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="email"
                    name="lawyerEmail"
                    id="lawyerEmail"
                    placeholder="Your Lawyer Email"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">22.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="advisorName" className="opacity-80">
                    Does the Client Have an Advisor?
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="flex items-center space-x-4">
                    <RadioInput label="Yes" value="1" />
                    <RadioInput label="No" value="0" />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="advisorName" className="opacity-80">
                    Name
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="text"
                    name="advisorName"
                    id="advisorName"
                    placeholder="Your Advisor Name"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="advisorPhoneNumber" className="opacity-80">
                    No. Telp/Mobile
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <div className="relative">
                    <p className="absolute top-1/2 left-4 -translate-y-1/2">+62</p>
                    <input
                      type="text"
                      name="advisorPhoneNumber"
                      id="advisorPhoneNumber"
                      placeholder="123-456-789"
                      className="flex-1 py-2 px-4 pl-14 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </div>
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80"></td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="advisorEmail" className="opacity-80">
                    Email
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <input
                    type="email"
                    name="advisorEmail"
                    id="advisorEmail"
                    placeholder="Your Advisor Email"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
              <tr>
                <td className="text-sm text-[#2A2A2A] pl-1 py-2 opacity-80">24.</td>
                <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 opacity-80">
                  <label htmlFor="advisorEmail" className="opacity-80">
                    Other General Information
                  </label>
                </td>
                <td className="py-2 min-w-[352px] opacity-80">
                  <textarea
                    name="advisorEmail"
                    id="advisorEmail"
                    placeholder="Description"
                    className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                  />
                </td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>

      <div className="border-t">
        <div className="p-8 max-w-[720px]">
          <p className=" text-[#2A2A2A] font-medium mb-8">
            <span className="inline-block mr-4">I.B</span>
            Group Audit
          </p>
          <div className="px-4 overflow-auto">
            <table className='className="w-full"'>
              <tbody>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">1.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px opacity-80]">
                    <label className="opacity-80">
                      Is this form prepared for the group of business (Group Audit Engagement)?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">2.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px opacity-80]">
                    <label className="opacity-80">
                      Please attach a group structure and entities name for consolidation purposes, including
                      information on the client's percentage of ownership.
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80">3.</td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px opacity-80]">
                    <label className="opacity-80">
                      Will the Firm do the work of more than 50% of the consolidated assets, income or profit before
                      income tax?
                    </label>
                  </td>
                  <td className="py-2">
                    <div className="flex items-center space-x-4">
                      <RadioInput label="Yes" value="1" />
                      <RadioInput label="No" value="0" />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td className="text-sm text-[#2A2A2A] pl-1 py-2 align-baseline opacity-80"></td>
                  <td className="text-sm text-[#2A2A2A] py-2 pl-2 pr-5 min-w-[352px]" colSpan={2}>
                    <p className="text-sm italic font-normal mb-6">
                      If the Firm is not going to perform work more than 50% of consolidated assets, income or profit
                      before income tax from continuing operations, it will likely be difficult to achieve the adequate
                      engagement required by the group Engagement team. Therefore, documented how to obtain such
                      adequate engagement.
                    </p>
                    <p className="text-sm italic font-normal mb-6">
                      For public companies with significant foreign components, discuss with client of which significant
                      components will be visited and/or redirected in the current year.
                    </p>
                    <textarea
                      placeholder="Comment..."
                      className="flex-1 py-2 px-4 w-full bg-white border border-[#E6E6E8] rounded-lg text-[#464646] placeholder:text-[#464646]/60"
                    />
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </Accordion>
  )
}
