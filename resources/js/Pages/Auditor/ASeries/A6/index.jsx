import React from 'react'
import {Head} from '@inertiajs/inertia-react'
import Authenticated from '@/Layouts/Authenticated'
import Panel from '@/Components/Panel'
import EngagementTeam from './components/EngagementTeam'

export default function A6Form(props) {
  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title="A.6.4  Engagagement Team and Project" />

      <div className="w-ful">
        <div className="flex space-x-8">
          <div className="flex-1 overflow-auto">
            <Panel>
              <Panel.Header>
                <Panel.Back href="#" />
                <Panel.Title title="AUD - Biffco Enterprises Ltd." subtitle="Jan 01, 2014 - Dec 31, 2014" />
                <Panel.Search placeholder="Search..." />
              </Panel.Header>
              <Panel.Content>
                <div className="bg-[#F8F8FA] text-center relative">
                  <h2 className="font-semibold my-5">A.6.4 Engagagement Team and Project</h2>
                </div>
                <div className="w-full h-[calc(100vh-200px)] overflow-auto">
                  <EngagementTeam />
                </div>
              </Panel.Content>
              <Panel.Breadcrumb>
                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.existing')} />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="AUD - Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Pra - Perikatan" />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Ruang Lingkup Audit" />
                <Panel.BreadcrumbItem current text="Masalah Audit Utama" />
              </Panel.Breadcrumb>
            </Panel>
          </div>
        </div>
      </div>
    </Authenticated>
  )
}
