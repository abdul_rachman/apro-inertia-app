import React, {useState} from 'react'
import {Head} from '@inertiajs/inertia-react'
import Authenticated from '@/Layouts/Authenticated'
import Panel from '@/Components/Panel'
import {dummyDataA4Form} from './dummy-data'

export default function A4Form(props) {
  const [currentData, setCurrentData] = useState(dummyDataA4Form)

  const handleEdit = () => {
    console.log('edit function running')
  }

  const handleSave = () => {
    console.log('save function running')
  }

  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title=" A.4  Scope in Financial Statement Area" />

      <div className="w-full">
        <div className="flex space-x-8">
          <div className="flex-1 overflow-auto">
            <Panel>
              <Panel.Header>
                <Panel.Back href="#" />
                <Panel.Title title="AUD - Biffco Enterprises Ltd." subtitle="Jan 01, 2014 - Dec 31, 2014" />
                <Panel.Search placeholder="Search..." />
              </Panel.Header>
              <Panel.Content>
                <div className="bg-[#F8F8FA] text-center relative">
                  <h2 className="font-semibold my-5"> A.4 Scope in Financial Statement Area</h2>
                  <div className="flex items-center h-auto absolute top-4 right-7">
                    <button onClick={handleEdit} className="rounded-md p-1 font-bold px-6 text-blue-400" type="button">
                      Edit
                    </button>
                    <button onClick={handleSave} className="rounded-md p-1 font-bold px-6 text-blue-400" type="button">
                      Save
                    </button>
                  </div>
                </div>
                <div className="w-full h-[calc(100vh-200px)] overflow-auto">
                  <table className="table-auto border w-full">
                    <thead>
                      <tr className="bg-[#F5F5F5] sticky top-0">
                        <th className="text-left p-3 border w-[50px]">FSA Code</th>
                        <th className="text-left p-3 border">FSA Description</th>
                        <th className="text-left p-3 border">Amount</th>
                        <th className="text-left p-3 border">Scope In/(Out)</th>
                        <th className="text-left p-3 border">Relevan Assertion</th>
                        <th className="text-left p-3 border">Accounting Estimate</th>
                        <th className="text-left p-3 border">Comment</th>
                      </tr>
                    </thead>
                    <tbody>
                      {currentData.map((item, index) => (
                        <tr key={`tr-${index}`} className="border">
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row1 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row2 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row3 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row4 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row5 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row6 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row7 ?? ''}</span>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Panel.Content>
              <Panel.Breadcrumb>
                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.existing')} />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem text="AUD - Biffco Enterprises Ltd." current={true} />
              </Panel.Breadcrumb>
            </Panel>
          </div>
        </div>
      </div>
    </Authenticated>
  )
}
