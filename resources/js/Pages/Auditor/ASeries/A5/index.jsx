import {useCallback, useState} from 'react'
import {Head} from '@inertiajs/inertia-react'
import Panel from '@/Components/Panel'
import Authenticated from '@/Layouts/Authenticated'
import {dummyDataA5Form} from './dummy-data'
import AddForm from '../A4/components/AddForm'

export default function A5Form(props) {
  const [currentData, setCurrentData] = useState(dummyDataA5Form)
  const [showAddModal, setShowAddModal] = useState(false)

  const handleAdd = useCallback(newData => {
    console.log('handle add new data', newData)
  }, [])

  const handleSave = () => {
    console.log('running save function')
  }

  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title="A.5  Set Financial Statement Area & Its Cycle" />

      <div className="w-full">
        <div className="flex space-x-8">
          <div className="flex-1 overflow-auto">
            <Panel>
              <Panel.Header>
                <Panel.Back href="#" />
                <Panel.Title title="AUD - Biffco Enterprises Ltd." subtitle="Jan 01, 2014 - Dec 31, 2014" />
                <Panel.Search placeholder="Search..." />
              </Panel.Header>
              <Panel.Content>
                <div className="bg-[#F8F8FA] text-center relative">
                  <h2 className="font-semibold my-5">A.5 Set Financial Statement Area & Its Cycle</h2>
                  <div className="flex items-center h-auto absolute top-4 right-7">
                    <button
                      onClick={() => setShowAddModal(true)}
                      className="rounded-md p-1 font-bold px-6 text-blue-400"
                      type="button"
                    >
                      Add
                    </button>
                    <button onClick={handleSave} className="rounded-md p-1 font-bold px-6 text-blue-400" type="button">
                      Save
                    </button>
                  </div>
                </div>
                <div className="w-full h-[calc(100vh-200px)] overflow-auto">
                  <table className="table-auto border w-full">
                    <thead>
                      <tr className="bg-[#F5F5F5] sticky top-0">
                        <th className="text-left p-3 border w-[50px]">FSA Code</th>
                        <th className="text-left p-3 border">FSA Description</th>
                        <th className="text-left p-3 border">FRCP Cycle</th>
                        <th className="text-left p-3 border">Revenue Cycle</th>
                        <th className="text-left p-3 border">Purchase Cycle</th>
                        <th className="text-left p-3 border">Payroll Cycle</th>
                        <th className="text-left p-3 border">Treasury Cycle</th>
                        <th className="text-left p-3 border">Other Cycle</th>
                      </tr>
                    </thead>
                    <tbody>
                      {currentData.map((item, index) => (
                        <tr key={`tr-${index}`} className="border">
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row1 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row2 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row3 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row4 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row5 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row6 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row7 ?? ''}</span>
                          </td>
                          <td className="border">
                            <span className="p-2 px-3 block">{item?.row8 ?? ''}</span>
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Panel.Content>
              <Panel.Breadcrumb>
                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.existing')} />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem text="AUD - Biffco Enterprises Ltd." current={true} />
              </Panel.Breadcrumb>
            </Panel>
          </div>
        </div>
      </div>
      <AddForm isOpen={showAddModal} onClose={() => setShowAddModal(false)} onAddNewData={handleAdd} />
    </Authenticated>
  )
}
