import Accordion from '../../Components/Accordion'
import Question from '../../Components/Question'
import RadioButtonComment from '../../Components/RadioButtonComment'
import RadioButtonInline from '../../Components/RadioButtonInline'
import SelectInline from '../../Components/SelectInline'

export default function AproLibraryConsideration({answer, handleAnswer}) {
  const handleChildAnswer = (parent, id, value) => {
    handleAnswer(
      'II',
      parent,
      {
        answer: value,
      },
      id
    )
  }

  return (
    <Accordion number={'II.'} title="APRO LIBRARY CONSIDERATION">
      <Question
        number={'2.1'}
        title="The following consideration will help you to setting up your APRO library"
        className="border-b"
      >
        <RadioButtonInline
          description="Is the entity have complex organization structure?"
          name="2.1.1"
          value={answer[1][1].answer}
          onChange={e => {
            handleChildAnswer(1, 1, e.target.value)
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <RadioButtonInline
          description="Is the entity have unusual or multiple complex audit or significant accounting matters? "
          name="2.1.2"
          value={answer[1][2].answer}
          onChange={e => {
            handleChildAnswer(1, 2, e.target.value)
          }}
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <RadioButtonInline
          name="2.1.3"
          value={answer[1][3].answer}
          onChange={e => {
            handleChildAnswer(1, 3, e.target.value)
          }}
          description="Is the entity is digital financial start-up company?"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <RadioButtonInline
          name="2.1.4"
          value={answer[1][4].answer}
          onChange={e => {
            handleChildAnswer(1, 4, e.target.value)
          }}
          description="Is the entity have more than one business line/segment and simple operation?"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <RadioButtonInline
          name="2.1.5"
          value={answer[1][5].answer}
          onChange={e => {
            handleChildAnswer(1, 5, e.target.value)
          }}
          description="Is the entity is dormant company, have few transaction and/or a holding company with no requirement for consolidated its financial statements?"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
        <SelectInline
          name="2.1.6"
          value={answer[1][6].answer}
          onChange={e => {
            handleChildAnswer(1, 6, e.target.value)
          }}
          description="How much budgeted audit hours of complete team for work planned in this project?"
          options={[
            {label: 'Select...', value: ''},
            {label: '> 200 Hour', value: '> 200 Hour'},
            {label: '< 200 Hour', value: '< 200 Hour'},
          ]}
        />
      </Question>
      <Question
        number={'2.2'}
        title="Chose the method for assessing relevant control activities for all cycles in this project!"
      >
        <RadioButtonComment
          isComment={false}
          name="2.2"
          value={answer[2].answer}
          onChange={e => {
            handleAnswer('II', 2, e.target.value, 'answer')
          }}
          description="Do you plan to complete any of the following additional engagement in this file?"
          options={[
            {label: 'Control Activity Matrix', value: 'Control Activity Matrix'},
            {label: 'Risk Questionnaire', value: 'Risk Questionnaire'},
          ]}
        />
      </Question>
    </Accordion>
  )
}
