import Accordion from '../../Components/Accordion'
import Question from '../../Components/Question'
import RadioButtonComment from '../../Components/RadioButtonComment'

export default function ReportingRequirements({answer, handleAnswer}) {
    const handleChildAnswer = (parent, id, value, type = "answer") => {
        handleAnswer(
          'III',
          parent,
          {
            ...answer[parent][id],
            [type]: value,
          },
          id
        )
      }
  return (
    <Accordion number={'III.'} title="REPORTING REQUIREMENTS">
      <Question number="3.1" title="Financial Statements" className="border-b">
        <RadioButtonComment
          number="3.1.1"
          title="Is the financial statement to be audited a consolidated financial statement?"
          type="horizontal"
          className="mb-10"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
          name="3.1.1"
          value={answer[1][1].answer}
          onChange={e => {
            handleChildAnswer(1, 1, e.target.value)
          }}
          comment={answer[1][1].comment}
          onChangeComment={ e => {
            handleChildAnswer(1, 1, e.target.value, "comment")
          }}
        />
        <RadioButtonComment
          number="3.1.2"
          title="Is this engagement the audit of a component and other auditor will be using your work?"
          type="horizontal"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
          name="3.1.2"
          value={answer[1][2].answer}
          onChange={e => {
            handleChildAnswer(1, 2, e.target.value)
          }}
          comment={answer[1][2].comment}
          onChangeComment={ e => {
            handleChildAnswer(1, 2, e.target.value, "comment")
          }}
        />
      </Question>
      <Question number="3.2" title="Key Audit Matter" className="border-b">
        <RadioButtonComment
          name="3.2"
          value={answer[2].answer}
          comment={answer[2].comment}
          onChange={e => {
            handleAnswer('III', 2, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('III', 2, e.target.value, 'comment')
          }}
          description="Will the engagement team include key audit matters in the auditor’s report (ISA 701)"
          type="horizontal"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
      <Question number="3.3" title="Annual Report" className="border-b">
        <RadioButtonComment
          name="3.3"
          value={answer[3].answer}
          comment={answer[3].comment}
          onChange={e => {
            handleAnswer('III', 3, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('III', 3, e.target.value, 'comment')
          }}
          description="Does the entity issue an annual report?"
          type="horizontal"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
      <Question number="3.4" title="Filling with a Foreign Regulator">
        <RadioButtonComment
          name="3.4"
          value={answer[4].answer}
          comment={answer[4].comment}
          onChange={e => {
            handleAnswer('III', 4, e.target.value, 'answer')
          }}
          onChangeComment={e => {
            handleAnswer('III', 4, e.target.value, 'comment')
          }}
          description="Will the client be filling any financial information including financial statements with regulator in foreign jurisdiction?"
          type="horizontal"
          options={[
            {label: 'Yes', value: 'Yes'},
            {label: 'No', value: 'No'},
          ]}
        />
      </Question>
    </Accordion>
  )
}
