export default function SelectInline({description, options, value, onChange}) {
  return (
    <div className="flex justify-between text-[#2a2a2acc] mb-4 items-center">
      <p>{description}</p>
      <div className="flex text-sm gap-3 text-[#2a2a2acc]">
        <select className="border rounded-lg border-[#CDCDCD] text-sm" onChange={onChange} value={value}>
          {options.map((item, index) => (
            <option key={`select-${index}`} value={item.value}>
              {item.label}
            </option>
          ))}
        </select>
      </div>
    </div>
  )
}
