export default function RadioButtonInline({description, options, onChange, name, value}) {
  return (
    <div className="flex justify-between text-[#2a2a2acc] mb-4 items-center">
      <p>{description}</p>
      <div className="flex text-sm gap-3 text-[#2a2a2acc]">
        {options.map((item, index) => (
          <label className="flex gap-2 items-center">
            <input type="radio" name={name} checked={value === item.value} onChange={onChange} value={item.value} />
            <span>{item.label}</span>
          </label>
        ))}
      </div>
    </div>
  )
}
