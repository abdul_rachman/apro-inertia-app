export default function RadioButtonComment({
  number,
  title,
  description,
  value,
  name="",
  onChange,
  options = [],
  type = 'vertical',
  comment,
  onChangeComment,
  className = '',
  isComment = true,
}) {
  return (
    <div className={className}>
      {number && title ? (
        <div className="flex font-medium text-[#2A2A2A] mb-6">
          <div className="w-8 mr-2">
            <span>{number}</span>
          </div>
          <div className="flex-1">
            <span>{title}</span>
          </div>
        </div>
      ) : null}
      {description ? <p className="mb-4 text-[#2a2a2acc]">{description}</p> : null}
      <div
        className={`flex ${
          type === 'vertical' ? 'flex-col' : 'flex-row'
        } gap-4 text-sm text-[#2a2a2acc] mb-3`}
      >
        {options.map((item, index) => (
          <label className="flex gap-2 items-center">
            <input
              type="radio"
              name={name}
              checked={value === item.value}
              onChange={onChange}
              value={item.value}
            />
            <span>{item.label}</span>
          </label>
        ))}
      </div>
      {isComment ? (
        <textarea
          className="w-full mt-3 border border-[#CDCDCD] bg-[#f5f5f566] rounded-lg"
          placeholder="Comment"
          value={comment}
          rows="6"
          onChange={onChangeComment}
        />
      ) : null}
    </div>
  )
}
