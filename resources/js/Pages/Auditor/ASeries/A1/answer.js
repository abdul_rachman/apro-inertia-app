export const answerData = {
  I: {
    1: {
      answer: 'Yes',
      comment: 'testing',
    },
    2: {
      answer: '',
      comment: '',
    },
  },
  II: {
    1: {
      1: {
        answer: '',
      },
      2: {
        answer: '',
      },
      3: {
        answer: '',
      },
      4: {
        answer: '',
      },
      5: {
        answer: '',
      },
      6: {
        answer: '',
      },
    },
    2: {
      answer: '',
      comment: '',
    },
  },
  III: {
    1: {
      1: {
        answer: '',
        comment: '',
      },
      2: {
        answer: '',
        comment: '',
      },
    },
    2: {
      answer: '',
      comment: '',
    },
    3: {
      answer: '',
      comment: '',
    },
    4: {
      answer: '',
      comment: '',
    },
  },
  IV: {
    1: {
      answer: '',
      comment: '',
    },
    2: {
      answer: '',
      comment: '',
    },
    3: {
      answer: '',
      comment: '',
    },
    4: {
      answer: '',
      comment: '',
    },
  },
  SIGNOF: {
    // 1:[
    //   {
    //     no:1,
    //     sign_off_by_initial: RR
    //     sign_off_role_id: 9 (auditor role id)
    //     sign_off_role_name: Approver (auditor role name)
    //     sign_off_by: 1 (user id)
    //     time_consume: 00:20
    //     date: 2023-05-19 11:00:12 (WIB)
    //     project_role_id: 2 (auditor role id)
    //     project_role_name: Partner (auditor role name)
    //     approval_to: 2 (user id)
    //   },
    //   {
    //     no:2,
    //     sign_off_by_initial: RR
    //     sign_off_role_id: 9 (auditor role id)
    //     sign_off_role_name: Approver (auditor role name)
    //     sign_off_by: 1 (user id)
    //     time_consume: 00:20
    //     date: 2023-05-19 11:00:12 (WIB)
    //     project_role_id: 2 (auditor role id)
    //     project_role_name: Partner (auditor role name)
    //     approval_to: 2 (user id)
    //   },
    // ],
    // 2:[
    //   {
    //     no:1,
    //     sign_off_by_initial: RR
    //     sign_off_role_id: 9 (auditor role id)
    //     sign_off_role_name: Approver (auditor role name)
    //     sign_off_by: 1 (user id)
    //     time_consume: 00:20
    //     date: 2023-05-19 11:00:12 (WIB)
    //     project_role_id: 2 (auditor role id)
    //     project_role_name: Partner (auditor role name)
    //     approval_to: 2 (user id)
    //   },
    //   {
    //     no:2,
    //     sign_off_by_initial: RR
    //     sign_off_role_id: 9 (auditor role id)
    //     sign_off_role_name: Approver (auditor role name)
    //     sign_off_by: 1 (user id)
    //     time_consume: 00:20
    //     date: 2023-05-19 11:00:12 (WIB)
    //     project_role_id: 2 (auditor role id)
    //     project_role_name: Partner (auditor role name)
    //     approval_to: 2 (user id)
    //   },
    // ],
  },
}
