import React, { useState, useRef } from "react";
import Authenticated from "@/Layouts/Authenticated";
import { Head } from "@inertiajs/inertia-react";
import Panel from "@/Components/Panel";
import ListMenu from "@/Components/ListMenu";
import moment from "moment";
import { Resizable } from "re-resizable";

export default function ExistingProject({ clients, ...props }) {
    const [active, setActive] = useState(clients[0])

    return (
        <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
            <Head title="Existing Project" />

            <div className="w-full">
                <div className="flex space-x-5">
                    <div className="flex-1">
                        <Panel>
                            <Panel.Header>
                                <Panel.Back href={route('auditor.projects.index')} />
                                <Panel.Title title="Existing Project" />
                                <Panel.Search placeholder="Search..." />
                            </Panel.Header>
                            <Panel.Content>
                                <div className="w-full flex overflow-hidden">
                                    <Resizable
                                        className="flex items-start justify-center border-r-8 border-[#b5b5c318] bg-white h-[calc(100vh-132px)]"
                                        defaultSize={{
                                            width: '50%',
                                        }}
                                        maxWidth="100%"
                                        minWidth="1"
                                        enable={{ top:false, right:true, bottom:false, left:false, topRight:false, bottomRight:false, bottomLeft:false, topLeft:false }}
                                    >
                                        <div className="flex-1 bg-white py-6">
                                            {
                                                clients?.map((item, index) => (
                                                    <ListMenu onClick={() => setActive(item)} key={`list-company-${index}`}>
                                                        <ListMenu.Group>
                                                            <ListMenu.Title text={item?.name} />
                                                        </ListMenu.Group>
                                                        <ListMenu.Group center={true} right={true}>
                                                            <ListMenu.Subtitle text={moment(item?.created_at).format("MMM DD, YYYY")} />
                                                            <ListMenu.Icon />
                                                        </ListMenu.Group>
                                                    </ListMenu>
                                                ))
                                            }
                                        </div>
                                    </Resizable>
                                    <div className="flex items-start justify-center bg-white h-[calc(100vh-132px)]" style={{ width: '100%', minWidth: '1px' }}>
                                        <div className="flex-1 bg-white py-6">
                                            {
                                                active?.projects?.map((item, index) => (
                                                    <ListMenu key={`project-${index}`} href={route('auditor.audit-stages.index', { project_id: item.id })}>
                                                        <ListMenu.Group type="col">
                                                            <ListMenu.Title text={`${item?.workspace_type} - ${item?.workspace}`} />
                                                            <ListMenu.Subtitle text={`${moment(item?.start_date).format("MMM DD, YYYY")} - ${moment(item?.end_date).format("MMM DD, YYYY")}`} />
                                                        </ListMenu.Group>
                                                        <ListMenu.Group center={true} right={true}>
                                                            <ListMenu.Badge count={56} />
                                                            <ListMenu.Subtitle text={moment(item?.updated_at).format("MMM DD, YYYY")} />
                                                        </ListMenu.Group>
                                                    </ListMenu>
                                                ))
                                            }
                                        </div>
                                    </div>
                                </div>
                                {/* <div className="flex space-x-4 bg-[#b5b5c318] h-[calc(100vh-132px)]">
                                    <div className="flex-1 bg-white py-6">
                                        {
                                            clients?.map((item, index) => (
                                                <ListMenu onClick={() => setActive(item)} key={`list-company-${index}`}>
                                                    <ListMenu.Group>
                                                        <ListMenu.Title text={item?.name} />
                                                    </ListMenu.Group>
                                                    <ListMenu.Group center={true} right={true}>
                                                        <ListMenu.Subtitle text={moment(item?.created_at).format("MMM DD, YYYY")} />
                                                        <ListMenu.Icon />
                                                    </ListMenu.Group>
                                                </ListMenu>
                                            ))
                                        }
                                    </div>

                                    <div className="flex-1 bg-white py-6">
                                        {
                                            active?.projects?.map((item, index) => (
                                                <ListMenu key={`project-${index}`} href={route('auditor.audit-stages.index', { project_id: item.id })}>
                                                    <ListMenu.Group type="col">
                                                        <ListMenu.Title text={`${item?.workspace_type} - ${item?.workspace}`} />
                                                        <ListMenu.Subtitle text={`${moment(item?.start_date).format("MMM DD, YYYY")} - ${moment(item?.end_date).format("MMM DD, YYYY")}`} />
                                                    </ListMenu.Group>
                                                    <ListMenu.Group center={true} right={true}>
                                                        <ListMenu.Badge count={56} />
                                                        <ListMenu.Subtitle text={moment(item?.updated_at).format("MMM DD, YYYY")} />
                                                    </ListMenu.Group>
                                                </ListMenu>
                                            ))
                                        }
                                    </div>
                                </div> */}
                            </Panel.Content>
                            <Panel.Breadcrumb>
                                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.index')} />
                                <Panel.BreadcrumbItem
                                    text={active?.name}
                                    current={true}
                                />
                            </Panel.Breadcrumb>
                        </Panel>
                    </div>
                </div>
            </div>
        </Authenticated>
    );
}
