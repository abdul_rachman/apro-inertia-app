import React, {useState, useRef} from 'react'
import Authenticated from '@/Layouts/Authenticated'
import {Head} from '@inertiajs/inertia-react'
import Panel from '@/Components/Panel'
import Swal from 'sweetalert2'

export default function TrialBalance({project_id = 1, ...props}) {
  const [errorIndex, setErrorIndex] = useState(undefined)
  const [errorRow, setErrorRow] = useState([])
  const [hide, setHide] = useState(0)
  const [drag, setDrag] = useState(false)
  const [currentData, setCurrentData] = useState([
    {
      row1: '',
      row2: '',
      row3: '1101 Cash and Cash Equivalent',
      row4: '',
      row5: '',
      row6: 100,
      row7: '',
      row8: 50,
      row9: '',
      row10: '',
      row11: '',
      row12: '',
      row13: '',
      row14: '',
    },
  ])

  const handleChange = (key, value, index) => {
    const temp = [...currentData]
    temp[index][key] = value
    if (temp.length === index + 1) {
      setCurrentData([
        ...temp,
        {
          row1: '',
          row2: '',
          row3: '',
          row4: '',
          row5: '',
          row6: '',
          row7: '',
          row8: '',
          row9: '',
          row10: '',
          row11: '',
          row12: '',
          row13: '',
          row14: '',
        },
      ])
    } else {
      setCurrentData(temp)
    }
  }

  const handleSave = () => {
    setErrorIndex(undefined)
    setErrorRow([])
    checkDuplicateRSACode()
    checkEmptyValue()
    if (errorIndex === undefined) {
      console.log('values', currentData)
    }
  }

  const checkEmptyValue = () => {
    for (let i = 0; i < currentData.length - 1; i++) {
      let br = false
      let tempRow = []
      Object.keys(currentData[i]).forEach(key => {
        if (currentData[i][key] === '') {
          setErrorIndex(i)
          tempRow.push(key)
          br = true
        }
      })
      if (br) {
        setErrorRow(tempRow)
        break
      }
    }
  }

  const checkDuplicateRSACode = () => {
    const valueArr = currentData.map(function (item) {
      return item.row4
    })
    const isDuplicate = valueArr.some(function (item, idx) {
      if (valueArr.indexOf(item) != idx) {
        setErrorIndex(idx)
        setErrorRow([...errorRow, 'row4'])
        return true
      } else {
        return false
      }
    })
    return isDuplicate
  }

  const handleDelete = index => {
    Swal.fire({
      title: 'Are you sure?',
      text: "You won't be able to revert this!",
      icon: 'warning',
      iconColor: '#ff5050',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Yes, delete it!',
    }).then(result => {
      if (result.isConfirmed) {
        const tmp = [...currentData]
        tmp.splice(index, 1)
        setCurrentData(tmp)
        Swal.fire({
          title: 'Deleted!',
          icon: 'success',
          iconColor: '#05e200',
          text: 'Your file has been deleted.',
          confirmButtonColor: '#05e200',
        })
      }
    })
  }

  const handleStart = (e, row, col) => {
    let iniMouse = e.clientX
    let iniSize = document.getElementById(`${row}${col}`).offsetWidth

    console.log('iniSize', iniSize)

    setDrag({
      iniMouse: iniMouse,
      iniSize: iniSize,
    })
  }

  const handleMove = (e, row, col) => {
    console.log('row', row)
    console.log('col', col)
    if (e.clientX) {
      let iniMouse = drag.iniMouse
      let iniSize = drag.iniSize
      let endMouse = e.clientX

      let endSize = iniSize + (endMouse - iniMouse)

      const temp = document.getElementById(`${row}${col}`)
      console.log('temp', temp)

      document.getElementById(`${row}${col}`).style.width = `${endSize}px`
    }
  }

  return (
    <Authenticated auth={props.auth} errors={props.errors} header={false} childrenPadding={false}>
      <Head title="Trial Balance" />

      <div className="w-ful">
        <div className="flex space-x-8">
          <div className="flex-1 overflow-auto">
            <Panel>
              <Panel.Header>
                <Panel.Back
                  href={route('auditor.audit-stages.index', {
                    project_id,
                  })}
                />
                <Panel.Title title="AUD - Biffco Enterprises Ltd." subtitle="Jan 01, 2014 - Dec 31, 2014" />
                <Panel.Search placeholder="Search..." />
              </Panel.Header>
              <Panel.Content>
                <div className="bg-[#F8F8FA] text-center relative">
                  <h2 className="font-semibold my-5">Trial Balance</h2>
                  <button
                    className="rounded-md p-1 font-bold px-6 absolute top-4 right-7 flex items-center justify-center gap-2"
                    title='Import / Export'
                    onClick={handleSave}
                  >
                    <svg fill="transparent" className="w-6 h-6" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                      <path
                        fill="#63D9C6"
                        d="M19.7903934,18.6127185 L19.7072026,18.7069258 L16.7071326,21.7069258 C16.6801187,21.7339397 16.6515664,21.7594153 16.6216183,21.7832098 L16.500353,21.8659223 L16.500353,21.8659223 L16.427064,21.9043128 L16.427064,21.9043128 L16.3400271,21.9405322 L16.3400271,21.9405322 L16.2335653,21.9723902 L16.2335653,21.9723902 L16.116647,21.9930913 L16.033029,21.9992768 L16.033029,21.9992768 L15.9409671,21.9980859 L15.8251966,21.9845213 L15.8251966,21.9845213 L15.6878494,21.9500809 L15.6878494,21.9500809 L15.5767675,21.9061457 L15.5767675,21.9061457 L15.4792778,21.8538236 L15.4792778,21.8538236 L15.3832241,21.7870331 L15.2928749,21.7069258 L12.2927974,18.7069258 C11.902263,18.3164015 11.902263,17.6832365 12.2927974,17.2927122 C12.6532907,16.9322283 13.2205364,16.9044987 13.6128377,17.2095236 L13.7070475,17.2927122 L14.9998966,18.584819 L14.9999741,8.99981902 C14.9999741,8.48698318 15.3860143,8.06431186 15.883353,8.00654675 L16.0000259,7.99981902 C16.5523106,7.99981902 17.0000259,8.44753427 17.0000259,8.99981902 L16.9998966,18.584819 L18.2929525,17.2927122 C18.6534458,16.9322283 19.2206915,16.9044987 19.6129929,17.2095236 L19.7072026,17.2927122 C20.0376548,17.6231559 20.0884936,18.1273245 19.859719,18.511222 L19.7903934,18.6127185 L19.7903934,18.6127185 Z M4.29279737,5.29255711 L7.29286736,2.29255711 L7.40481484,2.1959774 L7.51569719,2.12453966 L7.51569719,2.12453966 L7.62891562,2.07076785 L7.62891562,2.07076785 L7.73413453,2.03538486 L7.73413453,2.03538486 L7.82519664,2.01496161 L7.82519664,2.01496161 L7.94096709,2.00139699 L8.05914398,2.00139699 L8.05914398,2.00139699 L8.17466132,2.0149356 L8.17466132,2.0149356 L8.31274961,2.04953478 L8.31274961,2.04953478 L8.36670687,2.06905084 L8.45385903,2.10832658 L8.45385903,2.10832658 L8.52068604,2.14573132 L8.52068604,2.14573132 L8.60170489,2.20078783 L8.60170489,2.20078783 L8.66547577,2.25320781 L8.66547577,2.25320781 L8.70713264,2.29255711 L11.7072026,5.29255711 L11.7903934,5.38676445 C12.0700068,5.74636472 12.0700068,6.25296306 11.7903934,6.61256333 L11.7072026,6.70677067 L11.6129929,6.78995928 C11.2533833,7.06956543 10.7467718,7.06956543 10.3871623,6.78995928 L10.2929525,6.70677067 L8.99989658,5.41466389 L9.00002585,14.9996639 C9.00002585,15.5124997 8.61398566,15.9351711 8.11664698,15.9929362 L8.00002585,15.9996639 L7.88335302,15.9929362 C7.42427116,15.9396145 7.06002351,15.5753669 7.00670188,15.116285 L6.99997415,14.9996639 L6.99989658,5.41466389 L5.7070475,6.70677067 L5.61283773,6.78995928 C5.22053638,7.09498417 4.65329066,7.06725463 4.29279737,6.70677067 C3.93230409,6.34628671 3.90457384,5.77905565 4.20960662,5.38676445 L4.29279737,5.29255711 Z"
                      />
                    </svg>
                  </button>
                  <button
                    className="rounded-md p-1 font-bold px-6 absolute top-4 right-16 flex items-center justify-center gap-2"
                    title='Save'
                    onClick={handleSave}
                  >
                    <svg
                      className="h-6 w-6"
                      version="1.1"
                      id="Capa_1"
                      xmlns="http://www.w3.org/2000/svg"
                      viewBox="0 0 27.971 27.971"
                    >
                      <g>
                        <g id="c189_arrow">
                          <path
                            fill="#63D9C6"
                            d="M23.92,14.746l-4.05-4.051h2.374l-0.068-0.177c-1.407-3.561-4.882-6.088-8.95-6.088c-5.312,0-9.62,4.307-9.62,9.616
			c0,5.316,4.308,9.623,9.62,9.623c3.907,0,7.271-2.128,8.775-5.479l3.854,0.039c-0.013,0.03-3.032,8.918-12.693,8.918
			C5.893,27.148,0,21.254,0,13.987C0,6.715,5.893,0.824,13.161,0.824c6.08,0,11.195,4.116,12.709,9.715l0.032,0.156h2.069
			L23.92,14.746z"
                          />
                        </g>
                        <g id="Capa_1_105_"></g>
                      </g>
                    </svg>
                  </button>
                </div>
                <div className="w-full h-[calc(100vh-200px)] overflow-auto">
                  <table className="table-auto border w-full">
                    <thead>
                      <tr className="bg-[#F5F5F5] sticky top-0">
                        <th id="row1" className="text-left p-3 border w-[150px] relative">
                          Client COA{' '}
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '1')}
                            onDrag={e => handleMove(e, 'row', '1')}
                          />
                        </th>
                        <th id="row2" className="text-left p-3 border w-[150px] relative">
                          COA Description
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '2')}
                            onDrag={e => handleMove(e, 'row', '2')}
                          />
                        </th>
                        <th id="row3" className="text-left p-3 border w-[425px] relative">
                          FSA Code & Description{' '}
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '3')}
                            onDrag={e => handleMove(e, 'row', '3')}
                          />
                        </th>
                        <th id="row4" className="text-left p-3 border w-[150px] relative">
                          {' '}
                          Unaudited Balance{' '}
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '4')}
                            onDrag={e => handleMove(e, 'row', '4')}
                          />
                        </th>
                        <th id="row5" className="text-left p-3 border relative">
                          {' '}
                          AJE Ref{' '}
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '5')}
                            onDrag={e => handleMove(e, 'row', '5')}
                          />
                        </th>
                        <th id="row6" className="text-left p-3 border w-[150px] relative">
                          Debit
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '6')}
                            onDrag={e => handleMove(e, 'row', '6')}
                          />
                        </th>
                        <th id="row7" className="text-left p-3 border relative">
                          AJE Ref{' '}
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '7')}
                            onDrag={e => handleMove(e, 'row', '7')}
                          />
                        </th>
                        <th id="row8" className="text-left p-3 border relative">
                          Credit{' '}
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '8')}
                            onDrag={e => handleMove(e, 'row', '8')}
                          />
                        </th>
                        <th id="row9" className="text-left p-3 border relative">
                          Audited Balance
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '9')}
                            onDrag={e => handleMove(e, 'row', '9')}
                          />
                        </th>
                        <th id="row10" className="text-left p-3 border relative">
                          Balance Prior year
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '10')}
                            onDrag={e => handleMove(e, 'row', '10')}
                          />
                        </th>
                        <th id="row11" className={`text-left p-3 border`}>
                          Balance Prior year
                          <div
                            className="cursor-col-resize h-full absolute -right-1 top-0 w-2 z-10"
                            draggable={true}
                            onDragStart={e => handleStart(e, 'row', '11')}
                            onDrag={e => handleMove(e, 'row', '11')}
                          />
                        </th>
                      </tr>
                    </thead>
                    <tbody>
                      {currentData.map((item, index) => (
                        <tr key={`tr-${index}`} className={`border`}>
                          <td className="border">
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="Client COA"
                              value={item?.row1}
                              onChange={e => handleChange('row1', e.target.value, index)}
                            />
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row2') ? 'bg-red-500' : ''
                            }`}
                          >
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="COA Description"
                              value={item?.row2}
                              onChange={e => handleChange('row2', e.target.value, index)}
                            />
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row3') ? 'bg-red-500' : ''
                            }`}
                          >
                            <select
                              className="border-none w-full"
                              value={item?.row3}
                              onChange={e => {
                                handleChange('row3', e.target.value, index)
                              }}
                            >
                              <option value="">FSA Code & Description...</option>
                              <option value="1101 Cash and Cash Equivalent">1101 Cash and Cash Equivalent</option>
                              <option value="1102 Time Deposit">1102 Time Deposit</option>
                              <option value="1103 Derivative Assets">1103 Derivative Assets</option>
                            </select>
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row4') ? 'bg-red-500' : ''
                            }`}
                          >
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="Unaudited Balance"
                              value={item?.row4}
                              maxLength={6}
                              onChange={e => handleChange('row4', e.target.value, index)}
                            />
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row5') ? 'bg-red-500' : ''
                            }`}
                          >
                            {item?.row5}
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row6') ? 'bg-red-500' : ''
                            }`}
                          >
                            {item?.row6}
                          </td>
                          <td className="border p-1 text-center">{item?.row7}</td>
                          <td className="border p-1 text-center">{item?.row8}</td>
                          <td className="border p-1 text-center">
                            {item?.row6 !== '' && item?.row8 !== ''
                              ? parseFloat(item?.row4) + parseFloat(item?.row6) - parseFloat(item?.row8)
                              : item?.row4}
                          </td>
                          <td
                            className={`border ${
                              errorIndex === index && errorRow.includes('row10') ? 'bg-red-500' : ''
                            }`}
                          >
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="Balance Prior year"
                              value={item?.row10}
                              maxLength={6}
                              onChange={e => handleChange('row10', e.target.value, index)}
                            />
                          </td>
                          <td
                            className={`border ${hide < 1 ? 'hidden' : ''} ${
                              errorIndex === index && errorRow.includes('row11') ? 'bg-red-500' : ''
                            }`}
                          >
                            <input
                              className="border-none w-full"
                              type="text"
                              placeholder="Balance Prior year"
                              value={item?.row11}
                              maxLength={6}
                              onChange={e => handleChange('row11', e.target.value, index)}
                            />
                          </td>
                        </tr>
                      ))}
                    </tbody>
                  </table>
                </div>
              </Panel.Content>
              <Panel.Breadcrumb>
                <Panel.BreadcrumbItem text="Existing Project" href={route('auditor.projects.existing')} />
                <Panel.BreadcrumbItem href={route('auditor.projects.existing')} text="Biffco Enterprises Ltd." />
                <Panel.BreadcrumbItem text="AUD - Biffco Enterprises Ltd." current={true} />
              </Panel.Breadcrumb>
            </Panel>
          </div>
        </div>
      </div>
    </Authenticated>
  )
}

const HeaderRowCustom = props => <tr className="bg-[#F5F5F5] sticky top-0" {...props} />
