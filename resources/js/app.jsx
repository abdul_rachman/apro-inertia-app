import './bootstrap'
import '../css/app.css'

import React from 'react'
import {render} from 'react-dom'
import {createInertiaApp} from '@inertiajs/inertia-react'
import {InertiaProgress} from '@inertiajs/progress'
import {resolvePageComponent} from 'laravel-vite-plugin/inertia-helpers'
import {I18nextProvider} from 'react-i18next'
import i18next from 'i18next'
import common_id from './translations/id/common.json'
import common_en from './translations/en/common.json'

const appName = window.document.getElementsByTagName('title')[0]?.innerText || 'Laravel'

i18next.init({
  interpolation: {escapeValue: false},
  lng: 'id',
  resources: {
    id: {
      common: common_id,
    },
    en: {
      common: common_en,
    },
  },
})

createInertiaApp({
  title: title => `${title} - ${appName}`,
  resolve: name => resolvePageComponent(`./Pages/${name}.jsx`, import.meta.glob('./Pages/**/*.jsx')),
  setup({el, App, props}) {
    return render(
      <I18nextProvider i18n={i18next}>
        <App {...props} />
      </I18nextProvider>,
      el
    )
  },
})

InertiaProgress.init({color: '#4B5563'})
