<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }}</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;600;700&display=swap">

        <style type="text/css">
            body{
                font-family: Roboto, 'sans-serif', Arial;
                font-size: 12px;
            }
            .text-md{
                font-size: 14px;
            }
            .text-center{
                text-align: center;
            }
            .text-justify{
                text-align: justify;
            }
            .text-secondary{
                color: #787878;
            }
            table{
                border-collapse: collapse;
                width: 100%;
            }

            #logo{
                width: 120px;
                position: absolute;
                top: 0;
                right: 0;
            }
            .container{
                margin-top: 60px;
            }
            .content{
                margin-top: 7px;
            }
            #contact{
                width: 100%;
                position: absolute;
                bottom: 130px;
            }
            footer{
                width: 100%;
                position: absolute;
                bottom: 0;
                border-top: 1px solid #c1c1c1;
                padding-top: 10px;
                text-align: center;
            }
        </style>

        @yield('css')
    </head>

    <body>
        <div class="container">
            <header>
                <img id="logo" src="{{ public_path('images/logo.png') }}" alt="">
            </header>

            <section class="content">
                @yield('content')
            </section>

            <section id="contact">
                <div class="text-justify" style="margin-bottom: 10px;">
                    This invoice was generated automatically, if you need hardcopy of this invoice for your purpose please do not hesitate request to us through:
                </div>

                <div>WhatsApp : +62 812 1000 9283</div>
                <div>Email : info@apro.co.id</div>
            </section>

            <footer>
                <div><b>PT Karsa Media Digital</b></div>
                <div>Jl. Kertawibawa, Mutiara Land, No. B05, Karanglewas,</div>
                <div>Purwokerto-Banyumas, Central Java, P.O. Box. 53161</div>
                <div>Indonesia</div>
            </footer>
        </div>
    </body>
</html>
