<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>{{ config('app.name') }}</title>

        <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;600;700&display=swap">
    </head>

    <body>
        <div style="padding: 1rem; font-family: Roboto; font-size: 12px; color: #333; line-height: 1.25rem; background: #eee;">
            
            <div style="width: 100%; max-width: 550px; margin-right: auto; margin-left: auto;">
                <div style="margin-bottom: 20px; text-align: center;">
                    <img src="{{ asset('images/logo.png') }}" alt="" style="width: 80px;">
                </div>

                <div style="background: #fff; border-radius: 20px; padding: 20px 30px;">

                    @yield('content')

                </div>

                {{-- Begin::Footer --}}
                <div style="margin-top: 20px; font-size: 10px; line-height: 1.15rem; padding: 0 30px;">
                    <div>
                        Pesan ini dibuat secara otomatis, mohon tidak membalas. Jika butuh bantuan, silakan hubungi kami melalui email hello@apro.co.id
                    </div>

                    <div style="margin-top: 20px; text-align: center;">
                        Copyright &copy; {{ date('Y') .' '. config('app.name') }}. All Rights Reserved.
                    </div>
                </div>
                {{-- End::Footer --}}
            </div>

        </div>
    </body>
</html>
