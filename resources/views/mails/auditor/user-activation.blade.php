@extends('mails.auditor._layout')

@section('content')
    <div style="min-height: 150px;">
        <div>
            ID Perusahaan Anda: <b>{{ $company_code }}</b>.
        </div>
        <div>
            Silakan klik tombol berikut untuk aktivasi akun Anda.
        </div>
        <div style="text-align: center; margin-top: 10px; margin-bottom: 20px;">
            <a href="{{ $url }}" target="_blank"
                style="padding: 5px 10px; background: #3F8DFD; color: #FFF;" 
            >Aktivasi Akun</a>
        </div>
        <div>
            Jika tombol tidak bekerja, silakan copy-paste link berikut ke browser:
            <br>
            {{ $url }}
        </div>
    </div>
@endsection
