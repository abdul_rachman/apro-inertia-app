@extends('mails.company._layout')

@section('content')
    <div style="min-height: 150px;">
        <b>Dear {{ $user->company->name }}</b>

        <div style="margin-top: 10px;">
            {{ $user->incomplete_reason }}
        </div>

        <div style="margin-top: 30px;">
            Regards,
        </div>
        <div>
            {{ config('app.name') }}
        </div>
    </div>
@endsection
