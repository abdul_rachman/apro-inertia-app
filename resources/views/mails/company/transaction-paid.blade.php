@extends('mails.company._layout')

@section('content')
    <div style="min-height: 150px;">
        <b style="font-size: 14px;">Successfully Payment</b>

        <div style="margin-top: 15px;">
            <b>Dear {{ $user->company->name }},</b>
        </div>
        <div>
            {{ $user->company->address }}
        </div>

        <div style="margin-top: 10px">
            Thank you! 
        </div>
        <div>
            Now you can fully use and access APRO software. Here are your Payment Details.
        </div>

        <!-- Begin::Main Information -->
        <div style="margin-top: 15px; background: #eee; border-radius: 8px; border: 1px solid #c1c1c1; padding: 10px;">
            <table width="100%">
                <tbody>
                    <tr>
                        <td style="padding: 0;">Total payment</td>
                        <td style="padding: 0; text-align: right; color: #FF1C03;">
                            {{ thousandFormat($transaction->total_amount, 'Rp') }}
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0;">Payment method</td>
                        <td style="padding: 0; text-align: right;">
                            {{ ucwords(str_replace('_', ' ', $transaction->payment->payment_type)) }}
                            @isset($transaction->payment->payment_channel)
                                {{ ' - '. strtoupper($transaction->payment->payment_channel) }}
                            @endisset
                        </td>
                    </tr>

                    @isset($transaction->payment->biller_code)
                        <td style="padding: 0;">Biller code</td>
                        <td style="padding: 0; text-align: right;">
                            {{ $transaction->payment->biller_code }}
                        </td>
                    @endisset

                    <tr>
                        <td style="padding: 0;">
                            @if(isset($transaction->payment->va_number))
                                Virtual Account
                            @elseif(isset($transaction->payment->biller_code) || isset($transaction->payment->payment_code))
                                Bill key
                            @endif
                        </td>
                        <td style="padding: 0; text-align: right;">
                            @if(isset($transaction->payment->va_number))
                                {{ $transaction->payment->va_number }}
                            @elseif(isset($transaction->payment->bill_key))
                                {{ $transaction->payment->bill_key }}
                            @elseif(isset($transaction->payment->payment_code))
                                {{ $transaction->payment->payment_code }}
                            @endif
                        </td>
                    </tr>
                    <tr>
                        <td style="padding: 0;">Payment time</td>
                        <td style="padding: 0; text-align: right; color: #FF1C03;">
                            {{ date('j F Y H:i', strtotime($transaction->payment->transaction_date)) }} WIB
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- End::Main Information -->

        <!-- Begin::Payment Summary -->
        <h3 style="margin-top: 15px; margin-bottom: 10px; font-size: 14px;">
            Payment Summary
        </h3>

        <div style="border-radius: 8px; border: 1px solid #c1c1c1; padding: 10px;">
            <table width="100%">
                <tbody>
                    <tr>
                        <td colspan="2" style="padding: 0; text-align: left;">Order Number</td>
                        <td colspan="3" style="padding: 0; text-align: right; color: #787878;">
                            {{ $transaction->invoice_number }}
                        </td>
                    </tr>
                </tbody>
            </table>

            <table width="100%" style="margin-top: 10px; border-collapse: collapse;">
                <thead>
                    <tr>
                        <th style="padding: 0; text-align: left;">Order Description</th>
                        <th style="padding: 0; text-align: right;">Unit</th>
                        <th style="padding: 0; text-align: right;">Period (Month)</th>
                        <th style="padding: 0; text-align: right;">Unit Price/Month</th>
                        <th style="padding: 0; text-align: right;">Total Price</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style="padding: 0; text-align: left; display: list-item; margin-left: 20px;">
                            {{ $transaction->package_detail['name'] }}
                        </td>
                        <td style="padding: 0; text-align: right;">
                            1 Package
                        </td>
                        <td style="padding: 0; text-align: right;">
                            {{ $transaction->period }}
                        </td>
                        <td style="padding: 0; text-align: right;">
                            {{ thousandFormat($transaction->package_detail['price'], 'Rp') }}
                        </td>
                        <td style="padding: 0; text-align: right;">
                            {{ thousandFormat($transaction->package_detail['price'] * $transaction->period, 'Rp') }}
                        </td>
                    </tr>

                    @isset($transaction->package_add_ons)
                        @foreach($transaction->package_add_ons as $add_on)
                            @if( $add_on['qty'] != "0")
                            <tr>
                                <td style="padding: 0; text-align: left; display: list-item; margin-left: 20px;">
                                    {{ $add_on['name'] }}
                                </td>
                                <td style="padding: 0; text-align: right;">
                                    {{ $add_on['qty'] }} {{ $add_on['unit'] }}
                                </td>
                                <td style="padding: 0; text-align: right;">
                                    {{ $transaction->period }}
                                </td>
                                <td style="padding: 0; text-align: right;">
                                    {{ thousandFormat($add_on['price'], 'Rp') }}
                                </td>
                                <td style="padding: 0; text-align: right;">
                                    {{ thousandFormat($add_on['total_price'], 'Rp') }}
                                </td>
                            </tr>
                            @endif
                        @endforeach
                    @endisset

                    <tr>
                        <td colspan="5" style="padding: 2px 0;">
                            <hr style="border-top: 1px solid #c1c1c1; border-bottom: none; border-left: none; border-right: none;">
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2" style="padding: 0; text-align: left;">Total</th>
                        <th colspan="3" style="padding: 0; text-align: right;">
                            {{ thousandFormat($transaction->amount, 'Rp') }}
                        </th>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 0; text-align: left;">VAT 11%</td>
                        <td colspan="3" style="padding: 0; text-align: right;">
                            {{ thousandFormat($transaction->tax_amount, 'Rp') }}
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 0; text-align: left;">Promo discount</td>
                        <td colspan="3" style="padding: 0; text-align: right;">
                            Rp ({{ thousandFormat($transaction->voucher_discount) }})
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" style="padding: 0; text-align: left;">Admin Fee</td>
                        <td colspan="3" style="padding: 0; text-align: right;">
                            {{ thousandFormat($transaction->admin_fee, 'Rp') }}
                        </td>
                    </tr>

                    <tr>
                        <td colspan="5" style="padding: 2px 0;">
                            <hr style="border-top: 1px solid #c1c1c1; border-bottom: none; border-left: none; border-right: none;">
                        </td>
                    </tr>

                    <tr>
                        <th colspan="2" style="padding: 0; text-align: left;">Total Payment</th>
                        <th colspan="3" style="padding: 0; text-align: right; color: #FE0707;">
                            {{ thousandFormat($transaction->total_amount, 'Rp') }}
                        </th>
                    </tr>
                </tbody>
            </table>
        </div>
        <!-- End::Payment Summary -->
    </div>
@endsection
