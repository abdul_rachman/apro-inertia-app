@extends('mails.company._layout')

@section('content')
    <div style="min-height: 150px;">
        <b>Congratulation {{ $user->company->name }}</b>

        <div style="margin-top: 10px;">
            Your document has successfully verrified.
        </div>
        <div style="margin-top: 5px">
            Now you can proceed to next step.
        </div>

        <div style="margin-top: 30px;">
            Regards,
        </div>
        <div>
            {{ config('app.name') }}
        </div>
    </div>
@endsection
