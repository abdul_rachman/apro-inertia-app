<?php

namespace Tests\Feature;

use Inertia\Testing\AssertableInertia;
use Tests\TestCase;

class RegisterTest extends TestCase
{

    /**
     * test for register company when required fields fail validation
     *
     * @return void
     */
    public function test_register_company_when_required_fields_fail_validation()
    {
        $this->get('/register');

        $this->followingRedirects()
            ->post(route("register"))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("Auth/Register")
                    ->where("errors.name", "The name field is required.")
                    ->where("errors.email", "The email field is required.")
                    ->where("errors.address", "The address field is required.")
                    ->where("errors.phone", "The phone field is required.")
                    ->where("errors.wa", "The Whatsapp field is required.")
                    ->where("errors.tax_id_number", "The tax id number field is required.")
                    ->where("errors.password", "The password field is required.")
            );
    }

    /**
     * test for register company when success
     *
     * @return void
     */
    public function test_register_company_when_success()
    {
        $this->get("/register");

        $this->followingRedirects()
            ->post(route("register", [
                "address" => "yogyakarta",
                "email" => "company99@mailinator.com",
                "name" => "Company Test",
                "password" => "12345678",
                "password_confirmation" => "12345678",
                "phone" => "12345678",
                "tax_id_number" => "1234567890123456",
                "wa" => "081234567891",
            ]))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("Auth/Login")
            );    
    }

    /**
     * test for register company when tax id less than 16
     *
     * @return void
     */
    public function test_register_company_when_tax_id_less_than_16()
    {
        $this->get("/register");

        $this->followingRedirects()
            ->post(route("register", [
                "address" => "yogyakarta",
                "email" => "company99@mailinator.com",
                "name" => "Company Test",
                "password" => "12345678",
                "password_confirmation" => "12345678",
                "phone" => "12345678",
                "tax_id_number" => "123456789012345",
                "wa" => "081234567891",
            ]))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("Auth/Register")
                    ->where("errors.tax_id_number", "The tax id number must be 16 digits.")
            );
    }
}
