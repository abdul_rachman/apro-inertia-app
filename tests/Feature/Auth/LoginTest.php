<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Inertia\Testing\AssertableInertia;
use Tests\TestCase;

class LoginTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();
        $this->artisan("migrate:fresh");
        $this->artisan("db:seed");
    }
    /**
     * test for super admin when required fields fail validation
     *
     * @return void
     */
    public function test_super_admin_when_required_fields_fail_validation()
    {
        $this->get('/admin');

        $this->followingRedirects()
            ->post(route("admin.login.store"))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("Auth/AdminLogin")
                    ->where("errors.email", "The email field is required.")
                    ->where("errors.password", "The password field is required.")
            );
    }

    /**
     * test for super admin login where required fields fail validation
     *
     * @return void
     */
    public function test_super_admin_login_and_redirect_to_dashboard_page()
    {
        $this->get("/admin");

        $this->followingRedirects()
            ->post(route("admin.login.store", [
                "email" => "admin@mail.com",
                "password" => "123456"
            ]))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("Admin/Dashboard")
                    ->where("errors", [])
                    ->where("auth.user", auth()->user())
            );
    }

    /**
     * test for adminkap login where required fields fail validation
     *
     * @return void
     */
    public function test_adminkap_login_where_required_fields_fail_validation()
    {
        $this->get("/company/login");

        $this->followingRedirects()
            ->post(route("login"))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("Auth/CompanyLogin")
                    ->where("errors.companyID", "The company i d field is required.")
                    ->where("errors.email", "The email field is required.")
                    ->where("errors.password", "The password field is required.")
            );
    }

    /**
     * test for adminkap login where success and redirect dashboard
     *
     * @return void
     */
    public function test_adminkap_login_where_success_and_redirect_dashboard()
    {
        $this->get("/company/login");

        $this->followingRedirects()
            ->post(route("login", [
                "companyID" => "222333-01",
                "email" => "company1@mail.com",
                "password" => "123456"
            ]))
            ->assertOk()
            ->assertInertia(
                fn (AssertableInertia $page) => $page
                    ->component("AdminKAP/Dashboard")
                    ->where("errors", [])
                    ->where("auth.user", auth()->user())
            );
    }
}
