<?php

return [
    // Default Admin & Company Setting Values

    // Materiality
	'profit_based_entities' => [
        'profit_before_tax' => [
            'min' => '3',
            'max' => '10'
        ],
        'revenue' => [
            'min' => '0.5',
            'max' => '2'
        ]
    ],
    'asset_based_or_investment_entities' => [
        'total_assets' => [
            'min' => '1',
            'max' => '2'
        ],
        'net_assets' => [
            'min' => '2',
            'max' => '5'
        ]
    ],
    'not_for_profit_entities' => [
        'gross_income' => [
            'min' => '0.5',
            'max' => '1'
        ],
        'expenditure' => [
            'min' => '0.5',
            'max' => '1'
        ]
    ],

    // Risk Material Misstatement Level
    'risk_material_misstatement_level' => [
        'low'      => '1.4',
    	'moderate' => '2.3',
    	'high'     => '4.6',
    ],

	// Control Risk
	'control_risk_low' => '1',
	'control_risk_moderate' => '2',
	'control_risk_high' => '3',
	'control_design_yes' => '1',
	'control_design_partial' => '2',
	'control_design_no' => '3',
	'control_implementation_yes' => '1',
	'control_implementation_partial' => '2',
	'control_implementation_no' => '3',

	// Confidence Level
	'confidence_level' => [
		[
			'key'	=> 'very_low_level_1',
			'value'	=> [
				'level'  => 50,
				'factor' => 0.7
			]
		],
		[
			'key'	=> 'very_low_level_2',
			'value'	=> [
				'level'  => 55,
				'factor' => 0.8
			]
		],
		[
			'key'	=> 'very_low_level_3',
			'value'	=> [
				'level'  => 60,
				'factor' => 0.9
			]
		],
		[
			'key'	=> 'low_level_1',
			'value'	=> [
				'level'  => 65,
				'factor' => 1.1
			]
		],
		[
			'key'	=> 'low_level_2',
			'value'	=> [
				'level'  => 70,
				'factor' => 1.2
			]
		],
		[
			'key'	=> 'low_level_3',
			'value'	=> [
				'level'  => 75,
				'factor' => 1.4
			]
		],
		[
			'key'	=> 'moderate_level_1',
			'value'	=> [
				'level'  => 80,
				'factor' => 1.6
			]
		],
		[
			'key'	=> 'moderate_level_2',
			'value'	=> [
				'level'  => 85,
				'factor' => 1.9
			]
		],
		[
			'key'	=> 'moderate_level_3',
			'value'	=> [
				'level'  => 90,
				'factor' => 2.3
			]
		],
		[
			'key'	=> 'high_level_1',
			'value'	=> [
				'level'  => 95,
				'factor' => 3.0
			]
		],
		[
			'key'	=> 'high_level_2',
			'value'	=> [
				'level'  => 98,
				'factor' => 3.7
			]
		],
		[
			'key'	=> 'high_level_3',
			'value'	=> [
				'level'  => 99,
				'factor' => 4.6
			]
		],
	],

	// Risk of Incorrect Acceptance (RIA)
	'risk_of_incorrect_acceptance' => [
		[
			'risk' 	 => '1', 
			'factor' => 1.9,
			'bg1' 	 => 'bg-[#07FB0203]',
			'bg2' 	 => 'bg-[#ff5050cc]' 
		],
		[
			'risk' 	 => '5', 
			'factor' => 1.6,
			'bg1'  	 => 'bg-[#07FB020D]',
			'bg2' 	 => 'bg-[#ff5050b2]' 
		],
		[
			'risk' 	 => '10',
			'factor' => 1.5,
			'bg1'  	 => 'bg-[#07FB021A]',
			'bg2' 	 => 'bg-[#ff50509a]' 
		],
		[
			'risk' 	 => '15',
			'factor' => 1.4,
			'bg1'  	 => 'bg-[#07FB0226]',
			'bg2' 	 => 'bg-[#ff505080]' 
		],
		[
			'risk' 	 => '20',
			'factor' => 1.3,
			'bg1'  	 => 'bg-[#07FB0233]',
			'bg2' 	 => 'bg-[#ff505065]' 
		],
		[
			'risk' 	 => '25',
			'factor' => 1.25,
			'bg1'  	 => 'bg-[#07FB0240]',
			'bg2' 	 => 'bg-[#ff50504d]' 
		],
		[
			'risk' 	 => '30',
			'factor' => 1.2,
			'bg1'  	 => 'bg-[#07FB024D]',
			'bg2' 	 => 'bg-[#FF505033]' 
		],
		[
			'risk' 	 => '37',
			'factor' => 1.15,
			'bg1'  	 => 'bg-[#07FB0259]',
			'bg2' 	 => 'bg-[#ff50501a]' 
		],
		[
			'risk' 	 => '50',
			'factor' => 1.1,
			'bg1'  	 => 'bg-[#07FB025E]',
			'bg2' 	 => 'bg-[#FF50500D]' 
		],
	],

	// User Access Control
	'user_access_auditor' => [
		'can_assigned_as_an_admin' => '1',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'Admin Only',
	],
	'user_access_manager' => [
		'can_assigned_as_an_admin' => '1',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'Admin Only',
	],
	'user_access_partner' => [
		'can_assigned_as_an_admin' => '1',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'Admin Only',
	],
	'user_access_eqcr_partner' => [
		'can_assigned_as_an_admin' => '0',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'N/A',
	],
	'user_access_managing_partner' => [
		'can_assigned_as_an_admin' => '0',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'N/A',
	],
	'user_access_risk_management_partner' => [
		'can_assigned_as_an_admin' => '0',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'N/A',
	],
	'user_access_auditor_expert' => [
		'can_assigned_as_an_admin' => '0',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'N/A',
	],
	'user_access_auditor_expert' => [
		'can_assigned_as_an_admin' => '0',
		'create' => '1',
		'read' 	 => '1',
		'update' => '1',
		'delete' => 'N/A',
	],
	'user_access_guess' => [
		'can_assigned_as_an_admin' => '0',
		'create' => 'N/A',
		'read' 	 => '1',
		'update' => 'N/A',
		'delete' => 'N/A',
	],

	// Legal & Regulatory
	'certification' => [
		'registration_number'      => 'DID2022044339',
		'registration_date'        => 'June 23, 2022',
		'date_of_protection_start' => 'June 24, 2022',
		'app_name'                 => 'APRO',
		'registered_by'            => 'PT Karsa Media Digital (KMD)',
		'domain_name'              => 'https://www.apro.co.id'
	],

	'legal_notices' => '<p><strong>Legal Notices:</strong></p>
<p><strong>Copyright &copy; 2022 APRO. All rights reserved.</strong><br><strong>APRO is a trademark of PT Karsasoft Bara Technology (&ldquo;Karsasoft&rdquo;)., registered in the Republic of Indonesia.</strong></p>
<p><strong>KARSASOFT is a trademark or registered trademark of PT Karsasoft Bara Technology in the Republic of Indonesia is used under license.</strong></p>
<p><strong>APRO or APRO Software is developed by KARSASOFT by license agreement with PT Karsa Media Digital (&ldquo;KMD&rdquo;) as a product owner and content creator of APRO Software.</strong></p>
<p>For APRO and third party software license agreements presented at activation or included with the product and presented with any software updates, see <a href="https://www.apro.co.id/legal/third-party-license" aria-invalid="true">www.apro.co.id/legal/third-party-license</a>.</p>
<p>Certain contents or materials in APRO Software is under permission by The Indonesian Institute of Certified Public Accountants (IAPI). The content provides non-authoritative guidance on applying International Standards on Auditing (ISAs) issued by the International Auditing and Assurance Standards Board (IAASB).</p>
<p><strong>Disclaimer</strong></p>
<p>The forms or applications in APRO are designed to assist practitioners in the implementation of the International Standards on Auditing (ISAs). The forms or applications are not intended to be a substitute for the ISAs themselves.</p>
<p>A Practitioner should utilise these forms or applications in light of his or her professional judgment and the facts and circumstances involved in each particular audit.</p>
<p>APRO, KMD, KARSASOFT and IAPI disclaim any responsibility or liability that may occur, directly or indirectly, as a consequence of the use and application of these APRO Software.</p>
<p><strong>Copyright</strong></p>
<p>The APRO templates or applications are copyrighted by KMD and KARSASOFT. Use of this templates or applications are subject to KMD and KARSASOFT\'s end user license agreement. All intellectual property rights in the software and content remain the exclusive property of APRO, as the case may be. When using this template or application, you must comply with the law, including without limitation, copyright laws.</p>',

	'license' => '<p><strong>ENGLISH</strong></p>
<p><strong>IMPORTANT: BY USING APRO (&ldquo;SOFTWARE&rdquo;), YOU ARE AGREEING TO BE BOUND BY THE FOLLOWING TERMS:</strong></p>
<p><strong>A. APRO SOFTWARE LICENSE AGREEMENT </strong><br><strong>B. NOTICES FROM PT KARSASOFT BARA TECHNOLOGY (KARSASOFT) </strong></p>
<p><strong>PLEASE READ THIS SOFTWARE LICENSE AGREEMENT (&ldquo;LICENSE&rdquo;) CAREFULLY BEFORE USING APRO OR DOWNLOADING THE SOFTWARE UPDATE ACCOMPANYING THIS LICENSE. BY USING APRO OR DOWNLOADING A SOFTWARE UPDATE, AS APPLICABLE, YOU ARE AGREEING TO BE BOUND BY THE TERMS OF THIS LICENSE. IF YOU DO NOT AGREE TO THE TERMS OF THIS LICENSE, DO NOT USE THE APRO OR DOWNLOAD THE SOFTWARE UPDATE.</strong></p>
<p><strong>IF YOU HAVE RECENTLY SUBSCRIBES THE APRO AND YOU DO NOT AGREE TO THE TERMS OF THE LICENSE, YOU MAY ASK TO REFUND THE SUBSCRIBTIONS FEE WITHIN THE RETURN PERIOD TO THE PT KARSA MEDIA DIGITAL (&ldquo;KMD&rdquo;) AS A REPRESENTATIVE COMPANY OR MANAGING COMPANY OF APRO. WHERE YOU OBTAINED IT FOR A REFUND, SUBJECT TO APRO&rsquo;S RETURN POLICY FOUND AT <a href="https://www.apro.co.id/legal/sales-support/" aria-invalid="true">https://www.apro.co.id/legal/sales-support/</a>.</strong></p>
<p><strong>1. General</strong></p>
<p>(a) The software (including programming code, embedded software and third party software), documentation, interfaces, content, fonts and any data that came with APRO (&ldquo;Original APRO Software&rdquo;), as may be updated or replaced by feature enhancements, software updates or system restore software provided by APRO (&ldquo;APRO Software Updates&rdquo;), whether in read only memory, on any other media or in any other form (the Original APRO Software and APRO Software Updates are collectively referred to as the &ldquo;APRO Software&rdquo;) are licensed, not sold, to you by Karsasoft (&ldquo;APRO&rdquo;) for use only under the terms of this License. APRO and its licensors retain ownership of the APRO Software itself and reserve all rights not expressly granted to you. You agree that the terms of this License will apply to any APRO-branded app that may be built-in on your Device, unless such app is accompanied by a separate license, in which case you agree that the terms of that license will govern your use of that app.</p>
<p>(b) APRO, at its discretion, may make available future APRO Software Updates. The APRO Software Updates, if any, may not necessarily include all existing software features or new features that APRO releases for newer or other models. The terms of this License will govern any APRO Software Updates provided by APRO, unless such APRO Software Update is accompanied by a separate license, in which case you agree that the terms of that license will govern.</p>
<p>(c) If you use the express setup feature to set up a new Device based on your existing Device, you agree that the terms of this License will govern your use of the APRO Software on your new Device, unless it is accompanied by a separate license, in which case you agree that the terms of that license will govern your use of that APRO Software. Your Device will periodically check with APRO for APRO Software Updates. If an update is available, the update may automatically download and install onto your Device and, if applicable, your peripheral devices. By using the APRO Software, you agree that APRO may download and install automatic APRO Software Updates onto your Device and your peripheral devices. You can turn off automatic updates altogether at any time by changing the Automatic Updates settings found within Settings &gt; General &gt; Software Update.</p>
<p><strong>2. Permitted License Uses and Restrictions.</strong></p>
<p>(a) Subject to the terms and conditions of this License, you are granted a limited non-exclusive license to use the APRO Software. This License does not grant you any rights to use APRO proprietary interfaces and other intellectual property in the design, development, manufacture, licensing or distribution of third party devices and accessories, or third party software applications, for use with APRO Software. </p>
<p>(b) You may not, and you agree not to or enable others to, copy (except as expressly permitted by this License), decompile, reverse engineer, disassemble, attempt to derive the source code of, decrypt, modify, or create derivative works of the APRO Software or any services provided by the APRO Software or any part thereof (except as and only to the extent any foregoing restriction is prohibited by applicable law or by licensing terms governing use of open-source components that may be included with the APRO Software). You agree not to remove, obscure, or alter any proprietary notices (including trademark and copyright notices) that may be affixed to or contained within the APRO Software.</p>
<p>(c) The APRO Software may be used to reproduce materials so long as such use is limited to reproduction of non-copyrighted materials, materials in which you own the copyright, or materials you are authorized or legally permitted to reproduce. Notwithstanding the foregoing, you are prohibited from republishing, retransmitting or reproducing any images accessed through News or Maps as a stand-alone file. Title and intellectual property rights in and to any content displayed by, stored on or accessed through your Device belong to the respective content owner. Such content may be protected by copyright or other intellectual property laws and treaties, and may be subject to terms of use of the third party providing such content. Except as otherwise provided herein, this License does not grant you any rights to use such content nor does it guarantee that such content will continue to be available to you.</p>
<p>(d) In order to complete certain app and/or website action shortcuts, the APRO Software may need to access certain third party software applications, services or websites on your Device. You expressly consent to such use to the extent necessary to complete the Shortcut with the APRO Software.</p>
<p>(e) You agree to use the APRO Software and the Services (as defined in Section 4 below) in compliance with all applicable laws, including local laws of the country or region in which you reside or in which you download or use the APRO Software and Services. Features of the APRO Software and the Services may not be available in all languages or regions, some features may vary by region, and some may be restricted or unavailable from your service provider. A Wi-Fi or cellular data connection is required for some features of the APRO Software and Services.</p>
<p>(f) Use of the APRO requires a unique user ID and password combination, known as an Company ID. An Company ID is also required to access app updates and certain features of the APRO Software and Services.</p>
<p>(g) If you choose to allow automatic app updates, your Device will periodically check with APRO for updates to the apps on your Device and, if one is available, the update will automatically download and install onto your Device. You can turn off the automatic app updates at any time by going to Settings, general, and under Automatic Update, turn off Updates.</p>
<p>(h) Using your Device in some circumstances can distract you and may cause a dangerous situation (for example, avoid typing a text message while driving a car or using your device while riding a bicycle). By using your Device you agree that you are responsible for observing rules that prohibit or restrict the use your device.</p>
<p><strong>3. Transfer.</strong></p>
<p>You may not rent, lease, lend, sell, redistribute, or sublicense the APRO Software. You prohibited to retain any copies of the APRO Software, full or partial, including copies stored on a computer or other storage device.</p>
<p><strong>4. Services and Third Party Materials.</strong></p>
<p>(a) Certain Services may display, include or make available content, data, information, applications or materials from third parties (&ldquo;Third Party Materials&rdquo;) or provide links to certain third party web sites. By using the Services, you acknowledge and agree that APRO is not responsible for examining or evaluating the content, accuracy, completeness, timeliness, validity, copyright compliance, legality, decency, quality or any other aspect of such Third Party Materials or web sites. APRO, its officers, affiliates and subsidiaries do not warrant or endorse and do not assume and will not have any liability or responsibility to you or any other person for any third-party Services, Third Party Materials or web sites, or for any other materials, products, or services of third parties. Third Party Materials and links to other web sites are provided solely as a convenience to you.</p>
<p>(b) Such Services and Third Party Materials may not be available in all languages or in all countries or regions. APRO makes no representation that such Services and Third Party Materials are appropriate or available for use in any particular location. To the extent you choose to use or access such Services or Third Party Materials, you do so at your own initiative and are responsible for compliance with any applicable laws, including but not limited to applicable local laws and privacy and data collection laws.</p>
<p><strong>5. Termination.</strong></p>
<p>This License is effective until terminated. Your rights under this License will terminate automatically or otherwise cease to be effective without notice from APRO if you fail to comply with any term(s) of this License. Upon the termination of this License, you shall cease all use of the APRO Software. Sections 3, 4, 5, 6, 7, 8, and 9 of this License shall survive any such termination.</p>
<p><strong>6. Disclaimer of Warranties</strong></p>
<p>6.1 If you are a customer who is a consumer (someone who uses the APRO Software outside of your trade, business or profession), you may have legal rights in your country of residence which would prohibit the following limitations from applying to you, and where prohibited they will not apply to you. To find out more about rights, you should contact a local consumer advice organization.</p>
<p>6.2 YOU EXPRESSLY ACKNOWLEDGE AND AGREE THAT, TO THE EXTENT PERMITTED BY APPLICABLE LAW, USE OF THE APRO SOFTWARE AND ANY SERVICES PERFORMED BY OR ACCESSED THROUGH THE APRO SOFTWARE IS AT YOUR SOLE RISK AND THAT THE ENTIRE RISK AS TO SATISFACTORY QUALITY, PERFORMANCE, ACCURACY AND EFFORT IS WITH YOU.</p>
<p>6.3 TO THE MAXIMUM EXTENT PERMITTED BY APPLICABLE LAW, THE APRO SOFTWARE AND SERVICES ARE PROVIDED &ldquo;AS IS&rdquo; AND &ldquo;AS AVAILABLE&rdquo;, WITH ALL FAULTS AND WITHOUT WARRANTY OF ANY KIND, AND APRO AND APRO&rsquo;S LICENSORS (COLLECTIVELY REFERRED TO AS &ldquo;APRO&rdquo; FOR THE PURPOSES OF SECTIONS 7 AND 8) HEREBY DISCLAIM ALL WARRANTIES AND CONDITIONS WITH RESPECT TO THE APRO SOFTWARE AND SERVICES, EITHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES AND/OR CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY, FITNESS FOR A PARTICULAR PURPOSE, ACCURACY, QUIET ENJOYMENT, AND NON-INFRINGEMENT OF THIRD PARTY RIGHTS.</p>
<p>6.4 APRO DOES NOT WARRANT AGAINST INTERFERENCE WITH YOUR ENJOYMENT OF THE APRO SOFTWARE AND SERVICES, THAT THE FUNCTIONS CONTAINED IN, OR SERVICES PERFORMED OR PROVIDED BY, THE APRO SOFTWARE WILL MEET YOUR REQUIREMENTS, THAT THE OPERATION OF THE APRO SOFTWARE AND SERVICES WILL BE UNINTERRUPTED OR ERROR-FREE, THAT ANY SERVICES WILL CONTINUE TO BE MADE AVAILABLE, THAT DEFECTS IN THE APRO SOFTWARE OR SERVICES WILL BE CORRECTED, OR THAT THE APRO SOFTWARE WILL BE COMPATIBLE OR WORK WITH ANY THIRD PARTY SOFTWARE, APPLICATIONS OR THIRD PARTY SERVICES. </p>
<p>6.5 YOU FURTHER ACKNOWLEDGE THAT THE APRO SOFTWARE AND SERVICES ARE NOT INTENDED OR SUITABLE FOR USE IN SITUATIONS OR ENVIRONMENTS WHERE THE FAILURE OR TIME DELAYS OF, OR ERRORS OR INACCURACIES IN THE CONTENT, DATA OR INFORMATION PROVIDED BY THE APRO SOFTWARE OR SERVICES COULD LEAD TO DEATH, PERSONAL INJURY, OR SEVERE PHYSICAL OR ENVIRONMENTAL DAMAGE, INCLUDING WITHOUT LIMITATION THE OPERATION OF NUCLEAR FACILITIES, AIRCRAFT NAVIGATION OR COMMUNICATION SYSTEMS, AIR TRAFFIC CONTROL, LIFE SUPPORT OR WEAPONS SYSTEMS.</p>
<p>6.6 NO ORAL OR WRITTEN INFORMATION OR ADVICE GIVEN BY APRO OR AN APRO AUTHORIZED REPRESENTATIVE SHALL CREATE A WARRANTY. </p>
<p><strong>7. Limitation of Liability.</strong></p>
<p>TO THE EXTENT NOT PROHIBITED BY APPLICABLE LAW, IN NO EVENT SHALL APRO, ITS AFFILIATES, AGENTS OR PRINCIPALS BE LIABLE FOR PERSONAL INJURY, OR ANY INCIDENTAL, SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES WHATSOEVER, INCLUDING, WITHOUT LIMITATION, DAMAGES FOR LOSS OF PROFITS, CORRUPTION OR LOSS OF DATA, FAILURE TO TRANSMIT OR RECEIVE ANY DATA (INCLUDING WITHOUT LIMITATION COURSE INSTRUCTIONS, ASSIGNMENTS AND MATERIALS), BUSINESS INTERRUPTION OR ANY OTHER COMMERCIAL DAMAGES OR LOSSES, ARISING OUT OF OR RELATED TO YOUR USE OR INABILITY TO USE THE APRO SOFTWARE AND SERVICES OR ANY THIRD PARTY SOFTWARE, APPLICATIONS OR SERVICES IN CONJUNCTION WITH THE APRO SOFTWARE OR SERVICES, HOWEVER CAUSED, REGARDLESS OF THE THEORY OF LIABILITY (CONTRACT, TORT OR OTHERWISE) AND EVEN IF APRO HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.</p>
<p><strong>8. Controlling Law and Severability.</strong> </p>
<p>This License will be governed by and construed in accordance with the laws of the Republic of Indonesia, excluding its conflict of law principles. If for any reason a court of competent jurisdiction finds any provision, or portion thereof, to be unenforceable, the remainder of this License shall continue in full force and effect.</p>
<p><strong>9. Complete Agreement; Governing Language.</strong></p>
<p>This License constitutes the entire agreement between you and APRO relating to the APRO Software and supersedes all prior or contemporaneous understandings regarding such subject matter. No amendment to or modification of this License will be binding unless in writing and signed by APRO. Any translation of this License is done for local requirements and in the event of a dispute between the English and any non-English versions, the English version of this License shall govern, to the extent not prohibited by local law in your jurisdiction.</p>
<p>APRO2022001 <br>9/25/2022 </p>
<p>-------------------------------------------------------------------------------------------------------------------</p>
<p><strong>NOTICES FROM PT KARSASOFT BARA TECHNOLOGY (KARSASOFT)</strong></p>
<p>If Karsasoft needs to contact you about your product or account, you consent to receive the notices by email. You agree that any such notices that we send you electronically will satisfy any legal communication requirements.</p>',

	'privacy_policy' => '<p><strong>Copyright &copy; 2022 APRO. All rights reserved.&nbsp;</strong></p>
<p></p>
<p><strong>APRO is a trademark of PT Karsasoft Bara Technology (&ldquo;Karsasoft&rdquo;)., registered in the Republic of Indonesia.</strong></p>
<p></p>
<p><strong>APRO or APRO Software is developed by KARSASOFT by license agreement with PT Karsa Media Digital (&ldquo;KMD&rdquo;) as a product owner of APRO Software.</strong></p>
<p></p>
<p><strong>The following is our privacy policy regarding uses of APRO software</strong></p>
<p></p>
<p><strong>Committed to Protecting Your Information</strong></p>
<p></p>
<p>KARSASOFT and KMD or called as APRO is committed to protecting the privacy of all individuals. To deliver our commitment to privacy, we will comply with legislations and regulations of the countries in which we operate.</p>
<p></p>
<p>We actively communicate this privacy statement to all employees and stakeholders to ensure awareness of their requirement to comply with our privacy standards and to anyone else who asks for it</p>
<p></p>
<p>We ensure that lawful, fair and unobtrusive means are used to only collect personal information that is necessary for our business purposes.</p>
<p></p>
<p>We collect sensitive information only with individuals&rsquo; consent, and do not disclose any personal information to unrelated third parties, except with individuals&rsquo; consent or where we are required to do so by law.</p>
<p></p>
<p>We take reasonable steps to destroy or de-identify personal information in a secure manner when we have no further need for it or are required to do so by law.</p>
<p></p>
<p>We ensure only authorized personnel who have agreed to keep personal information confidential have access to the information.</p>
<p></p>
<p>We take reasonable steps to keep personal information up to date, accurate and complete.</p>
<p></p>
<p>In most cases we provide individuals access to their personal information on request.</p>
<p></p>
<p>Periodically we review and revise this Privacy Statement and related procedures to maintain their relevance.</p>
<p></p>
<p>APRO recognizes and respects the importance of each individual&rsquo;s privacy. This statement and related procedures ensure all personal information held by APRO is handled appropriately and kept confidential.</p>
<p></p>
<p><strong>What do we use personal information for?</strong></p>
<p></p>
<p>Any personal details you provide will be used for the purpose of dealing with your request, enquiry or application for employment. This may involve disclosing your personal information to companies within the APRO, or to service providers who assist us in operating our business. These organizations are required to comply with the principles of this statement.</p>
<p></p>
<p>If your request is for company newsletters or other material, you can direct your e-mail to our Customer Relations at [info@apro.co.id]. Your details and information about the newsletters, or material you wish to receive will be held on the client database and used for the purpose of administering your subscription or request. We may use your e-mail details to contact you from time to time to provide you with information about APRO.</p>
<p></p>
<p>With regard to all other enquiries, your details will only be accessible and used by personnel dealing directly with your request or for marketing purposes about APRO software or other related product inconnection with your subsciption.</p>
<p></p>
<p><strong>Disclosing Personal Information</strong></p>
<p></p>
<p>As noted above, we may share personal information with related companies or with contractors performing services for APRO, which are required to comply with this statement. APRO will only make such disclosures to achieve the purpose for which the information was provided. Otherwise, APRO&rsquo;s policy is not to disclose any personal information to any unrelated third party, except with your consent, or where we are required by law to do so.</p>
<p>Retention and Disposal of Information</p>
<p>APRO will only keep personal information it needs for the purpose for which it was collected. APRO will dispose of such information when it has no further need to use it, or it is required by law to do so.</p>
<p></p>
<p><strong>APRO Web Site</strong></p>
<p></p>
<p>This Statement applies to all of our web pages at www.apro.co.id (the site). It explains our practices on collection and processing of any personal information that may be collected as a result of your use of the site (i.e. information from which an individual&rsquo;s identity is apparent or can be reasonably ascertained).</p>
<p></p>
<p>This site contains forms that allow you to request information about APRO, subsciption of APRO software or make other enquiries. By submitting a form, you provide us with certain personal information which may include your name, e-mail address and any other details you include in the form. This information is collected by us for the purpose of dealing with your request. We may not be able to deal with your request without this information.</p>
<p></p>
<p>We do not collect any other personal information via the site. We do not use web tracking or other devices to store your personal information.</p>
<p></p>
<p><strong>E-mail Security</strong></p>
<p></p>
<p>In line with our policy on e-mail security, any e-mails you send will be automatically scanned, which could result in certain attachments and styles of message being automatically blocked. Our IT administrators may have access to your e-mails to authorize the content. This is done purely for IT security purposes and our IT administrators will not access the contents of your e-mails once this process is complete.</p>
<p></p>
<p><strong>Security of Information</strong></p>
<p></p>
<p>You should be aware that the internet is not a secure environment. However, APRO uses all reasonable efforts to ensure that any personal information collected is held securely. In addition, only authorised personnel have access to personal information. Such personnel have agreed to ensure confidentiality of this information and are required to comply with this policy.</p>
<p></p>
<p></p>
<p>Last Modified: October 1, 2022</p>',

	'term_and_conditions' => "<p>The following describes the terms and conditions (the &ldquo;Terms and Conditions&rdquo;) upon which PT Karsa Media Digital (&ldquo;KMD&rdquo;) (the &ldquo;Company&rdquo; or &ldquo;APRO&rdquo; or &ldquo;We&rdquo;) offers access to the Internet site found at www.apro.co.id alongside its specific public IP and any related sub-domains and/or applications thereof (respectively, the &ldquo;Site&rdquo; or &ldquo;App&rdquo;, and together the &ldquo;Services&rdquo;) to you the customer, irrespective of whether or not you are an Account (as defined below) holder (&ldquo;you&rdquo; or &ldquo;You&rdquo;).</p>
<p></p>
<p>PLEASE READ THE PRIVACY POLICY, COOKIE POLICY, RISK WARNING AND ALL OF THE FOLLOWING TERMS AND CONDITIONS INCLUDING THE SPECIAL CONDITIONS BEFORE USING OUR SERVICES. BY CONTINUING TO ACCESS OR USE OUR SERVICES, YOU SIGNIFY YOUR ACCEPTANCE OF THESE TERMS AND CONDITIONS. We reserve the right to amend, remove, or add to these Terms and Conditions at any time. Please check the &ldquo;Last modified&rdquo; at the bottom of this document to see when the Terms and Conditions were last updated. Any changes to the Terms and Conditions will become effective when we post the revised Terms and Conditions. Your use of the Services, or your provision of personal information following any changes means that you accept the updated Terms and Conditions.</p>
<p></p>
<p>If, at any time, you do not wish to accept the Terms and Conditions, you may not use our Services. Any terms and conditions proposed by you which are in addition to, or which conflict with these Terms and Conditions are expressly rejected by the Company and will have no force or effect.</p>
<p></p>
<p><strong>Your Account</strong></p>
<p></p>
<p>As part of the process necessary to set up an account on the Services (an &ldquo; Account &rdquo;) and obtain access to certain parts of the Services, you will be required to either provide your full name, email address, password, and phone number (the &ldquo; Registration Credentials &rdquo;) and legal or licence document and other related document as required. You must ensure that your Registration Credentials are accurate, truthful and updated. We reserve the right to block the creation of your Account based on our inability to confirm the authenticity of your Registration Credentials.</p>
<p></p>
<p>We permit you to maintain only one Account to access the Services at any time and you hereby represent that you currently have no other Account(s).</p>
<p></p>
<p>You may terminate your Account at any time by email notice to us trough our representatif email: info@apro.co.id. Upon termination, you will receive a confirmation via e-mail that the request was received, and your Account will be terminated immediately.</p>
<p></p>
<p><strong>Subscription-Based Services</strong></p>
<p></p>
<p>We offer services, products, and subscriptions for a subscription fee (respectively, &ldquo;Subscription Based Services&rdquo; and &ldquo;Subscription Fee&rdquo;).</p>
<p></p>
<p>These Subscription Based Services are governed by any additional terms you agree to when you register for the Subscription Based Services (including any third party&rsquo;s terms of service, privacy policy and other relevant policies providing any such Subscription Based Service, which you should read thoroughly before agreeing to them) and these Terms. If you register for Subscription Based Services, you agree to pay the applicable Subscription Fees set forth on the Site and/or App and must designate a payment method and provide us with accurate billing and payment information, and you have the continuing obligation to keep it up to date. If the Subscription Based Services includes a third-party product or service, you understand and agree that your purchase and use of the Service is also subject to the third party&rsquo;s terms of service and privacy policy, which you should read thoroughly before agreeing to them. APRO assume no liability or responsibility on such products or services when it is provided by a third party.</p>
<p></p>
<p><strong>Payment.</strong> You represent that you are at least the minimum age required to enter into a legal agreement. You agree to pay us for any Subscription Based Services you purchase from us (or a third party), as well as all other charges incurred under your account, including applicable taxes and fees. You are responsible for all charges incurred under your account, including purchases made by you or anyone you allow to use your account or any sub-or linked accounts (including any person with implied, actual, or apparent authority) or anyone who gains access to your account as a result of your failure to safeguard your authentication credentials. You authorize and direct us to charge your designated payment method for these charges or, if it fails, to charge any other payment method you have on file with us, even if we received it in association with other Subscription Based Services. You are responsible for all charges even if your payment method fails or is denied. You authorize and direct us to retain all information about any payment method(s) associated with your account. We may import payment information you entered during a prior purchase and provide you the option to use that payment information during purchase of a new product. You permit us to obtain and use updated information from the issuer of your payment method in accordance with the policies and procedures of any applicable card brands. We may in some instances continue charging a payment method past its expiration date at our discretion and subject to the payment processors' or issuing bank's approval. Surcharges may apply if you use certain payment methods, such as payment from your checking or savings account. We may charge for Subscription Based Services in advance and on a daily, monthly, yearly, lump sum, or other basis in accordance with the stated terms, as long as your subscription remains active, even if you have not downloaded or used such Service or accessed your online account. We may (and you authorize us to) take steps to verify the validity of the credit card information you provide to us. You authorize us to do so for verification and anti-fraud purposes.</p>
<p></p>
<p><strong>Renewal.</strong> We use auto-renewal for many of our Subscription Based Services. At the expiration of each subscription term for such Subscription Based Services, we will automatically renew your subscription and charge the credit card or other payment method you have provided to us, unless you cancel your subscription at least 48 hours before the end of the current subscription period. Unless otherwise stated herein, your subscription will be automatically renewed at the then-current price, excluding promotional and discount pricing. We may, in our sole discretion, post charges to your payment method individually or aggregate charges for some or all of your Subscription Based Services with us.</p>
<p></p>
<p><strong>Free Trials.</strong> We may offer you free trials, so that you may try a Subscription Based Services subscription without charge or obligation (&ldquo;Free Trial&rdquo;). Unless otherwise stated and unless you cancel your subscription prior to the expiration of the Free Trial, periodic subscription fees will be charged at the then-applicable rate upon expiration of the Free Trial period and will continue to be charged until the subscription is canceled or expired. If you are not satisfied with a particular Subscription Based Services, you must cancel the subscription before the Free Trial ends to avoid charges. We reserve the right to limit you to one free trial or promotion of a Subscription Based Services and to prohibit the combining of free trials or other promotional offers.</p>
<p></p>
<p><strong>No Refunds.</strong> Notwithstanding anything to the contrary set forth herein (but subject to any applicable law), all charges for a Subscription Based Services are nonrefundable unless provided otherwise in the terms you agree to when you register for a Subscription Based Services. However, if you believe that you have been charged in error or if you believe you should be refunded for any other reason, you should contact us within 60 days of such charge. We reserve the right to refuse such a refund request for any reason, including if we reasonably believe (i) that you are trying to unfairly exploit this refund policy, for example, by making repetitive refund requests in respect of the same feature; (ii) if you are in breach of the Terms; or (iii) if we reasonably suspect that you are using the Subscription Based Services fraudulently.</p>
<p></p>
<p><strong>Change in Fees and Billing Method.</strong> We may change our fees and billing methods at any time. We will provide you with notice of any price increase at least thirty (30) days in advance. Subject to applicable law, (i) if you disagree with any proposed change, your sole remedy is to cancel your Subscription Based Services before the price change takes effect and (ii) your continued use of or subscription to the Subscription Based Service after the price change takes effect constitutes your agreement to pay the new price for the Subscription Based Service.</p>
<p></p>
<p><strong>Delinquency.</strong> After 30 days from the date of any unpaid charges, your Subscription Based Services will be deemed delinquent and we may terminate or suspend your account and Subscription Based Service for nonpayment. You are liable for any fees, including attorney and collection fees, incurred by us in our efforts to collect any remaining balances from you.</p>
<p></p>
<p><strong>Notice Period.</strong> You must notify us about any billing problems or discrepancies within 30 days after they first appear on your billing method statement. If you do not bring them to our attention within 30 days, you agree that you waive your right to dispute such problems or discrepancies.</p>
<p></p>
<p><strong>Disclaimer</strong></p>
<p></p>
<p>Due to the number of sources from which the content presented on our Services is obtained, and the inherent hazards of electronic distribution, there may be delays, omissions or inaccuracies in such content and the Services.</p>
<p></p>
<p>THE SERVICES, AND ANY MATERIAL AND/OR CONTENT APPEARING THEREON (&ldquo; CONTENT &rdquo;) ARE PROVIDED &ldquo;AS IS&rdquo;, WITHOUT ANY WARRANTIES. APRO, ITS EMPLOYEES, OFFICERS, DIRECTORS, AFFILIATES, AGENTS AND LICENSORS CANNOT AND DO NOT WARRANT THE ACCURACY, COMPLETENESS, CURRENTNESS, TIMELINESS, NONINFRINGEMENT, TITLE, MERCHANTABILITY OR FITNESS FOR A PARTICULAR PURPOSE OF THE CONTENT AVAILABLE THROUGH THE SERVICES, OR THE SERVICES THEMSELVES, AND APRO HEREBY DISCLAIMS ANY SUCH EXPRESS OR IMPLIED WARRANTIES.</p>
<p></p>
<p>IN NO EVENT SHALL APRO, ITS EMPLOYEES, OFFICERS, DIRECTORS, AFFILIATES, AGENTS OR LICENSORS BE LIABLE TO YOU OR TO ANY THIRD PARTY OR TO ANYONE ELSE FOR ANY KIND OF FINANCIAL LOSS, LOST PROFITS, ANY SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGE OR ANY OTHER SIMILAR DAMAGE OR ANY OTHER LOSS OR INJURY, RESULTING DIRECTLY OR INDIRECTLY FROM USE OF THE SERVICES, CAUSED IN WHOLE OR PART BY ITS NEGLIGENCE OR CONTINGENCIES BEYOND ITS CONTROL IN PROCURING, COMPILING, INTERPRETING, REPORTING OR DELIVERING THE SERVICESAND ANY CONTENT ON THEREON.</p>
<p></p>
<p>IN NO EVENT SHALL APRO, ITS EMPLOYEES, OFFICERS, DIRECTORS, AFFILIATES, AGENTS OR LICENSORS BE LIABLE TO YOU OR ANY THIRD PARTY OR ANY ONE ELSE FOR ANY DECISION MADE OR ACTION TAKEN BY YOU IN RELIANCE ON SUCH CONTENT ON THE SERVICESOR THE SERVICES THEMSELVES.</p>
<p></p>
<p><strong>Legal Restrictions</strong></p>
<p></p>
<p>Without limiting the foregoing, you understand that laws regarding financial contracts vary throughout the world, and it is your sole obligation to ensure that you fully comply with any law, regulation or directive, applicable to your country of residence with regards to the use of the Services. The ability to access to our Services does not necessarily mean that our Services, and/or your activities via the Services, are legal under the laws, regulations or directives applicable to your country of residence.</p>
<p></p>
<p>The Services does not constitute, and may not be used for the purposes of, an offer or solicitation to anyone in any jurisdiction in which such an offer or solicitation is not authorized, or to any person to whom it is unlawful to make such an offer or solicitation. Access to the Services, and the offering of financial contracts via our Services, may be restricted in certain jurisdictions, and, accordingly, users accessing our Services are required to inform themselves of, and to observe, such restrictions.</p>
<p></p>
<p><strong>Limited License</strong></p>
<p></p>
<p>APRO grants you a non-exclusive, non-transferable and limited personal license to access and use the Services (the &ldquo; License &rdquo;). This License is conditional on your full and continuing compliance with these Terms and Conditions. You agree not to &ldquo;deep-link&rdquo; to the Services, resell or permit access to the Services to others, and not to copy any materials appearing on the Services for resale or for any other purpose to others without the prior written consent of APRO. You shall be responsible and bound by any unauthorized use of the Services, made in breach of this section. You agree not to use any electronic communication feature of Services on the Services for any purpose that is unlawful, tortious, abusive, and intrusive on another's privacy, harassing, libelous, defamatory, embarrassing, obscene, threatening or hateful. The License granted under these Terms and Conditions will terminate if APRO believes that any information provided by you, including (but not limited to) your Registration Credentials, is no longer current or accurate, or if you fail to otherwise comply with any term or condition of these Terms and Conditions and all rules and guidelines for each of the Services. Upon such violation, you agree to cease accessing Services. You agree that APRO, at its sole discretion and with or without notice, may terminate your access to any or all Services, and remove and discard any information or content within the Services.</p>
<p></p>
<p><strong>Use &amp; Access</strong></p>
<p></p>
<p>You shall be responsible for providing and maintaining the means by which you access the Services, which may include, but is not limited to, your personal computer or mobile device, connectivity hardware, and telecommunication lines.</p>
<p></p>
<p>You shall be responsible for all access and service fees necessary to connect to the Services and assume all charges incurred by use of such connectivity services. You further assume all risks associated with the use and storage of information on your personal computer, mobile device or on any other computer or device through which you will gain access to the Services (hereinafter referred to as &ldquo; Computer &rdquo;).</p>
<p></p>
<p>You represent and warrant that you have implemented and plan to operate and maintain appropriate protection in relation to the security and control of your Computer and any information and data included therein.</p>
<p></p>
<p>You agree that APRO will not be liable in any way to you in the event of failure of or damage or destruction to your Computer systems, data or records or any part thereof, or for delays, losses, errors or omissions resulting from the failure or mismanagement of any telecommunications or Computer equipment or software.</p>
<p></p>
<p>You will not in any way, whether directly or indirectly, expose APRO or any of APRO's online service providers to any computer virus or other similarly harmful or inappropriate material or device.</p>
<p></p>
<p>Without limiting the generality of the foregoing, your use of the Services is subject to the following restrictions:</p>
<p></p>
<ol>
<li>You may not use, sell, rent, lease, copy, modify, distribute, redistribute, license, publicly perform or display, publish, edit, create derivative works from, or otherwise make unauthorized use of the Services and/or any Content and Marks, without APRO's prior explicit written consent; Likewise, You shall not modify, make derivative works of, disassemble, reverse compile or reverse engineer any part of the Services, without APRO's prior explicit written consent;</li>
<li>Except as expressly stated herein, no part of the Services, Content and/or Marks contained therein may be copied, reproduced, distributed, republished, downloaded, displayed, posted or transmitted in any form or by any means, without APRO's prior explicit written consent;</li>
<li><span>You shall not access the Services in order to build a similar or competitive service;</span></li>
<li>You may not use the Services in connection with material which promotes illegal activities, or the violation of any local, state, national, or international law or regulation, including, without limitation, laws governing intellectual property and other proprietary rights (including, but not limited to, patents, trademarks, trade secrets, copyrights, or any confidential, proprietary or trade secret information of any third party), information protection and privacy, including, but not limited to, content which disseminates another person's personal information without his or her permission;</li>
<li>The Services may not be used or accessed by any automated machine, bot, spider, or such other automated feature or service;</li>
<li>You shall not impersonate any person or entity or otherwise misrepresent affiliation, connection or association with any person or entity, or use any fraudulent, misleading or inaccurate contact information;</li>
<li>You may not remove, circumvent, disable, damage or otherwise interfere with any features of the Services, or attempt to gain unauthorized access to any portion thereof through any means, or interfere with, corrupt, or disrupt the operation or performance of the Services or the ability of any other person to use them (including, without limitation, by attempting to degrade the performance of the servers in any way);</li>
<li>You may not violate other users' or third parties' rights to privacy, publicity and other rights, or harvest, scrape, data aggregate, data mine, screen scrape, index or collect data and information about other users or third parties without their consent, whether manually, or automatically with the use of any means, including without limitation bots, crawlers, spiders, sifters and load testers, without the express written consent of APRO, or engage in testing, pen-testing, sniffing or monitoring of the Services, their systems, software or hardware in any way;</li>
<li>You may not use the Services in connection with material which a reasonable person could deem to be: offensive, inaccurate, incomplete, abusive, obscene, objectionable, defamatory, libelous, fraudulent or deceptive, indecent, pornographic, profane, threatening, advocating harassment or intimidation, distressing, vulgar, hateful, malicious, harmful for minors, racially or ethnically offensive, advocating racism, bigotry, hatred or physical harm of any kind against any group or individual, or disparaging the religious, political, or legal agenda of any person or entity, or is otherwise inconsistent with these Terms and Conditions including any of our policies;</li>
</ol>
<p></p>
<p>APRO reserves the right, at any time, to modify, suspend, or discontinue the Services or any part thereof with or without notice. You agree that APRO will not be liable to You or to any third party for any modification, suspension, or discontinuance of the Services or any part thereof.</p>
<p></p>
<p><strong>Force Majeure</strong></p>
<p></p>
<p>You agree that APRO will not be liable in any way to you or to any other person in the event of force majeure (including, but not limited to, the act of any government or legal authority) or for the failure of or damage or destruction to your computer systems, data or records or any part thereof, or for delays, losses, errors or omissions resulting from the failure or mismanagement of any telecommunications or computer equipment or software.</p>
<p></p>
<p><strong>Technical Problems</strong></p>
<p></p>
<p>You understand that while the Internet and the World Wide Web are generally reliable, technical problems or other conditions may delay or prevent you from accessing the Services.</p>
<p></p>
<p>APRO shall not be liable, and you agree not to hold or seek to hold APRO or any of its agents or service providers liable, for any technical problems, system failures and malfunctions, communication line failures, equipment or software failures or malfunctions, system access issues, system capacity issues, high Internet traffic demand, security breaches and unauthorized access, and other similar computer problems and defects.</p>
<p></p>
<p>APRO does not represent, warrant or guarantee that you will be able to access or use the Services at times or locations of your choosing, or that APRO will have adequate capacity for the Services as a whole or in any geographic location.</p>
<p></p>
<p>APRO does not represent, warrant or guarantee that the Services will provide uninterrupted and error-free service. APRO does not make any warranties or guarantees with respect to the Services and the Content, including but not limited to, warranties for merchantability or fitness for a particular purpose.</p>
<p></p>
<p>Without limiting the foregoing APRO will not be responsible for an impossibility to execute orders and requirements due to failures in the operation of informational systems caused by technical faults, which are beyond its control.</p>
<p></p>
<p><strong>User Content</strong></p>
<p></p>
<p>&ldquo;User Content&rdquo; means any and all information and content that You submit to, or use with, the Services (e.g., comments, filling the form, etc). You are solely responsible for Your User Content. You assume all risks associated with the use of Your User Content, including any reliance on its accuracy, completeness or usefulness by others, or any disclosure of Your User Content that makes You or any third party personally identifiable.</p>
<p></p>
<p>You hereby represent and warrant that: (a) You are the owner of the User Content, or have sufficient rights and authority thereto; and (b) Your User Content does not violate these Terms and Conditions; and (c) your User Content does not contain any virus, adware, spyware, worms, or other harmful or malicious code. You alone are responsible for Your User Content.</p>
<p></p>
<p>Each Services user is solely responsible for any and all of its User Content. APRO does not control User Content, nor shall Fusion Media be responsible for any User Content. APRO makes no guarantees regarding the accuracy, currency, suitability, or quality of any User Content. Your interactions with other Services users are solely between You and such user. You agree that APRO will not be responsible for any loss or damage incurred as the result of any such interactions. If there is a dispute between You and any user, APRO is under no obligation to become involved in its resolution or in any other manner whatsoever.</p>
<p></p>
<p><strong>Breach</strong></p>
<p></p>
<p>You agree to fully indemnify, defend and hold harmless APRO, its corporate affiliates and their respective officers, directors and employees immediately upon demand from and against all claims, demands, liabilities, damages, losses, costs and expenses, including legal fees and other charges whatsoever, howsoever caused, that may arise as a result of: (i) any breach of these Terms and Conditions by you or (ii) violation by you of any law or the rights of any third party.</p>
<p></p>
<p>Without prejudice to any other rights in these Terms and Conditions, if You breach in whole or in part any provision contained herein, APRO or any of its corporate affiliates which provide the Services to You reserve the right to take such action as they sees fit, including (but not limited to) terminating any agreement in place with You, terminating or blocking the Services to You and/or taking legal action against You.</p>
<p></p>
<p><strong>Governing Law and Court Jurisdiction</strong></p>
<p></p>
<p>These Terms and Conditions shall be governed by the laws of the District Court of Purwokerto, Central Java, Indonesia, without regard to conflicts of law principles thereof. This is the case regardless of whether you reside or transact business with APRO anywhere else in the world.</p>
<p></p>
<p>If any part of these Terms and Conditions are held unlawful, void or unenforceable, that part will be deemed severable and will not affect the validity and enforceability of any remaining provisions.</p>
<p></p>
<p><strong>Disclosures</strong></p>
<p></p>
<p>The Services hereunder are offered by PT Karsa Media Digital, registered at:</p>
<p></p>
<p><strong>PT Karsa Media Digital</strong></p>
<p>Jl. Kertawibawa, Mutiara Land, No. B05, Karanglewas,</p>
<p>Purwokerto-Banyumas, Central Java,</p>
<p>P.O. Box. 53161</p>
<p>Indonesia</p>
<p></p>
<p></p>
<p>Last Modified: October 1, 2022</p>",
];
