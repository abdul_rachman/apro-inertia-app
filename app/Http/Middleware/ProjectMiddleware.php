<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\Project;
use Illuminate\Http\Request;

class ProjectMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        $project_id = $request->route('project_id');
        $user = auth()->user();
        $project = Project::query()
            ->where('company_id', $user->company_id)
            ->find($project_id);
        if (!$project) {
            return back()->withErrors("Project not found");
        }

        $check = $project->auditors()->where('auditor_id', $user->auditor_id)->exists();
        if (!$check) {
            return back()->withErrors("You are unauthorize in this project");
        }

        return $next($request);
    }
}
