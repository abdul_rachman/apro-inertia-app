<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class RiskOfIncorrectAcceptanceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'data.*.risk'   => 'required|numeric',
            'data.*.factor' => 'required|numeric',
            'data.*.bg1'    => 'required',
            'data.*.bg2'    => 'required',
        ];
    }
}
