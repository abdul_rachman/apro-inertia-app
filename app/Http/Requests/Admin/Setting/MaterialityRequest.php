<?php

namespace App\Http\Requests\Admin\Setting;

use Illuminate\Foundation\Http\FormRequest;

class MaterialityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'profit_before_tax' => 'required',
            'revenue'           => 'required',
            'total_assets'      => 'required',
            'net_assets'        => 'required',
            'gross_income'      => 'required',
            'expenditure'       => 'required',
            '*.min'             => 'required|numeric|between:0,100',
            '*.max'             => 'required|numeric|between:0,100',
        ];
    }
}
