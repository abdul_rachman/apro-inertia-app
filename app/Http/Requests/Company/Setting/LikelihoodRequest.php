<?php

namespace App\Http\Requests\Company\Setting;

use Illuminate\Foundation\Http\FormRequest;

class LikelihoodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'likelihood' => 'required|in:three-cell,five-cell',
        ];
    }
}
