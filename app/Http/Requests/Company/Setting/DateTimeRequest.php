<?php

namespace App\Http\Requests\Company\Setting;

use Illuminate\Foundation\Http\FormRequest;

class DateTimeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'key' => 'required|in:hour_time_24,show_date_in_status_bar,set_automatically,time_zone',
            'value' => 'required'
        ];
    }
}
