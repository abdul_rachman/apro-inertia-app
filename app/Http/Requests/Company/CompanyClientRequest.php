<?php

namespace App\Http\Requests\Company;

use Illuminate\Foundation\Http\FormRequest;

class CompanyClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'name'                       => 'required|string|min:5|max:255',
            'tax_id_number'              => 'required|numeric|digits:16',
            'business_id_number'         => 'required',
            'business_id_date'           => 'required|date',
            'deed_of_establishment'      => 'required',
            'deed_of_establishment_date' => 'required|date',
            'phone'                      => 'required|numeric|digits_between:10,15',
            'address'                    => 'required',
            'pic_name'                   => 'required|string|max:255',
            'pic_email'                  => 'required|email|max:255',
            'pic_phone'                  => 'required|numeric|digits_between:10,15',
            'status'                     => 'required|in:Active,Inactive',
        ];

        if (isUpdateUrl($this->url())) {
            $rules['id'] = 'required';
        }

        return $rules;
    }
}
