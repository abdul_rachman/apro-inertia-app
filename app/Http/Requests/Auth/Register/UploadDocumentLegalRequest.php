<?php

namespace App\Http\Requests\Auth\Register;

use Illuminate\Foundation\Http\FormRequest;

class UploadDocumentLegalRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'tax_id'                   => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'company_licence'          => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'business_id'              => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'partner_citizen_id_card'  => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'partner_business_id_card' => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'pic_citizen_id_card'      => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'pic_business_id_card'     => 'required|file|mimes:pdf,jpg,jpeg,png|max:2048',
            'privacy'                  => 'required|accepted:true',
            'term_and_conditions'      => 'required|accepted:true',
        ];
    }
}
