<?php

namespace App\Http\Requests\Auditor;

use Illuminate\Foundation\Http\FormRequest;

class ProjectRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        $rules = [
            'company_client_id' => 'required',
            'workspace_type'    => 'required|in:AUD,AUP',
            'workspace'         => 'required',
            'start_date'        => 'required|date',
            'end_date'          => 'required|date|after:start_date'
        ];

        if (isUpdateUrl($this->url())) {
            $rules['id'] = 'required';
            $rules['status'] = 'required|in:On Going,Completed';
        }

        return $rules;
    }
}
