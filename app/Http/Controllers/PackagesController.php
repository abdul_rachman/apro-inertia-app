<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\PackageRequest;
use App\Models\Package;
use App\Models\Transaction;
use Inertia\Inertia;

class PackagesController extends Controller
{
    public function index()
    {
        $packages = Package::latest('id')->paginate(20);
        return Inertia::render('Admin/Packages', [
            'packages'     => $packages,
        ]);
    }

    public function store(PackageRequest $request)
    {
        Package::create([
            'name'        => $request->name,
            'description' => $request->description,
            'max_user'    => $request->max_user ?? null,
            'storage'     => $request->storage,
            'price'       => $request->price ?? null,
            'period'      => $request->period ?? null,
        ]);

        return back()->withSuccess('Successfully add package');
    }

    public function edit(Package $package)
    {
        return Inertia::render('Admin/Packages/Edit', [
            'package' => $package,
        ]);
    }

    public function update(PackageRequest $request)
    {
        $package = Package::findOrFail($request->id);

        $package->update([
            'name'        => $request->name,
            'description' => $request->description,
            'max_user'    => $request->max_user ?? null,
            'storage'     => $request->storage,
            'price'       => $request->price ?? null,
            'period'      => $request->period ?? null,
        ]);

        return back()->withSuccess('Successfully update package');
    }

    public function destroy(Package $package)
    {
        $transaction = Transaction::where('package_id', $package->id)->first();

        if ($transaction) {
            $package->delete();    // Soft delete
        } else {
            $package->forceDelete();    // Hard delete
        }

        return back()->withSuccess('Successfully delete package');
    }
}
