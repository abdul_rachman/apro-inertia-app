<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\MidtransService;
use Illuminate\Http\Request;
use Log;

class PaymentController extends Controller
{
    public function index(Request $request)
    {
        try {
            $midtrans_service = new MidtransService();
            $midtrans_service->callback($request);

            return response()->json([
                'status' => 'success'
            ]);
        } catch (\Exception $e) {
            Log::channel('midtrans')->info('===============================');
            Log::channel('midtrans')->error('Error : ' . json_encode($e->getMessage()));
            Log::channel('midtrans')->info('Request : ' . json_encode($request));
            Log::channel('midtrans')->info('#' . "\n");

            return response()->json([
                'message' => $e->getMessage()
            ], 400);
        }
    }
}
