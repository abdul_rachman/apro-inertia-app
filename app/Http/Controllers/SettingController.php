<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\Admin\Setting\AboutRequest;
use App\Http\Requests\Admin\Setting\CertificationRequest;
use App\Http\Requests\Admin\Setting\ConfidenceLevelRequest;
use App\Http\Requests\Admin\Setting\ControlRiskRequest;
use App\Http\Requests\Admin\Setting\InherentRiskRequest;
use App\Http\Requests\Admin\Setting\LikelihoodRequest;
use App\Http\Requests\Admin\Setting\MaterialityRequest;
use App\Http\Requests\Admin\Setting\MenuRequest;
use App\Http\Requests\Admin\Setting\RiskRequest;
use App\Http\Requests\Admin\Setting\RiskOfIncorrectAcceptanceRequest;
use App\Http\Requests\Admin\Setting\UserAccessControlRequest;
use App\Http\Requests\ChangePasswordRequest;
use App\Models\Setting;
use App\Models\SignOffSetting;
use App\Models\InherentRiskConsiderationFactor;
use Inertia\Inertia;
use DB;
use Hash;

class SettingController extends Controller
{
    public function index()
    {
        return Inertia::render('Admin/Settings/Index');
    }

    public function about()
    {
        $settings = Setting::query()
            ->where('group', 'about')
            ->pluck('value', 'key');

        return Inertia::render('Admin/Settings/About', [
            'settings' => $settings
        ]);
    }

    public function updateAbout(AboutRequest $request)
    {
        DB::beginTransaction();
            $setting_keys = [
                'app_name',
                'software_version',
                'model_name',
                'security_version',
                'security_expirate_date'
            ];

            foreach ($request->all() as $key => $value) {
                if (in_array($key, $setting_keys)) {
                    Setting::where('key', $key)->update(['value' => json_encode($value)]);
                }
            }
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update about setting'
        ]);
    }

    public function legalNotice()
    {
        $settings = Setting::query()
            ->where('key', 'legal_notices')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/LegalNotice', [
            'settings' => $settings
        ]);
    }

    public function legalLicense()
    {
        $settings = Setting::query()
            ->where('key', 'license')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/License', [
            'settings' => $settings
        ]);
    }

    public function legalPrivacyPolicy()
    {
        $settings = Setting::query()
            ->where('key', 'privacy_policy')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/PrivacyPolicy', [
            'settings' => $settings
        ]);
    }

    public function legalTermAndConditions()
    {
        $settings = Setting::query()
            ->where('key', 'term_and_conditions')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/TermAndConditions', [
            'settings' => $settings
        ]);
    }

    // Update legal notice & license
    public function updateLegal(Request $request)
    {
        $setting_keys = [
            'legal_notices',
            'license',
            'privacy_policy',
            'term_and_conditions',
        ];

        if (!in_array($request->key, $setting_keys)) {
            return response()->ijson([
                'status'  => false,
                'message' => 'Invalid key'
            ]);
        }

        if (!$request->value) {
            return response()->ijson([
                'status'  => false,
                'message' => 'Value is required'
            ]);
        }

        Setting::query()
            ->where('key', $request->key)
            ->update([
                'value' => json_encode($request->value)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update legal setting'
        ]);
    }

    public function legalCertification()
    {
        $settings = Setting::query()
            ->where('key', 'certification')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/Certification', [
            'settings' => $settings->value
        ]);
    }

    public function updateLegalCertification(CertificationRequest $request)
    {
        $value = [
            'registration_number'      => $request->registration_number,
            'registration_date'        => $request->registration_date,
            'date_of_protection_start' => $request->date_of_protection_start,
            'app_name'                 => $request->app_name,
            'registered_by'            => $request->registered_by,
            'domain_name'              => $request->domain_name
        ];

        Setting::where('key', 'certification')->update(['value' => json_encode($value)]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update legal certification setting'
        ]);
    }

    public function materiality()
    {
        $settings = Setting::query()
            ->where('group', 'materiality')
            ->get();

        $data = [];
        $value_keys = [
            'profit_before_tax',
            'total_assets',
            'gross_income',
        ];
        foreach ($settings as $key => $setting) {
            foreach ($setting->value as $i => $item) {
                $data[] = [
                    'id'     => $i,
                    'parent' => in_array($i, $value_keys) ? $setting->name : '',
                    'label'  => ucfirst(str_replace('_', ' ', $i)),
                    'min'    => $item['min'],
                    'max'    => $item['max'],
                ];
            }
        }

        return Inertia::render('Admin/Settings/Materiality', [
            'settings' => $data
        ]);
    }

    public function updateMateriality(MaterialityRequest $request)
    {
        DB::beginTransaction();
            Setting::query()
                ->where('key', 'profit_based_entities')
                ->update([
                    'value' => [
                        'profit_before_tax' => [
                            'min' => $request->profit_before_tax['min'],
                            'max' => $request->profit_before_tax['max']
                        ],
                        'revenue' => [
                            'min' => $request->revenue['min'],
                            'max' => $request->revenue['max']
                        ]
                    ]
                ]);
            Setting::query()
                ->where('key', 'asset_based_or_investment_entities')
                ->update([
                    'value' => [
                        'total_assets' => [
                            'min' => $request->total_assets['min'],
                            'max' => $request->total_assets['max']
                        ],
                        'net_assets' => [
                            'min' => $request->net_assets['min'],
                            'max' => $request->net_assets['max']
                        ]
                    ]
                ]);
            Setting::query()
                ->where('key', 'not_for_profit_entities')
                ->update([
                    'value' => [
                        'gross_income' => [
                            'min' => $request->gross_income['min'],
                            'max' => $request->gross_income['max']
                        ],
                        'expenditure' => [
                            'min' => $request->expenditure['min'],
                            'max' => $request->expenditure['max']
                        ]
                    ]
                ]);
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update materiality setting'
        ]);
    }

    public function risk()
    {
        $risks = Setting::query()
            ->where('group', 'risk_material_misstatement_level')
            ->pluck('value', 'key');

        $inherent_risks = InherentRiskConsiderationFactor::query()
            ->whereNull('company_id')
            ->where('is_active', 1)
            ->get();

        return Inertia::render('Admin/Settings/Risk', [
            'risks'          => $risks,
            'inherent_risks' => $inherent_risks
        ]);
    }

    public function updateRisk(RiskRequest $request)
    {
        DB::beginTransaction();
            foreach ($request->all() as $key => $value) {
                Setting::query()
                    ->where('key', $key)
                    ->update([
                        'value' => json_encode($value)
                    ]);
            }
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update risk setting'
        ]);
    }

    public function storeInherentRisk(InherentRiskRequest $request)
    {
        InherentRiskConsiderationFactor::create([
            'company_id'     => null,
            'name'           => $request->name,
            'is_significant' => $request->is_significant,
            'is_active'      => 1,
            'is_editable'    => 1,
        ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully add inherent risk setting'
        ]);
    }

    public function updateInherentRisk(InherentRiskRequest $request)
    {
        foreach ($request->data as $key => $item) {
            if ($item['is_editable'] == 0) {
                continue;
            }

            InherentRiskConsiderationFactor::query()
                ->where('id', $item['id'])
                ->where('is_editable', 1)
                ->whereNull('company_id')
                ->update([
                    'name' => $item['name'],
                    'is_significant' => $item['is_significant']
                ]);
        }

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update inherent risk setting'
        ]);
    }

    public function destroyInherentRisk(InherentRiskRequest $request)
    {
        InherentRiskConsiderationFactor::query()
            ->where('id', $request->id)
            ->where('is_editable', 1)
            ->whereNull('company_id')
            ->update([
                'is_active' => 0,
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully delete inherent risk setting'
        ]);
    }

    public function likelihood()
    {
        $setting = Setting::query()
            ->where('group', 'likelihood_and_magnitude')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/Likelihood', [
            'settings' => $setting->value
        ]);
    }

    public function updateLikelihood(LikelihoodRequest $request)
    {
        Setting::query()
            ->where('key', 'likelihood_and_magnitude')
            ->update([
                'value' => json_encode($request->likelihood)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update likelihood setting'
        ]);
    }

    public function controlRisk()
    {
        $settings = Setting::query()
            ->where('group', 'control_risk')
            ->pluck('value', 'key');

        return Inertia::render('Admin/Settings/ControlRisk', [
            'settings' => $settings
        ]);
    }

    public function updateControlRisk(ControlRiskRequest $request)
    {
        DB::beginTransaction();
            foreach ($request->except(['type']) as $key => $value) {
                Setting::query()
                    ->where('key', $key)
                    ->update([
                        'value' => json_encode($value)
                    ]);
            }
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update control risk setting'
        ]);
    }

    public function confidenceLevel()
    {
        $settings = Setting::query()
            ->where('key', 'confidence_level')
            ->firstOrFail();
        $data = collect($settings->value);

        return Inertia::render('Admin/Settings/ConfidenceLevel', [
            'settings' => $data->pluck('value', 'key')
        ]);
    }

    public function updateConfidenceLevel(ConfidenceLevelRequest $request)
    {
        $data = [];
        foreach ($request->all() as $key => $item) {
            $data[] = [
                'key'   => $key,
                'value' => $item,
            ];
        }

        Setting::query()
            ->where('key', 'confidence_level')
            ->update([
                'value' => $data
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update confidence level setting'
        ]);
    }

    public function riskOfIncorrectAcceptance()
    {
        $setting = Setting::query()
            ->where('key', 'risk_of_incorrect_acceptance')
            ->firstOrFail();

        return Inertia::render('Admin/Settings/RiskOfIncorrectAcceptance', [
            'settings' => $setting->value
        ]);
    }

    public function updateRiskOfIncorrectAcceptance(RiskOfIncorrectAcceptanceRequest $request)
    {
        Setting::query()
            ->where('key', 'risk_of_incorrect_acceptance')
            ->update([
                'value' => $request->data
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update risk of incorrect acceptance setting'
        ]);
    }

    public function userAccessControl()
    {
        $settings = Setting::query()
            ->where('group', 'user_access_control')
            ->pluck('value', 'key');

        return Inertia::render('Admin/Settings/UserAccessControl', [
            'settings' => $settings
        ]);
    }

    public function updateUserAccessControl(UserAccessControlRequest $request)
    {
        $keys = [
            'user_access_auditor',
            'user_access_manager',
            'user_access_partner',
            'user_access_eqcr_partner',
            'user_access_managing_partner',
            'user_access_risk_management_partner',
            'user_access_auditor_expert',
            'user_access_guess',
        ];

        DB::beginTransaction();
            foreach ($request->only($keys) as $key => $value) {
                Setting::query()
                    ->where('key', $key)
                    ->update([
                        'value' => $value
                    ]);
            }
        DB::commit();

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update user access control setting'
        ]);
    }

    public function menu()
    {
        $settings = Setting::query()
            ->where('group', 'apro_menu_setting')
            ->pluck('value', 'key');

        return Inertia::render('Admin/Settings/MenuSettings', [
            'settings' => $settings
        ]);
    }

    public function updateMenu(MenuRequest $request)
    {
        Setting::query()
            ->where('key', $request->key)
            ->update([
                'value' => json_encode($request->value)
            ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully update menu setting'
        ]);
    }

    private function signOffValue($value)
    {
        $result = 'N/A';
        if ($value == 1) {
            $result = 'Yes';
        } else if ($value == 0) {
            $result = 'No';
        }

        return $result;
    }

    private function signOffData($settings)
    {
        $data = [];
        foreach ($settings as $key => $setting) {
            $columns = [];
            foreach ($setting as $i => $item) {
                $columns[] = $this->signOffValue($item->has_permission);
            }

            $data[] = [
                'title'   => $setting->first()->role->name,
                'columns' => $columns
            ];
        }

        return $data;
    }

    public function preEngagement()
    {
        $settings = SignOffSetting::query()
            ->where('group', 'pre_engagement')
            ->get()
            ->groupBy('auditor_role_id');

        return Inertia::render('Admin/Settings/PreEngagement', [
            'settings' => $settings
        ]);
    }

    public function identifyAndAssessRisk()
    {
        $settings = SignOffSetting::query()
            ->with('role')
            ->where('group', 'identify_assess_risk')
            ->get()
            ->groupBy('auditor_role_id');

        return Inertia::render('Admin/Settings/IdentifyAndAssessRisk', [
            'settings' => $this->signOffData($settings)
        ]);
    }

    public function designAuditResponse()
    {
        $settings = SignOffSetting::query()
            ->where('group', 'design_audit_response')
            ->get()
            ->groupBy('auditor_role_id');

        return Inertia::render('Admin/Settings/DesignAuditResponse', [
            'settings' => $this->signOffData($settings)
        ]);
    }

    public function obtainAuditEvidance()
    {
        $settings = SignOffSetting::query()
            ->where('group', 'obtain_audit_evidence')
            ->get()
            ->groupBy('auditor_role_id');

        return Inertia::render('Admin/Settings/ObtainAuditEvidance', [
            'settings' => $this->signOffData($settings)
        ]);
    }

    public function completionAndReporting()
    {
        $settings = SignOffSetting::query()
            ->where('group', 'completion_and_reporting')
            ->get()
            ->groupBy('auditor_role_id');

        return Inertia::render('Admin/Settings/CompletionAndReporting', [
            'settings' => $this->signOffData($settings)
        ]);
    }

    public function updateChangePassword(ChangePasswordRequest $request)
    {
        $user = auth()->user();
        if (!Hash::check($request->old_password, $user->password)) {
            return response()->ijson([
                'status'  => false,
                'message' => 'Invalid old password'
            ]);
        }

        $user->update([
            'password' => bcrypt($request->new_password)
        ]);

        return response()->ijson([
            'status'  => true,
            'message' => 'Successfully change password'
        ]);
    }
}
