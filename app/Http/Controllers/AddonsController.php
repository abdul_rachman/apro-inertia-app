<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\PackageAddOnRequest;
use App\Models\PackageAddOn;
use Inertia\Inertia;

class AddonsController extends Controller
{
    public function index()
    {
        $addons = PackageAddOn::latest('id')->paginate(20);
        return Inertia::render('Admin/Addons', [
            'addons'     => $addons,
        ]);
    }

    public function store(PackageAddOnRequest $request)
    {
        PackageAddOn::create([
            'name'        => $request->name,
            'description' => $request->description,
            'price'       => $request->price ?? null,
            'unit'        => $request->unit,
        ]);

        return back()->withSuccess('Successfully add package add on');
    }

    public function edit(PackageAddOn $package_add_on)
    {
        return Inertia::render('Admin/Addons/Edit', [
            'addon' => $package_add_on,
        ]);
    }

    public function update(PackageAddOnRequest $request)
    {
        $package_add_on = PackageAddOn::findOrFail($request->id);

        $package_add_on->update([
            'name'        => $request->name,
            'description' => $request->description,
            'price'       => $request->price ?? null,
            'unit'        => $request->unit,
        ]);

        return back()->withSuccess('Successfully update package add on');
    }

    public function destroy(PackageAddOn $package_add_on)
    {
        $package_add_on->delete();

        return back()->withSuccess('Successfully delete package add on');
    }
}
