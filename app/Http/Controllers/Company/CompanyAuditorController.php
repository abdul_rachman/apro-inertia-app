<?php

namespace App\Http\Controllers\Company;

use App\Http\Controllers\Controller;
use App\Http\Requests\Company\CompanyAuditorRequest;
use App\Mail\Auditor\UserActivationMail;
use App\Models\Auditor;
use App\Models\AuditorRole;
use App\Models\Company;
use App\Models\ProjectAuditor;
use App\Models\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Inertia\Inertia;
use Mail;

class CompanyAuditorController extends Controller
{
    public function index()
    {
        $user = auth()->user();
        $auditors = Auditor::query()
            ->with([
                'role:id,name',
                'user:id,auditor_id,name,email,status'
            ])
            ->whereHas('user', function ($q) use ($user) {
                $q->where("company_id", $user->company_id);
            })
            ->latest('id')
            ->paginate(20);

        $auditor_roles = AuditorRole::query()
            ->select(['id', 'name'])
            ->where('type', 'Office')
            ->orderBy('name')
            ->get();

        return Inertia::render('AdminKAP/Auditor', [
            'auditors' => $auditors,
            'roles' => $auditor_roles
        ]);
    }

    public function roles()
    {
        $auditor_roles = AuditorRole::query()
            ->select(['id', 'name'])
            ->where('type', 'Office')
            ->orderBy('name')
            ->get();

        return response()->json([
            'status' => true,
            'data'   => $auditor_roles
        ]);
    }

    public function store(CompanyAuditorRequest $request)
    {
        $user = auth()->user();
        $user->load('company.package');

        $count = Auditor::query()
            ->whereHas('user', function ($q) use ($user) {
                $q->where('company_id', $user->company_id)
                    ->where('role', 'auditor')
                    ->where('status', 'Active');
            })
            ->count();

        if ($user->company->package->max_user <= $count) {
            return back()->withErrors("Max. user is {$user->company->package->max_user}. Please upgrade your package.");
        }

        do {
            $activate_token = Str::random(40);
        } while (User::where('activate_token', $activate_token)->first());

        DB::beginTransaction();
        $auditor = Auditor::create([
            'auditor_role_id' => $request->auditor_role_id,
            'staff_id'        => $request->staff_id,
            'id_number'       => $request->id_number,
            'address'         => $request->address,
            'education'       => $request->education,
        ]);

        $user = User::create([
            'company_id'     => $user->company_id,
            'auditor_id'     => $auditor->id,
            'name'           => $request->name,
            'email'          => $request->email,
            'role'           => 'auditor',
            'status'         => 'Inactive',
            'activate_token' => $activate_token
        ]);
        DB::commit();

        $url = route('auditor.activation', $activate_token);

        Mail::to($request->email)->send(new UserActivationMail($url, $user->company->code));

        return redirect()->route('company.auditors.index')->withSuccess('Successfully add auditor');
    }

    public function edit(Auditor $auditor)
    {
        $auditor_roles = AuditorRole::query()
            ->select(['id', 'name'])
            ->where('type', 'Office')
            ->orderBy('name')
            ->get();

        $auditor->load('user');

        return response()->json([
            'status' => true,
            'data'   => [
                'auditor_roles' => $auditor_roles,
                'auditor'       => $auditor
            ]
        ]);
    }

    public function update(CompanyAuditorRequest $request)
    {
        $user = auth()->user();

        $auditor_user = User::query()
            ->where('auditor_id', $request->id)
            ->where('company_id', $user->company->id)
            ->firstOrFail();

        DB::beginTransaction();
        Auditor::query()
            ->where('id', $request->id)
            ->update([
                'auditor_role_id' => $request->auditor_role_id,
                'staff_id'        => $request->staff_id,
                'id_number'       => $request->id_number,
                'address'         => $request->address,
                'education'       => $request->education,
            ]);

        $auditor_user->update([
            'name'   => $request->name,
            'email'  => $request->email,
            'status' => $request->status
        ]);
        DB::commit();

        return redirect()->route('company.auditors.index')->withSuccess('Successfully update auditor');
    }

    public function destroy(Auditor $auditor)
    {
        $user = auth()->user();
        if ($auditor->user->company_id != $user->company_id) {
            return back()->withErrors('Forbidden access');
        }

        $project = ProjectAuditor::query()
            ->where('auditor_id', $auditor->id)
            ->first();
        if ($project) {
            return back()->withErrors('Auditor has been assigned in project');
        }

        DB::beginTransaction();
        $auditor->user->delete();
        $auditor->delete();
        DB::commit();

        return redirect()->route('company.auditors.index')->withSuccess('Successfully delete auditor');
    }
}
