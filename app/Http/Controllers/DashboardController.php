<?php

namespace App\Http\Controllers;

use Inertia\Inertia;

class DashboardController extends Controller
{
    public function index()
    {
        
        return Inertia::render('Admin/Dashboard');
    }

    public function auditor()
    {
        return Inertia::render('Auditor/Dashboard');
    }
}
