<?php

namespace App\Http\Controllers\Auditor;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auditor\ChartOfAccountRequest;
use App\Models\ChartOfAccountTemplate;
use App\Models\ChartOfAccount;
use App\Models\TrialBalance;
use Illuminate\Http\Request;
use Inertia\Inertia;

class ChartOfAccountController extends Controller
{
    public function index(Request $request, $project_id)
    {
        $search = $request->query('search');

        $templates = ChartOfAccountTemplate::query()
            ->with('level1')
            ->whereNotNull('parent_id')
            ->when($search, function($q) use($search) {
                $q->where('description', 'like', "%$search%");
            })
            ->get();

        $coas = ChartOfAccount::query()
            ->with('level1')
            ->where('project_id', $project_id)
            ->when($search, function($q) use($search) {
                $q->where('description', 'like', "%$search%");
            })
            ->get();

        $data = $templates->concat($coas);

        if ($request->ajax()) {
            return response()->json([
                'status' => true,
                'data'   => $data
            ]);
        }

        return Inertia::render("Auditor/ChartOfAccount", [
            'data' => $data
        ]);
    }

    public function option($project_id)
    {
        $templates = ChartOfAccountTemplate::query()
            ->whereNull('parent_id')
            ->get();

        return response()->json([
            'status' => true,
            'data'   => $templates
        ]);
    }

    public function store(ChartOfAccountRequest $request, $project_id)
    {
        $template = ChartOfAccountTemplate::where('code', $request->code)->first();
        if ($template) {
            return response()->json([
                'status'  => false,
                'message' => 'Code already has been taken'
            ]);
        }

        $coa = ChartOfAccount::query()
            ->where('project_id', $project_id)
            ->where('code', $request->code)
            ->first();

        $data = [
            'parent_id'   => $request->parent_id,
            'code'        => $request->code,
            'description' => $request->description,
            'sign'        => $request->sign,
        ];

        if ($coa) {
            $data['updated_by'] = auth()->id();
            $coa->update($data);
        } else {
            $data['project_id'] = $project_id;
            $data['created_by'] = auth()->id();
            $coa = ChartOfAccount::create($data);
        }

        return response()->json([
            'status' => true,
            'data'   => $coa
        ]);
    }

    public function destroy($project_id, ChartOfAccount $chart_of_account)
    {
        if ($project_id != $chart_of_account->project_id) {
            return response()->json([
                'status'  => false,
                'message' => 'Invalid project'
            ]);
        }

        $check = TrialBalance::query()
            ->where('reference_id', $chart_of_account->id)
            ->where('reference_type', 'App\Models\ChartOfAccount')
            ->first();
        if ($check) {
            return response()->json([
                'status'  => false,
                'message' => 'Cannot delete chart of account. It has been assigned in trial balance'
            ]);
        }

        $chart_of_account->delete();

        return response()->json([
            'status'  => true,
            'message' => 'Successfully delete chart of account'
        ]);
    }
}
