<?php

namespace App\Http\Controllers;

use App\Http\Requests\IncompleteUserRequest;
use App\Mail\Company\UserActivationMail;
use App\Mail\Company\UserIncompleteMail;
use App\Models\Company;
use App\Models\CompanyDocument;
use App\Models\CompanySetting;
use App\Models\InherentRiskConsiderationFactor;
use App\Models\User;
use App\Services\CompanySettingService;
use Inertia\Inertia;
use DB;
use Mail;

class ClientController extends Controller
{
    public function index()
    {
        $companies = Company::query()
            ->with('user:id,company_id,name,status')
            ->whereHas('user', function($q) {
                $q->where('status', 'Active');
            })
            ->latest('id')
            ->paginate(20);

        $new_companies = Company::query()
            ->with('user:id,company_id,name,status')
            ->whereHas('user', function($q) {
                $q->whereIn('status', ['New', 'Incomplete']);
            })
            ->latest('id')
            ->get();

        return Inertia::render('Admin/Clients', [
            'companies'     => $companies,
            'new_companies' => $new_companies,
        ]);
    }

    public function show(Company $company)
    {
        $company->load(['user:id,company_id,name', 'package:id,name']);

        $documents = CompanyDocument::query()
            ->where('company_id', $company->id)
            ->orderBy('type')
            ->orderBy('title')
            ->get();

        return Inertia::render('Admin/ClientsDetail', [
            'company'   => $company,
            'documents' => $documents
        ]);
    }

    public function activate(User $user)
    {
        if ($user->role != 'company-admin') {
            return back()->withErrors("Invalid user's role");
        }

        if ($user->status == 'Active') {
            return back()->withErrors("User's status already Active");
        }

        DB::beginTransaction();
            $user->update([
                'status' => 'Active'
            ]);

            $this->companySetting($user);
        DB::commit();

        Mail::to($user->email)->send(new UserActivationMail($user));

        return redirect()->route('admin.clients.show', $user->company_id)->withSuccess('Successfully activate Company Admin');
    }

    public function incomplete(IncompleteUserRequest $request, User $user)
    {
        if ($user->role != 'company-admin') {
            return back()->withErrors("Invalid user's role");
        }

        if ($user->status == 'Active') {
            return back()->withErrors("User's status already Active");
        }

        $user->update([
            'status'            => 'Incomplete',
            'incomplete_reason' => $request->reason
        ]);

        Mail::to($request->email)->send(new UserIncompleteMail($user));

        return redirect()->route('admin.clients.show', $user->company_id)->withSuccess('Successfully incomplete Company Admin');
    }

    private function companySetting($user)
    {
        $check = CompanySetting::query()
            ->where('company_id', $user->company_id)
            ->first();

        if (!$check) {
            $company_settings = CompanySettingService::defaultWhenActivate();
            foreach ($company_settings as $key => $item) {
                $item['company_id'] = $user->company_id;
                CompanySetting::create($item);
            }
        }

        $check = InherentRiskConsiderationFactor::query()
            ->where('company_id', $user->company_id)
            ->first();
        if (!$check) {
            $inherent_risks = InherentRiskConsiderationFactor::query()
                ->whereNull('company_id')
                ->where('is_active', 1)
                ->where('is_editable', 1)
                ->get()
                ->toArray();
            foreach ($inherent_risks as $key => $item) {
                $item['company_id'] = $user->company_id;
                InherentRiskConsiderationFactor::create($item);
            }
        }
    }
}
