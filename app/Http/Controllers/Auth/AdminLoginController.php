<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\AdminLoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

class AdminLoginController extends Controller
{
    public function create()
    {
        return Inertia::render('Auth/AdminLogin', [
            'canResetPassword' => Route::has('password.request'),
            'status'           => session('status'),
        ]);
    }

    public function store(AdminLoginRequest $request)
    {
        $user = User::query()
            ->where('email', $request->email)
            ->where('role', 'super-admin')
            ->first();

        if (!$user) {
            return back()->withErrors('Email not found');
        }

        if ($user->status == 'Inactive') {
            return back()->withErrors('Your account is inactive.');
        }

        $request->authenticate();

        $request->session()->regenerate();

        return redirect()->intended(route('admin.dashboard'));
    }
}
