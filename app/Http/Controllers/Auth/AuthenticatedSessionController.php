<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

class AuthenticatedSessionController extends Controller
{
    /**
     * Display the login view.
     *
     * @return \Inertia\Response
     */
    public function create()
    {
        return Inertia::render('Auth/Login', [
            'canResetPassword' => Route::has('password.request'),
            'status' => session('status'),
        ]);
    }

    public function companyCreate()
    {
        return Inertia::render('Auth/CompanyLogin', [
            'canResetPassword' => Route::has('password.request'),
            'status' => session('status'),
        ]);
    }

    /**
     * Handle an incoming authentication request.
     *
     * @param  \App\Http\Requests\Auth\LoginRequest  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(LoginRequest $request)
    {
        $user = User::query()
            ->whereHas('company', function($q) use($request) {
                $q->where('code', $request->companyID);
            })
            ->where('email', $request->email)
            ->where('role', '!=', 'super-admin')
            ->first();

        if (!$user) {
            return back()->withErrors('Email or Company ID is invalid');
        }

        if ($user->status == 'Inactive') {
            return back()->withErrors('Your account is inactive.');
        }

        $request->authenticate();

        $request->session()->regenerate();

        $redirect = redirectByUserRole($user->role, $user->status, $user->transaction);

        return redirect()->intended($redirect);
    }

    /**
     * Destroy an authenticated session.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request)
    {
        Auth::guard('web')->logout();

        $request->session()->invalidate();

        $request->session()->regenerateToken();

        return redirect('/');
    }
}
