<?php

namespace App\Services;

use App\Models\Setting;

class CompanySettingService
{
	public static function init()
	{
		return [
            // date_time
            [
                'group' => 'date_time',
                'key'   => 'hour_time_24',
                'name'  => '24-Hour Time',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'show_date_in_status_bar',
                'name'  => 'Show Date in Status Bar',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'set_automatically',
                'name'  => 'Set Automatically',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'time_zone',
                'name'  => 'Time Zone',
                'value' => 'Asia/Jakarta',
            ],
            // font
            [
                'group' => 'font',
                'key'   => 'font_type',
                'name'  => 'Font Type',
                'value' => 'Inter',
            ],
            // language_and_region
            [
                'group' => 'language_and_region',
                'key'   => 'app_language',
                'name'  => 'APRO Language',
                'value' => 'English',
            ],
            [
                'group' => 'language_and_region',
                'key'   => 'region',
                'name'  => 'Region',
                'value' => 'Indonesia',
            ],
            [
                'group' => 'language_and_region',
                'key'   => 'calendar',
                'name'  => 'Calendar',
                'value' => 'Gregorian',
            ],
            // materiality
            [
                'group' => 'materiality',
                'key'   => 'profit_based_entities',
                'name'  => 'Profit based entities',
                'value' => config('constants.profit_based_entities'),
            ],
            [
                'group' => 'materiality',
                'key'   => 'asset_based_or_investment_entities',
                'name'  => 'Asset based or investment entities',
                'value' => config('constants.asset_based_or_investment_entities'),
            ],
            [
                'group' => 'materiality',
                'key'   => 'not_for_profit_entities',
                'name'  => 'Not-for-profit entities',
                'value' => config('constants.not_for_profit_entities'),
            ],
            // risk_material_misstatement_level
            [
                'group' => 'risk_material_misstatement_level',
                'key'   => 'risk_material_misstatement_level_low',
                'name'  => 'Low',
                'value' => config('constants.risk_material_misstatement_level.low'),
            ],
            [
                'group' => 'risk_material_misstatement_level',
                'key'   => 'risk_material_misstatement_level_moderate',
                'name'  => 'Moderate',
                'value' => config('constants.risk_material_misstatement_level.moderate'),
            ],
            [
                'group' => 'risk_material_misstatement_level',
                'key'   => 'risk_material_misstatement_level_high',
                'name'  => 'High',
                'value' => config('constants.risk_material_misstatement_level.high'),
            ],
            // likelihood_and_magnitude
            [
                'group' => 'likelihood_and_magnitude',
                'key'   => 'likelihood_and_magnitude',
                'name'  => 'Likelihood & Magnitude',
                'value' => 'three-cell',
            ],
            // apro_menu_setting
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_b_1_2',
                'name'  => 'B.1.2 Fraud Considerations',
                'value' => '1',
            ],
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_b_1_3',
                'name'  => 'B.1.3 Going Concern Considerations',
                'value' => '1',
            ],
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_e_6',
                'name'  => 'E.6 Key Audit Matters',
                'value' => '1',
            ],
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_e_7',
                'name'  => 'E.7 Audit Consultation',
                'value' => '1',
            ],
            // profile - billing rate
            [
                'group' => 'billing_rate',
                'key'   => 'billing_rate',
                'name'  => 'Billing Rate',
                'value' => 'in',    // in / out JABODETABEK
            ],
            // display
            [
                'group' => 'display',
                'key'   => 'auto_lock',
                'name'  => 'Auto-Lock',
                'value' => '5 minutes',
            ],
            [
                'group' => 'display',
                'key'   => 'text_size',
                'name'  => 'Text Size',
                'value' => '12pt',
            ],
            [
                'group' => 'display',
                'key'   => 'bold_text',
                'name'  => 'Bold Text',
                'value' => '1',
            ],
            [
                'group' => 'display',
                'key'   => 'company_logo',
                'name'  => 'Company Logo',
                'value' => null,
            ],
        ];
	}

    public static function defaultWhenActivate()
    {
        $groups = [
            'materiality',
            'risk_material_misstatement_level',
            'likelihood_and_magnitude',
            'apro_menu_setting',
        ];
        $settings = Setting::query()
            ->select([
                'group',
                'key',
                'name',
                'value'
            ])
            ->whereIn('group', $groups)
            ->get()
            ->toArray();

        return array_merge($settings, [
            // date_time
            [
                'group' => 'date_time',
                'key'   => 'hour_time_24',
                'name'  => '24-Hour Time',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'show_date_in_status_bar',
                'name'  => 'Show Date in Status Bar',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'set_automatically',
                'name'  => 'Set Automatically',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'time_zone',
                'name'  => 'Time Zone',
                'value' => 'Asia/Jakarta',
            ],
            // font
            [
                'group' => 'font',
                'key'   => 'font_type',
                'name'  => 'Font Type',
                'value' => 'Inter',
            ],
            // language_and_region
            [
                'group' => 'language_and_region',
                'key'   => 'app_language',
                'name'  => 'APRO Language',
                'value' => 'English',
            ],
            [
                'group' => 'language_and_region',
                'key'   => 'region',
                'name'  => 'Region',
                'value' => 'Indonesia',
            ],
            [
                'group' => 'language_and_region',
                'key'   => 'calendar',
                'name'  => 'Calendar',
                'value' => 'Gregorian',
            ],
            // profile - billing rate
            [
                'group' => 'billing_rate',
                'key'   => 'billing_rate',
                'name'  => 'Billing Rate',
                'value' => 'in',    // in / out JABODETABEK
            ],
            // display
            [
                'group' => 'display',
                'key'   => 'auto_lock',
                'name'  => 'Auto-Lock',
                'value' => '5 minutes',
            ],
            [
                'group' => 'display',
                'key'   => 'text_size',
                'name'  => 'Text Size',
                'value' => '12pt',
            ],
            [
                'group' => 'display',
                'key'   => 'bold_text',
                'name'  => 'Bold Text',
                'value' => '1',
            ],
            [
                'group' => 'display',
                'key'   => 'company_logo',
                'name'  => 'Company Logo',
                'value' => null,
            ],
        ]);
    }
}