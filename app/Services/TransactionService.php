<?php

namespace App\Services;

use App\Models\Transaction;
use App\Models\User;
use Illuminate\Support\Str;

class TransactionService {
    public function createCode()
    {
        do {
            $code = strtoupper(Str::random(8));
            $check_code = Transaction::where('code', $code)->first();
        } while (!empty($check_code));

        return $code;
    }

	public function createInvoiceNumber()
    {
        $inv_code = 'INV/' . date('Ymd') . '/';
        $count = Transaction::query()
            ->whereYear('created_at', date('Y'))
            ->count();

        do {
            $count++;
            $invoice_number = $inv_code . sprintf('%07d', $count);
            $check_code = Transaction::where('invoice_number', $invoice_number)->first();
        } while (!empty($check_code));

        return $invoice_number;
    }

    public function settled(Transaction $transaction, User $user)
    {
        $transaction->update([
            'status' => config('statuses.transaction.completed'),
            'package_expiry_date' => date('Y-m-d', strtotime('+' . $transaction->period . 'months')),
        ]);
    }
}
