<?php

namespace App\Services;

use App\Mail\Company\InvoiceMail;
use App\Mail\Company\TransactionExpiredMail;
use App\Mail\Company\TransactionPaidMail;
use App\Models\Package;
use App\Models\PaymentGuide;
use App\Models\Transaction;
// use App\Models\User;
use Mail;

class MidtransService
{
    public $client_key;
    public $server_key;

    public function __construct()
    {
        $this->client_key = config('services.midtrans.client_key');
        $this->server_key = config('services.midtrans.server_key');

        // Set Merchant Server Key
        \Midtrans\Config::$serverKey = $this->server_key;

        // Set to Development/Sandbox Environment (default)
        \Midtrans\Config::$isProduction = config('services.midtrans.is_production');
    }

    public function snapToken($transaction_code, $total_amount)
    {
        // Set sanitization on (default)
        \Midtrans\Config::$isSanitized = true;

        // Set 3DS transaction for credit card to true
        \Midtrans\Config::$is3ds = true;

        $params = [
            'transaction_details' => [
                'order_id'     => $transaction_code,
                'gross_amount' => $total_amount,
            ]
        ];

        try {
            return \Midtrans\Snap::getSnapToken($params);
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function callback($request)
    {
        try {
            $notif = new \Midtrans\Notification();
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }

        $notif    = $notif->getResponse();
        $status   = $notif->transaction_status;
        $type     = $notif->payment_type;
        $order_id = $notif->order_id;
        $fraud    = $notif->fraud_status ?? null;

        $transaction = Transaction::with(['user.company:id,name,address,tax_id_number'])->where('code', $order_id)->first();
        if (empty($transaction)) {
            throw new \Exception("Transaction not found");
        }

        $transactionPayment = $transaction->payment()->firstOrNew();

        $transactionOldStatus = $transaction->status;
        $transactionStatus = null;
        if ($status == 'capture') {
            // For credit card transaction, we need to check whether transaction is challenge by FDS or not
            if ($type == 'credit_card') {
                if ($fraud == 'challenge') {
                    // TODO set payment status in merchant's database to 'Challenge by FDS'
                    // TODO merchant should decide whether this transaction is authorized or not in MAP
                    $transactionPayment->note  = 'Challenge by FDS';
                } else {
                    $transactionStatus = config('statuses.transaction.completed');
                }
            }
        } elseif ($status == 'settlement') {
            $transactionStatus = config('statuses.transaction.completed');
        } elseif ($status == 'deny') {
            $transactionPayment->note = '(Deny) Midtrans fraud detection system reject';
        } elseif ($status == 'expire') {
            $transactionStatus = config('statuses.transaction.expired');
            $transactionPayment->note  = 'Payment expired by Midtrans';
        } elseif ($status == 'cancel') {
            $transactionStatus = config('statuses.transaction.canceled');
            $transactionPayment->note  = 'Canceled by Midtrans or Merchant';
        }

        if (isset($notif->settlement_time)) {
            $transactionPayment->transaction_date = date('Y-m-d H:i:s', strtotime($notif->settlement_time));
        } else if (isset($notif->transaction_time)) {
            $transactionPayment->transaction_date = date('Y-m-d H:i:s', strtotime($notif->transaction_time));
        }

        if (!isset($transactionPayment->expired_at)) {
            $transactionPayment->expired_at = date('Y-m-d H:i:s', strtotime($transaction->created_at .' +24 hours'));
        }

        $transactionPayment->external_id = $notif->transaction_id;
        $transactionPayment->status = $status;
        $transactionPayment->fraud_status = $fraud;
        $transactionPayment->payment_type = $type;
        $transactionPayment->biller_code = $notif->biller_code ?? null;
        $transactionPayment->bill_key = $notif->bill_key ?? null;
        $transactionPayment->payment_code = $notif->payment_code ?? null;

        if (isset($notif->va_numbers[0]->va_number)) {
            $transactionPayment->va_number = $notif->va_numbers[0]->va_number;
        } elseif (isset($notif->permata_va_number)) {
            $transactionPayment->va_number = $notif->permata_va_number;
            $transactionPayment->payment_channel = 'permata';
        }

        if ($type == 'echannel') {
            $transactionPayment->payment_channel = 'mandiri';
        } elseif ($type == 'cstore') {
            $transactionPayment->payment_channel = $notif->store ?? null;
        }

        $transactionPayment->save();

        if ($transactionStatus != null) {
            $transaction->update([
                'status' => $transactionStatus,
            ]);

            if ($transactionStatus == config('statuses.transaction.completed')) {
                // admin push notification
                /*$admins = User::admin()->get();
                $firebase = new FirebaseService;
                foreach ($admins as $key => $admin) {
                    $firebase->fcm(
                        $admin,
                        $transaction,
                        'New Order',
                        'Order with number '. $transaction->invoice_number .' has been paid.'
                    );
                }

                // customer push notification
                $firebase->fcm(
                    $transaction->user,
                    $transaction,
                    'Pembayaran Berhasil',
                    'Pesananmu dengan nomor '. $transaction->invoice_number .' telah dibayar. Pesanan akan segera diproses.'
                );*/

                Mail::to($transaction->user->email)->send(new TransactionPaidMail($transaction->user, $transaction));
            }

            if ($transactionStatus == config('statuses.transaction.expired') &&
                $transactionOldStatus != config('statuses.transaction.canceled') && 
                $transactionOldStatus != config('statuses.transaction.rejected')
            ) {
                Mail::to($transaction->user->email)->send(new TransactionExpiredMail($transaction->user, $transaction));
            }
        }

        if ($status == 'pending') {
            // customer push notification
            /*$firebase = new FirebaseService;
            $firebase->fcm(
                $transaction->user,
                $transaction,
                'Menunggu Pembayaran',
                'Pesanan dengan nomor '. $transaction->invoice_number .' telah dibuat. Silakan lakukan pembayaran sebesar '. thousandFormat($transaction->total_amount, 'Rp') .
                    ' sebelum batas waktu '. date('d-m-Y H:i', strtotime($transaction->payment->expired_at))
            );*/

            // send invoice email
            $paymentGuide = PaymentGuide::query()
                ->where('payment_type', $transaction->payment->payment_type)
                ->when($transaction->payment->payment_channel, function($q) use($transaction) {
                    $q->where('payment_channel', $transaction->payment->payment_channel);
                })
                ->get();

            Mail::to($transaction->user->email)->send(new InvoiceMail($transaction->user, $transaction, $paymentGuide));
        }

        return $transaction;
    }
}
