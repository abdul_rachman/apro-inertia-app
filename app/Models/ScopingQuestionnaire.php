<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ScopingQuestionnaire extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'project_id',
        'questionnaire',
        'opened_by',
        'is_sign_off',
        'created_by',
        'updated_by',
    ];

    protected $casts = [
        'questionnaire' => 'array',
    ];
}
