<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ChartOfAccount extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'parent_id',
        'code',
        'description',
        'sign',
        'created_by',
        'updated_by',
    ];

    protected $appends = [
        'is_template'
    ];

    public function getIsTemplateAttribute()
    {
        return 0;
    }

    public function level1()
    {
        return $this->belongsTo(ChartOfAccountTemplate::class, 'parent_id');
    }
}
