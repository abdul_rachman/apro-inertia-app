<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CompanyDocument extends Model
{
    protected $fillable = [
        'company_id',
        'type',
        'title',
        'file_name',
        'file_size',
        'size_unit'
    ];

    protected $appends = [
        'file_url'
    ];

    public function getFileUrlAttribute()
    {
        return storageUrl(config('storage.document') . $this->file_name);
    }
}
