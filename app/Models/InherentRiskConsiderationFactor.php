<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InherentRiskConsiderationFactor extends Model
{
    use HasFactory;

    protected $fillable = [
        'company_id',
        'name',
        'is_significant',
        'is_active',
        'is_editable',
    ];
}
