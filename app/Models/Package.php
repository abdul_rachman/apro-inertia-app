<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Package extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'max_user',
        'storage',
        'price',
        'period',
    ];

    protected $casts = [
        'description' => 'array',
    ];
}
