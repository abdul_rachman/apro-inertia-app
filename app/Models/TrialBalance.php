<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrialBalance extends Model
{
    use HasFactory;

    protected $fillable = [
        'project_id',
        'reference_id',
        'reference_type',
        'client_code_account',
        'account',
        'unaudited',
        'debit_reff',
        'debit',
        'credit_reff',
        'credit',
        'current_balance',
        'prior_balance',
        'created_by',
        'updated_by',
    ];

    public function coa()
    {
        return $this->morphTo(__FUNCTION__, 'reference_type', 'reference_id');
    }
}
