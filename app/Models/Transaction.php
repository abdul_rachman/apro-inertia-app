<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $fillable = [
        'user_id',
        'package_id',
        'voucher_id',
        'code',
        'invoice_number',
        'status',
        'package_detail',
        'package_add_ons',
        'period',
        'voucher_code',
        'voucher_discount',
        'amount',
        'tax_amount',
        'admin_fee',
        'total_amount',
        'package_expiry_date',
    ];

    protected $casts = [
        'package_detail'  => 'json',
        'package_add_ons' => 'array',
    ];

    public function payment()
    {
        return $this->hasOne(TransactionPayment::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
