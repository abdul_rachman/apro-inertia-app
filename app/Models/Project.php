<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    protected $fillable = [
        'company_id',
        'company_client_id',
        'code',
        'workspace_type',
        'workspace',
        'status',
        'start_date',
        'end_date',
    ];

    public function client()
    {
        return $this->belongsTo(CompanyClient::class, 'company_client_id', 'id');
    }

    public function auditors()
    {
        return $this->belongsToMany(Auditor::class, 'project_auditors')->withPivot('is_admin');
    }
}
