<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PaymentGuide extends Model
{
    protected $fillable = [
        'payment_type',
        'payment_channel',
        'title',
        'guidances',
    ];

    protected $casts = [
        'guidances' => 'array',
    ];
}
