<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TransactionPayment extends Model
{
    protected $fillable = [
        'transaction_id',
        'external_id',
        'va_number',
        'biller_code',
        'bill_key',
        'payment_code',
        'payment_type',
        'payment_channel',
        'status',
        'fraud_status',
        'note',
        'transaction_date',
        'expired_at',
    ];
}
