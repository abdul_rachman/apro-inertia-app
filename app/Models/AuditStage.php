<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AuditStage extends Model
{
    protected $fillable = [
        'audit_stage_id',
        'workspace_type',
        'code',
        'name',
        'route_name',
    ];
}
