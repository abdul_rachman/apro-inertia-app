<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Voucher extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'code',
        'type',
        'discount',
        'quota',
        'start_date',
        'end_date',
    ];

    public function discountPrice($amount)
    {
        $discount = null;
        $promo_price = null;
        $message = null;

        if ($this->quota > 0 || $this->quota === null) {
            $check = true;
            if (isset($this->start_date) && isset($this->end_date)) {
                $today = date('Y-m-d');
                $check = $today >= $this->start_date && $today <= $this->end_date;
            }

            if ($check) {
                if ($this->type == 'Percent') {
                    $discount = $amount * $this->discount / 100;
                } else {
                    $discount = $this->discount;
                }
                $promo_price = $amount > $discount ? $amount - floor($discount) : 0;
            } else {
                $message = 'Voucher tidak dapat digunakan. Tanggal voucher tidak berlaku.';
            }
        } else {
            $message = 'Voucher tidak dapat digunakan. Kuota voucher telah habis.';
        }

        return [
            'discount' => $discount,
            'amount'   => $promo_price,
            'message'  => $message
        ];
    }
}
