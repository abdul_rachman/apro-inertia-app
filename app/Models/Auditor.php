<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Auditor extends Model
{
    use HasFactory;

    protected $fillable = [
        'auditor_role_id',
        'staff_id',
        'id_number',
        'address',
        'education',
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

    public function role()
    {
        return $this->belongsTo(AuditorRole::class, 'auditor_role_id');
    }

    public function projects()
    {
        return $this->belongsToMany(Project::class, 'project_auditors')->withPivot('is_admin');
    }
}
