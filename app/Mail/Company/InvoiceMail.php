<?php

namespace App\Mail\Company;

use App\Models\Transaction;
use App\Models\User;
use Barryvdh\DomPDF\Facade\Pdf;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class InvoiceMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(public $user, public $transaction, public $payment_guide)
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $data = [
            'user'        => $this->user,
            'transaction' => $this->transaction,
        ];
        $pdf = Pdf::loadView('pdf.company.transaction-invoice', $data);
        $file_name = str_replace('/', '-', $this->transaction->invoice_number) . '.pdf';

        return $this->subject('Invoice')
                    ->view('mails.company.transaction-invoice')
                    ->attachData($pdf->output(), $file_name, [
                        'mime' => 'application/pdf',
                    ]);
    }
}
