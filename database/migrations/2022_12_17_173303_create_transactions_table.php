<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->id();
            $table->foreignId('user_id')->constrained();
            $table->foreignId('package_id')->constrained();
            $table->foreignId('voucher_id')->nullable()->constrained();
            $table->string('code');
            $table->string('invoice_number');
            $table->string('status');
            $table->json('package_detail');
            $table->text('package_add_ons')->nullable();
            $table->unsignedInteger('period')->nullable();
            $table->string('voucher_code')->nullable();
            $table->unsignedInteger('voucher_discount')->nullable();
            $table->unsignedInteger('amount');
            $table->unsignedInteger('tax_amount')->nullable();
            $table->unsignedInteger('admin_fee')->nullable();
            $table->unsignedInteger('total_amount');
            $table->date('package_expiry_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
};
