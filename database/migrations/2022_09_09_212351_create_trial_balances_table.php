<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trial_balances', function (Blueprint $table) {
            $table->id();
            $table->foreignId('project_id')
                    ->constrained()
                    ->onUpdate('cascade')
                    ->onDelete('cascade');
            $table->unsignedBigInteger('reference_id');
            $table->string('reference_type');
            $table->string('client_code_account');
            $table->string('account');
            $table->decimal('unaudited', 18, 2);
            $table->decimal('debit_reff', 18, 2)->nullable();
            $table->decimal('debit', 18, 2)->nullable();
            $table->decimal('credit_reff', 18, 2)->nullable();
            $table->decimal('credit', 18, 2)->nullable();
            $table->decimal('current_balance', 18, 2);
            $table->decimal('prior_balance', 18, 2);
            $table->timestamps();
            $table->unsignedBigInteger('created_by')->nullable();
            $table->unsignedBigInteger('updated_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trial_balances');
    }
};
