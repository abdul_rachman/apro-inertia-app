<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('audit_stages', function (Blueprint $table) {
            $table->unsignedBigInteger('audit_stage_id')->nullable()->change();
            $table->string('workspace_type')->nullable()->after('audit_stage_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('audit_stages', function (Blueprint $table) {
            $table->unsignedBigInteger('audit_stage_id')->nullable(false)->change();
            $table->dropColumn('workspace_type');
        });
    }
};
