<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\CompanyClient;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CompanyClientSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        CompanyClient::truncate();

        $faker = Faker::create();

        $companies = Company::get();
        foreach ($companies as $key => $company) {
            for ($i=0; $i < 10; $i++) {
                $tax_id_number = randomNumber(16);
                $code = companyCode($tax_id_number, 1);
                $date_time = $faker->dateTimeBetween('-30 years', 'now');
                $date = $date_time->format('Y-m-d');
                $company_name = $faker->company;
                $phone = '021' . $faker->randomNumber(8);
                $pic_name = $faker->firstName;

                CompanyClient::create([
                    'company_id'              => $company->id,
                    'code'                    => $code,
                    'tax_id_number'           => $tax_id_number,
                    'name'                    => $company_name,
                    'phone'                   => $phone,
                    'address'                 => $faker->address,
                    'business_id_number'      => randomNumber(13),
                    'business_id_date'        => $date,
                    'deed_of_establishment'   => $faker->randomNumber(8),
                    'deed_of_establishment_date' => $date,
                    'website'                 => Str::slug($company_name, '-') . '.com',
                    'pic_name'                => $pic_name,
                    'pic_email'               => Str::slug($pic_name, '_') . '@mail.com',
                    'pic_phone'               => '08' . randomNumber(10),
                    'status'                  => 'Active'
                ]);
            }
        }
    }
}
