<?php

namespace Database\Seeders;

use App\Models\Company;
use App\Models\CompanySetting;
use App\Models\InherentRiskConsiderationFactor;
use App\Models\Setting;
use App\Models\SignOffSetting;
use App\Services\CompanySettingService;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        SignOffSetting::truncate();
        InherentRiskConsiderationFactor::truncate();
        CompanySetting::truncate();
        Setting::truncate();

        $settings = [
            // about
            [
                'group' => 'about',
                'key'   => 'app_name',
                'name'  => 'Name',
                'value' => 'APRO',
            ],
            [
                'group' => 'about',
                'key'   => 'software_version',
                'name'  => 'Software Version',
                'value' => '1.0.0',
            ],
            [
                'group' => 'about',
                'key'   => 'model_name',
                'name'  => 'Model Name',
                'value' => 'APRO Beta',
            ],
            [
                'group' => 'about',
                'key'   => 'security_version',
                'name'  => 'Security Version',
                'value' => '202205123128',
            ],
            [
                'group' => 'about',
                'key'   => 'security_expirate_date',
                'name'  => 'Expirate Date',
                'value' => '18/10/2025',
            ],
            // software_update
            [
                'group' => 'software_update',
                'key'   => 'automatic_updates',
                'name'  => 'Automatic Updates',
                'value' => '1',
            ],
            // date_time
            [
                'group' => 'date_time',
                'key'   => 'hour_time_24',
                'name'  => '24-Hour Time',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'show_date_in_status_bar',
                'name'  => 'Show Date in Status Bar',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'set_automatically',
                'name'  => 'Set Automatically',
                'value' => '1',
            ],
            [
                'group' => 'date_time',
                'key'   => 'time_zone',
                'name'  => 'Time Zone',
                'value' => 'Asia/Jakarta',
            ],
            // font
            [
                'group' => 'font',
                'key'   => 'font_type',
                'name'  => 'Font Type',
                'value' => 'Inter',
            ],
            // language_and_region
            [
                'group' => 'language_and_region',
                'key'   => 'app_language',
                'name'  => 'APRO Language',
                'value' => 'English',
            ],
            [
                'group' => 'language_and_region',
                'key'   => 'region',
                'name'  => 'Region',
                'value' => 'Indonesia',
            ],
            [
                'group' => 'language_and_region',
                'key'   => 'calendar',
                'name'  => 'Calendar',
                'value' => 'Gregorian',
            ],
            // legal_and_regulatory
            [
                'group' => 'legal_and_regulatory',
                'key'   => 'legal_notices',
                'name'  => 'Legal Notices',
                'value' => config('constants.legal_notices'),
            ],
            [
                'group' => 'legal_and_regulatory',
                'key'   => 'license',
                'name'  => 'License',
                'value' => config('constants.license'),
            ],
            [
                'group' => 'legal_and_regulatory',
                'key'   => 'certification',
                'name'  => 'Certification',
                'value' => config('constants.certification'),
            ],
            [
                'group' => 'legal_and_regulatory',
                'key'   => 'privacy_policy',
                'name'  => 'Privacy Policy',
                'value' => config('constants.privacy_policy'),
            ],
            [
                'group' => 'legal_and_regulatory',
                'key'   => 'term_and_conditions',
                'name'  => 'Term and Conditions',
                'value' => config('constants.term_and_conditions'),
            ],
            // materiality
            [
                'group' => 'materiality',
                'key'   => 'profit_based_entities',
                'name'  => 'Profit based entities',
                'value' => config('constants.profit_based_entities'),
            ],
            [
                'group' => 'materiality',
                'key'   => 'asset_based_or_investment_entities',
                'name'  => 'Asset based or investment entities',
                'value' => config('constants.asset_based_or_investment_entities'),
            ],
            [
                'group' => 'materiality',
                'key'   => 'not_for_profit_entities',
                'name'  => 'Not-for-profit entities',
                'value' => config('constants.not_for_profit_entities'),
            ],
            // risk_material_misstatement_level
            [
                'group' => 'risk_material_misstatement_level',
                'key'   => 'risk_material_misstatement_level_low',
                'name'  => 'Low',
                'value' => config('constants.risk_material_misstatement_level.low'),
            ],
            [
                'group' => 'risk_material_misstatement_level',
                'key'   => 'risk_material_misstatement_level_moderate',
                'name'  => 'Moderate',
                'value' => config('constants.risk_material_misstatement_level.moderate'),
            ],
            [
                'group' => 'risk_material_misstatement_level',
                'key'   => 'risk_material_misstatement_level_high',
                'name'  => 'High',
                'value' => config('constants.risk_material_misstatement_level.high'),
            ],
            // likelihood_and_magnitude
            [
                'group' => 'likelihood_and_magnitude',
                'key'   => 'likelihood_and_magnitude',
                'name'  => 'Likelihood & Magnitude',
                'value' => 'three-cell',
            ],
            // control_risk
            [
                'group' => 'control_risk',
                'key'   => 'control_risk_low',
                'name'  => 'Low',
                'value' => config('constants.control_risk_low'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_risk_moderate',
                'name'  => 'Moderate',
                'value' => config('constants.control_risk_moderate'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_risk_high',
                'name'  => 'High',
                'value' => config('constants.control_risk_high'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_design_yes',
                'name'  => 'Yes',
                'value' => config('constants.control_design_yes'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_design_partial',
                'name'  => 'Partial',
                'value' => config('constants.control_design_partial'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_design_no',
                'name'  => 'No',
                'value' => config('constants.control_design_no'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_implementation_yes',
                'name'  => 'Yes',
                'value' => config('constants.control_implementation_yes'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_implementation_partial',
                'name'  => 'Partial',
                'value' => config('constants.control_implementation_partial'),
            ],
            [
                'group' => 'control_risk',
                'key'   => 'control_implementation_no',
                'name'  => 'No',
                'value' => config('constants.control_implementation_no'),
            ],
            // confidence_level
            [
                'group' => 'confidence_level',
                'key'   => 'confidence_level',
                'name'  => 'Risk Reduction Required',
                'value' => config('constants.confidence_level'),
            ],
            // risk_of_incorrect_acceptance
            [
                'group' => 'risk_of_incorrect_acceptance',
                'key'   => 'risk_of_incorrect_acceptance',
                'name'  => 'Risk of Incorrect Acceptance (%)',
                'value' => config('constants.risk_of_incorrect_acceptance'),
            ],
            // user_access_control
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_auditor',
                'name'  => 'Auditor',
                'value' => config('constants.user_access_auditor'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_manager',
                'name'  => 'Manager',
                'value' => config('constants.user_access_manager'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_partner',
                'name'  => 'Partner',
                'value' => config('constants.user_access_partner'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_eqcr_partner',
                'name'  => 'EQCR Partner',
                'value' => config('constants.user_access_eqcr_partner'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_managing_partner',
                'name'  => 'Managing Partner',
                'value' => config('constants.user_access_managing_partner'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_risk_management_partner',
                'name'  => 'Risk Management Partner',
                'value' => config('constants.user_access_risk_management_partner'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_auditor_expert',
                'name'  => 'Auditor Expert',
                'value' => config('constants.user_access_auditor_expert'),
            ],
            [
                'group' => 'user_access_control',
                'key'   => 'user_access_guess',
                'name'  => 'Guess',
                'value' => config('constants.user_access_guess'),
            ],
            // apro_menu_setting
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_b_1_2',
                'name'  => 'B.1.2 Fraud Considerations',
                'value' => '1',
            ],
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_b_1_3',
                'name'  => 'B.1.3 Going Concern Considerations',
                'value' => '1',
            ],
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_e_6',
                'name'  => 'E.6 Key Audit Matters',
                'value' => '1',
            ],
            [
                'group' => 'apro_menu_setting',
                'key'   => 'menu_e_7',
                'name'  => 'E.7 Audit Consultation',
                'value' => '1',
            ],
            // sign_off_setting
            [
                'group' => 'sign_off_setting',
                'key'   => 'sign_off_pre_engagement',
                'name'  => 'A Pre-engagement',
                'value' => null,
            ],
            [
                'group' => 'sign_off_setting',
                'key'   => 'sign_off_identify_and_asses_risk',
                'name'  => 'B Identify and Asses Risk',
                'value' => null,
            ],
            [
                'group' => 'sign_off_setting',
                'key'   => 'sign_off_design_audit_response',
                'name'  => 'C Design Audit Response',
                'value' => null,
            ],
            [
                'group' => 'sign_off_setting',
                'key'   => 'sign_off_obtain_audit_evidance',
                'name'  => 'D Obtain Audit Evidance',
                'value' => null,
            ],
            [
                'group' => 'sign_off_setting',
                'key'   => 'sign_off_completion_and_reporting',
                'name'  => 'E Completion and Reporting',
                'value' => null,
            ],
            // display
            [
                'group' => 'display',
                'key'   => 'auto_lock',
                'name'  => 'Auto-Lock',
                'value' => '5 minutes',
            ],
            [
                'group' => 'display',
                'key'   => 'text_size',
                'name'  => 'Text Size',
                'value' => '12pt',
            ],
            [
                'group' => 'display',
                'key'   => 'bold_text',
                'name'  => 'Bold Text',
                'value' => '1',
            ],
            [
                'group' => 'display',
                'key'   => 'company_logo',
                'name'  => 'Company Logo',
                'value' => null,
            ],
        ];

        foreach ($settings as $key => $item) {
            Setting::create($item);
        }

        $risks = [
            [
                'name'           => 'Risk a risk of fraud',
                'is_significant' => 1,
                'is_active'      => 1,
                'is_editable'    => 0,
            ],
            [
                'name'           => 'Risk is related to recent significant economic, accounting or other development',
                'is_significant' => 1,
                'is_active'      => 1,
                'is_editable'    => 0,
            ],
            [
                'name'           => 'The complexity of transactions',
                'is_significant' => 1,
                'is_active'      => 1,
                'is_editable'    => 0,
            ],
            [
                'name'           => 'Risk involves significant transaction with related parties',
                'is_significant' => 1,
                'is_active'      => 1,
                'is_editable'    => 0,
            ],
            [
                'name'           => 'The degree of subjectivity in the measurement uncertainty of financial information (involving a wide range of measurement uncertanity)',
                'is_significant' => 1,
                'is_active'      => 1,
                'is_editable'    => 0,
            ],
            [
                'name'           => 'Risk involves significant transaction that outside the normal course of business (Unusual transaction)',
                'is_significant' => 1,
                'is_active'      => 1,
                'is_editable'    => 0,
            ],
            [
                'name'           => 'Usual routine of transaction of business',
                'is_significant' => 0,
                'is_active'      => 1,
                'is_editable'    => 1,
            ],
            [
                'name'           => 'Corrected misstatement in prior period',
                'is_significant' => 0,
                'is_active'      => 1,
                'is_editable'    => 1,
            ],
            [
                'name'           => 'Vulnerable to changes in the business environment',
                'is_significant' => 0,
                'is_active'      => 1,
                'is_editable'    => 1,
            ],
            [
                'name'           => 'There are identified contingencies',
                'is_significant' => 0,
                'is_active'      => 1,
                'is_editable'    => 1,
            ],
            [
                'name'           => 'Impact on financial loss',
                'is_significant' => 0,
                'is_active'      => 1,
                'is_editable'    => 1,
            ]
        ];

        foreach ($risks as $key => $item) {
            InherentRiskConsiderationFactor::create($item);
        }

        // Begin : Sign Off Setting
        $pre_engagements = ['A.1', 'A.2', 'A.3', 'A.4', 'A.5', 'A.6', 'CWS'];
        foreach ($pre_engagements as $item) {
            for ($i=1; $i <= 8; $i++) {     // auditor_role_id
                $has_permission = null;
                if ($i <= 3 || $i == 7 && $item == 'A.6') {
                    $has_permission = 1;
                } else if (in_array($i, [4, 5, 6, 7]) && $item == 'CWS') {
                    $has_permission = 0;
                }

                SignOffSetting::create([
                    'group'            => 'pre_engagement',
                    'auditor_role_id'  => $i,
                    'audit_stage_code' => $item,
                    'has_permission'   => $has_permission,
                    'is_editable'      => is_null($has_permission) ? 1 : 0
                ]);
            }
        }

        $identify_assess_risks = [
            'B.1', 'B.2', 'B.3', 'B.4',
            'B.5', 'B.6', 'B.7', 'B.8',
            'B.9', 'B.10', 'B.11', 'B.12',
            'B.13', 'ERA'
        ];
        foreach ($identify_assess_risks as $item) {
            for ($i=1; $i <= 8; $i++) {
                $has_permission = null;
                if ($i <= 3 || $i == 7 && in_array($item, ['B.2', 'B.4', 'B.5', 'B.6'])) {
                    $has_permission = 1;
                } else if ($i == 7) {
                    $has_permission = 0;
                }

                SignOffSetting::create([
                    'group'            => 'identify_assess_risk',
                    'auditor_role_id'  => $i,
                    'audit_stage_code' => $item,
                    'has_permission'   => $has_permission,
                    'is_editable'      => is_null($has_permission) ? 1 : 0
                ]);
            }
        }

        $design_audit_responses = ['C.1', 'C.2', 'C.3', 'C.4', 'C.5', 'C.6', 'ETDC', 'PSO'];
        foreach ($design_audit_responses as $item) {
            for ($i=1; $i <= 8; $i++) {
                $has_permission = null;
                if ($i <= 3 || (in_array($i, [4, 7]) && in_array($item, ['ETDC', 'PSO']))) {
                    $has_permission = 1;
                } else if (in_array($i, [5, 6]) && in_array($item, ['ETDC', 'PSO'])) {
                    $has_permission = 0;
                }

                SignOffSetting::create([
                    'group'            => 'design_audit_response',
                    'auditor_role_id'  => $i,
                    'audit_stage_code' => $item,
                    'has_permission'   => $has_permission,
                    'is_editable'      => is_null($has_permission) ? 1 : 0
                ]);
            }
        }

        $obtain_audit_evidences = [
            'D.1', 'D.2', 'D.3', 'D.4',
            'D.5', 'D.6', 'D.7', 'D.8',
            'D.9', 'D.10', 'D.11', 'D.12',
        ];
        foreach ($obtain_audit_evidences as $item) {
            for ($i=1; $i <= 8; $i++) {
                $has_permission = null;
                if ($i <= 3 || $i == 7 && in_array($item, ['D.9', 'D.10', 'D.11'])) {
                    $has_permission = 1;
                } else if ($i == 7) {
                    $has_permission = 0;
                }

                SignOffSetting::create([
                    'group'            => 'obtain_audit_evidence',
                    'auditor_role_id'  => $i,
                    'audit_stage_code' => $item,
                    'has_permission'   => $has_permission,
                    'is_editable'      => is_null($has_permission) ? 1 : 0
                ]);
            }
        }

        $completion_and_reportings = [
            'E.1', 'E.2', 'E.3', 'E.4',
            'E.5', 'E.6', 'E.7', 'E.8',
            'E.9', 'E.10', 'IARD', 'ASO', 'AC',
        ];
        foreach ($completion_and_reportings as $item) {
            for ($i=1; $i <= 8; $i++) {
                $has_permission = 1;
                if (
                    $i == 1 && in_array($item, ['E.6', 'E.7', 'IARD']) ||
                    $i <= 3 && $item == 'E.8'
                ) {
                    $has_permission = 0;
                } else if (
                    $i == 4 && !in_array($item, ['E.7', 'E.8', 'E.9', 'IARD']) ||
                    $i > 4
                ) {
                    $has_permission = null;
                }

                SignOffSetting::create([
                    'group'            => 'completion_and_reporting',
                    'auditor_role_id'  => $i,
                    'audit_stage_code' => $item,
                    'has_permission'   => $has_permission,
                    'is_editable'      => is_null($has_permission) ? 1 : 0
                ]);
            }
        }
        // End : Sign Off Setting

        $companies = Company::all();
        $company_settings = CompanySettingService::init();
        $company_risks = collect($risks)->where('is_editable', 1);

        foreach ($companies as $key => $company) {
            foreach ($company_settings as $item) {
                $item['company_id'] = $company->id;
                CompanySetting::create($item);
            }

            foreach ($company_risks as $item) {
                $item['company_id'] = $company->id;
                InherentRiskConsiderationFactor::create($item);
            }
        }
    }
}
