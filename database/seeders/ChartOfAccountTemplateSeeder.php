<?php

namespace Database\Seeders;

use App\Models\ChartOfAccountTemplate;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ChartOfAccountTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ChartOfAccountTemplate::truncate();

        $data = [
            [
                'id'          => 1,
                'code'        => 'D3.1',
                'description' => 'Assets',
            ],
            [
                'id'          => 2,
                'code'        => 'D3.2',
                'description' => 'Liabilities',
            ],
            [
                'id'          => 3,
                'code'        => 'D3.3',
                'description' => 'Equity',
            ],
            [
                'id'          => 4,
                'code'        => 'D3.4',
                'description' => 'Revenue',
            ],
            [
                'id'          => 5,
                'code'        => 'D3.5',
                'description' => 'Expenses',
            ],
            [
                'id'          => 6,
                'code'        => 'D3.6',
                'description' => 'Other Comprehensive Income (expenses)',
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.101',
                'description' => 'Kas dan Setara Kas',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.102',
                'description' => 'Deposito Berjangka',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.103',
                'description' => 'Aset Derivatif',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.104',
                'description' => 'Investasi Surat Berharga Untuk di Jual',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.105',
                'description' => 'Piutang usaha - Jangka Pendek',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.106',
                'description' => 'Piutang lain-lain - Jangka Pendek',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.107',
                'description' => 'Kredit dan Pembiayaan',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.108',
                'description' => 'Persediaan',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.109',
                'description' => 'Pajak Dibayar di Muka - Bagian Lancar',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.110',
                'description' => 'Uang Muka dan Biaya Dibayar di Muka - Bagian Lancar',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.111',
                'description' => 'Aset Keuangan Lancar Lain',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.201',
                'description' => 'Piutang usaha - Jangka Panjang',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.202',
                'description' => 'Piutang lain-lain - Jangka Panjang',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.203',
                'description' => 'Kredit dan Pembiayaan - Jangka Panjang',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.204',
                'description' => 'Investasi Saham',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.205',
                'description' => 'Persediaan - Jangka Panjang',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.206',
                'description' => 'Properti Investasi',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.207',
                'description' => 'Aset Tetap',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.208',
                'description' => 'Aset Hak Guna',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.209',
                'description' => 'Aset Biologis',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.210',
                'description' => 'Aset Tak Berwujud',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.211',
                'description' => 'Beban Ditangguhkan',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.212',
                'description' => 'Aset Pajak Tangguhan',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.213',
                'description' => 'Goodwill',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.214',
                'description' => 'Pajak Dibayar di Muka - Bagian Tidak Lancar',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.215',
                'description' => 'Uang Muka dan Biaya Dibayar di Muka - Bagian Tidak Lancar',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.216',
                'description' => 'Aset Keuangan Lain',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 1,
                'code'        => 'D3.1.217',
                'description' => 'Aset Tidak Lancar Lain',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.101',
                'description' => 'Utang usaha - Jangka Pendek',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.102',
                'description' => 'Utang lain-lain - Jangka Pendek',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.103',
                'description' => 'Uang muka dan Pendapatan diterima di Muka',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.104',
                'description' => 'Utang Bank - Jangka Pendek',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.105',
                'description' => 'Utang Surat Berharga - Jangka Pendek',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.106',
                'description' => 'Liabilitas Sewa - Jangka Pendek',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.107',
                'description' => 'Liabilitas Asuransi - Jangka Pendek',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.108',
                'description' => 'Utang Pajak',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.109',
                'description' => 'Beban Masih Harus Dibayar',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.110',
                'description' => 'Liabilitas Jangka Pendek Lain',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.201',
                'description' => 'Utang Bank - Jangka Panjang',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.202',
                'description' => 'Utang lain-lain - Jangka Panjang',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.203',
                'description' => 'Utang Surat Berharga - Jangka Panjang',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.204',
                'description' => 'Liabilitas Sewa - Jangka Panjang',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.206',
                'description' => 'Liabilitas Asuransi - Jangka Panjang',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.208',
                'description' => 'Uang muka dan Pendapatan diterima di Muka',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.209',
                'description' => 'Liabilitas Imbalan Kerja',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.210',
                'description' => 'Utang Jaminan ',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.211',
                'description' => 'Instrumen Keuangan Derivatif',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.212',
                'description' => 'Provisi',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 2,
                'code'        => 'D3.2.213',
                'description' => 'Liabilitas Jangka Panjang Lain',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 3,
                'code'        => 'D3.3.101',
                'description' => 'Modal Saham',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 3,
                'code'        => 'D3.3.102',
                'description' => 'Tambahan Modal Disetor',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 3,
                'code'        => 'D3.3.103',
                'description' => 'Saldo Penghasilan Komprehensif Lain',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 3,
                'code'        => 'D3.3.104',
                'description' => 'Saldo Laba',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 3,
                'code'        => 'D3.3.105',
                'description' => 'Ekuitas Lain',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 3,
                'code'        => 'D3.3.201',
                'description' => 'Ekuitas Diatribusi ke Kepentingan Nonpengendali',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 4,
                'code'        => 'D3.4.101',
                'description' => 'Penjualan',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 4,
                'code'        => 'D3.4.102',
                'description' => 'Pendapatan Keuangan',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 4,
                'code'        => 'D3.4.103',
                'description' => 'Pendapatan Asuransi',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 4,
                'code'        => 'D3.4.104',
                'description' => 'Pendapatan Lain-lain',
                'sign'        => 'Credit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.101',
                'description' => 'Beban Pokok Penjualan',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.102',
                'description' => 'Beban Pemasaran',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.103',
                'description' => 'Beban Umum dan Administrasi',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.201',
                'description' => 'Bagian Laba/(rugi) Entitas Asosiasi, Ventura Bersama, dan Pengendalian Bersama',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.301',
                'description' => 'Beban Bunga',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.401',
                'description' => 'Beban Lain-lain',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.501',
                'description' => 'Pajak Penghasilan Final',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 5,
                'code'        => 'D3.5.601',
                'description' => 'Pajak Penghasilan Badan',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 6,
                'code'        => 'D3.6.101',
                'description' => 'Bagian atas Laba/(Rugi) dan Penghasilan Komprehensif Lainnya Entitas Asosiasi dan Ventura Bersama',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 6,
                'code'        => 'D3.6.201',
                'description' => 'Penghasilan Komprehensif Lainnya - Tidak Direklasifikasikan ke Laba Rugi',
                'sign'        => 'Debit'
            ],
            [
                'parent_id'   => 6,
                'code'        => 'D3.6.301',
                'description' => 'Penghasilan Komprehensif Lainnya - Direklasifikasikan ke Laba Rugi',
                'sign'        => 'Debit'
            ],
        ];

        foreach ($data as $key => $item) {
            ChartOfAccountTemplate::create($item);
        }
    }
}
