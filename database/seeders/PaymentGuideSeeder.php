<?php

namespace Database\Seeders;

use App\Models\PaymentGuide;
use Illuminate\Database\Seeder;
use File;

class PaymentGuideSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        PaymentGuide::truncate();

        $json = File::get(base_path("database/seeders/data/payment_guides.json"));
        $data = json_decode($json, true);

        foreach ($data as $key => $item) {
            PaymentGuide::create($item);
        }
    }
}
