<?php

namespace Database\Seeders;

use App\Models\Auditor;
use App\Models\AuditorRole;
use App\Models\Company;
use App\Models\CompanyClient;
use App\Models\Project;
use App\Models\ProjectAuditor;
use Faker\Factory as Faker;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class ProjectSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        ProjectAuditor::truncate();
        Project::truncate();

        $authorize_roles = [
            'Manager',
            'Supervisor',
            'Senior',
            'Junior'
        ];
        $auditor_role_ids = AuditorRole::query()
            ->whereIn('name', $authorize_roles)
            ->pluck('id')
            ->toArray();
        $companies = Company::get();

        $faker = Faker::create();
        foreach ($companies as $key => $company) {
            $auditors = Auditor::query()
                ->whereHas('user', function($q) use($company) {
                    $q->where('company_id', $company->id)
                        ->where('role', 'auditor');
                })
                ->get();

            $company_clients = CompanyClient::where('company_id', $company->id)->get();

            for ($i=0; $i < 5; $i++) {
                $start_date = $faker->dateTimeBetween('-1 years', 'now');
                $end_date = $faker->dateTimeBetween('now', '+1 years');

                $project = Project::create([
                    'company_id'        => $company->id,
                    'company_client_id' => $company_clients->random()->id,
                    'code'              => $end_date->format('Ym'),
                    'workspace_type'    => $i % 2 == 0 ? 'AUD' : 'AUP',
                    'workspace'         => Str::title(implode(' ', $faker->words(2))),
                    'status'            => $i % 3 == 1 ? 'On Going' : 'Completed',
                    'start_date'        => $start_date->format('Y-m-d'),
                    'end_date'          => $end_date->format('Y-m-d'),
                ]);

                $project_admins = $auditors->whereIn('auditor_role_id', $auditor_role_ids);
                if ($project_admins->count() >= 2) {
                    $project_admins = $project_admins->random(2);
                }

                $project_auditors = $auditors->whereNotIn('id', $project_admins->pluck('id'));
                if ($project_auditors->count() >= 3) {
                    $project_auditors = $project_auditors->random(3);
                }

                $project->auditors()->attach($project_admins->pluck('id'), ['is_admin' => 1]);
                $project->auditors()->attach($project_auditors->pluck('id'), ['is_admin' => 0]);
            }
        }
    }
}
